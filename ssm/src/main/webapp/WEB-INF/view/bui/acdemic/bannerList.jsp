<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="http://g.alicdn.com/bui/bui/1.1.21/css/bs3/dpl.css" rel="stylesheet">
  	<link href="http://g.alicdn.com/bui/bui/1.1.21/css/bs3/bui.css" rel="stylesheet">
    <link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />   

   
 </head>
 <body>

  <div class="container">
    <div class="row">
      <form id="searchForm" class="form-horizontal span24">
        <div class="row">

          <div class="control-group span8">
            <label class="control-label">名称：</label>
            <div class="controls">
              <input type="text" class="control-text" name="searchname">
            </div>
          </div>
          
          <div class="span3 offset2">
            <button  type="button" id="btnSearch" class="button button-small button-primary" style="height:25px;width:50px">搜索</button>
          </div>
        </div>
      </form>
    </div>
    <div class="search-grid-container">
      <div id="grid"></div>
    </div>

  </div>
  <div id="content" class="hide">
      <form id="J_Form" class="form-horizontal" action="<{:U('university/add')}>">
      
        <div class="row">
          <div class="control-group span8">
            <label class="control-label"><s>*</s>标题</label>
            <div class="controls">
              <input name="title" type="text" data-rules="{required:true}" class="input-normal control-text">
            </div>
          </div>
        </div>
         <div class="row">
          <div class="control-group span8">
            <label class="control-label"><s>*</s>href</label>
            <div class="controls">
              <input name="hrefUrl" type="text" class="input-normal control-text">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="control-group span8">
            <label class="control-label"><s>*</s>上传图片</label>
            <div class="controls">
            		<div id="J_Uploader">
      			</div>
              	<input name="imageUrl" type="hidden" id="imageurl">
            </div>
          </div>
        </div>
      </form>
    </div>

 <script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script>
  <script type="text/javascript" src="../scripts/ajaxfileupload.js"></script>
  <script type="text/javascript" src="../assets/js/bui-min.js"></script>
  <script src="http://g.alicdn.com/bui/seajs/2.3.0/sea.js"></script>
  <script type="text/javascript" src="../assets/js/config-min.js"></script>

  <!-- 以上一行之前没有 这样在360浏览器 执行Search.createStore时候 报错  -->
  <script type="text/javascript" src="../assets/js/common/search-min.js"></script>


  <script src="http://g.alicdn.com/bui/seajs/2.3.0/sea.js"></script>
  <script src="http://g.alicdn.com/bui/bui/1.1.21/config.js"></script>


  <script type="text/javascript">
    BUI.use('common/page');
  </script>

<script type="text/javascript">


  BUI.use('common/search',function (Search) {

      editing = new BUI.Grid.Plugins.DialogEditing({
        contentId : 'content', //设置隐藏的Dialog内容
        autoSave : true, //添加数据或者修改数据时，自动保存
        triggerCls : 'btn-edit'
      }),

      columns = [
          //{title:'编号',dataIndex:'num',width:50},
          {title:'标题',dataIndex:'title',width:200,renderer:function(value){
        	  	var length = value.length;
        	  	
        	  	if(length > 15)
        	  	{
        	  		return value.substr(0,10)+"...";
        	  	}
        	  	
        	  	return value;
          }},
          {title:'图片',dataIndex:'imageUrl',width:150,renderer : function(value){
    	  	  
	        	 
	          return "<image width='40px' height='40px' src='${pageContext.request.contextPath }/resourse/uploadBanner/"+value+"' >";
	         
	      }}, 
	      {title:'地址',dataIndex:'hrefUrl',width:400},
          {title:'操作',dataIndex:'',width:200,renderer : function(value,obj){
        	  	  
	        	  editStr1 = '<span class="grid-command btn-edit" title="编辑">编辑</span>',
		      delStr = '<span class="grid-command btn-del" title="删除">删除</span>';//页面操作不需要使用Search.createLink
	          
	          return editStr1 + delStr;
             
          }}
        ],
      store = Search.createStore('${pageContext.request.contextPath }/banner/getAllBanner',{
        proxy : {
          save : { //也可以是一个字符串，那么增删改，都会往那么路径提交数据，同时附加参数saveType
            addUrl : "${pageContext.request.contextPath }/banner/addBanner",
            updateUrl : "${pageContext.request.contextPath }/banner/updateBanner",
            removeUrl : "${pageContext.request.contextPath }/banner/delete"
          },
          method : 'POST',
        },
        autoSync : true ,//保存数据后，自动更新
        pageSize:10
      });
      
     
    var  gridCfg = Search.createGridCfg(columns,{
         tbar : {
           items : [
             {text : '<i class="icon-plus"></i>新建',btnCls : 'button button-small',handler:addFunction},
             {text : '<i class="icon-remove"></i>删除',btnCls : 'button button-small',handler : delFunction},
           ]
         },
         emptyDataTpl:'<div class="centered"><img alt="Crying" src="__PUBLIC__/img/nodata.png"><h2>查询的数据不存在</h2></div>',
         plugins : [editing,BUI.Grid.Plugins.CheckSelection,BUI.Grid.Plugins.AutoFit] // 插件形式引入多选表格

    });
      
    var  search = new Search({
        store : store,
        gridCfg : gridCfg
      }),
      grid = search.get('grid');

    function addFunction(){
      var newData = {isNew : true}; //标志是新增加的记录
      editing.add(newData,'name'); //添加记录后，直接编辑
    }

    //删除操作
    function delFunction(){
      var selections = grid.getSelection();
      delItems(selections);
    }

    function delItems(items){
        var ids = [];
        BUI.each(items,function(item){
          ids.push(item.id);
        });

        if(ids.length){
          BUI.Message.Confirm('确认要删除选中的记录么？',function(){
            store.save('remove',{ids : ids.join(',')});
          },'question');
        }
      }
    

    //监听事件，删除一条记录
    grid.on('cellclick',function(ev){
      var sender = $(ev.domTarget); //点击的Dom
      if(sender.hasClass('btn-del')){
        var record = ev.record;
        delItems([record]);
      }
    });
  });


</script>
 <script type="text/javascript">
        BUI.use('bui/uploader',function (Uploader) {
          
      /**
       * 返回数据的格式
       *
       *  默认是 {url : 'url'},否则认为上传失败
       *  可以通过isSuccess 更改判定成功失败的结构
       */
      var uploader = new Uploader.Uploader({
        render: '#J_Uploader',
        url: '${pageContext.request.contextPath }/banner/upload/',
        //rules: {
	         //文的类型
	         //ext: ['.mp4','文件类型只能为{0}'],
	         //上传的最大个数
	         //max: [1, '文件的最大个数不能超过{0}个'],
	         //文件大小的最小值,这个单位是kb
	         //minSize: [10, '文件的大小不能小于{0}KB'],
	         //文件大小的最大值,单位也是kb
	         //maxSize: [2048, '文件大小不能大于2M']
	    //},
	    //根据业务需求来判断上传是否成功
	    isSuccess: function(result){
	    	
	    	//console.log(JSON.parse(result));
	        if(result && result.url){
	        		//alert(result.url);
	        		
	        		$("#imageurl").val(result.url);
	        		
	            BUI.Message.Alert("上传成功" );
	            
	        }else{
	            BUI.Message.Alert("上传失败");
	        } 
	    }
      }).render();
    });
    </script>
    

</body>
</html>
