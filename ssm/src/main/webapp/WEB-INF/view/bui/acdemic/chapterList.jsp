<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="http://g.alicdn.com/bui/bui/1.1.21/css/bs3/dpl.css" rel="stylesheet">
  	<link href="http://g.alicdn.com/bui/bui/1.1.21/css/bs3/bui.css" rel="stylesheet">
    <link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />   

   
 </head>
 <body>

  <div class="container">
  	<a id="download_href" href="${pageContext.request.contextPath }/resourse/import_chaptersInfo_file.xlsx" style="display:none"></a>
  	<form enctype="multipart/form-data">
  		<input type="file" id="btn_file" style="display:none" onchange="uploadFile(this)" name="excelFile">
    </form>
    
    <div class="row">
      <form id="searchForm" class="form-horizontal span24">
        <div class="row">

          <div class="control-group span8">
            <label class="control-label">名称：</label>
            <div class="controls">
              <input type="text" class="control-text" name="searchname">
            </div>
          </div>
          
          <div class="span3 offset2">
            <button  type="button" id="btnSearch" class="button button-small button-primary" style="height:25px;width:50px">搜索</button>
          </div>
        </div>
      </form>
    </div>
    <div class="search-grid-container">
      <div id="grid"></div>
    </div>

  </div>
  <div id="content" class="hide">
      <form id="J_Form" class="form-horizontal" action="<{:U('university/add')}>">
      	<div class="row">
          <div class="control-group span8">
            <label class="control-label"><s>*</s>课程编号</label>
            <div class="controls">
              <input name="course_id" type="text" data-rules="{required:true}" class="input-normal control-text">
            </div>
          </div>
        </div>
        
        <div class="row">
          <div class="control-group span8">
            <label class="control-label"><s>*</s>章节名称</label>
            <div class="controls">
              <input name="name" type="text" data-rules="{required:true}" class="input-normal control-text">
            </div>
          </div>
        </div>
       <div class="row">
	        	<div class="control-group span8">
		        	<label class="control-label"><s>*</s>是否重点</label>
		            <div class="controls">
		              	<select  data-rules="{required:true}"  name="is_important" class="input-normal">
			                <option value="">请选择</option>
			                <option value="1">否</option>
			                <option value="2">是</option>
		              	</select>
		            </div>
	            </div>
        </div>
       	<div class="row">
	        	<div class="control-group span8">
		        	<label class="control-label"><s>*</s>是否难点</label>
		            <div class="controls">
		              	<select  data-rules="{required:true}"  name="is_difficult" class="input-normal">
			                <option value="">请选择</option>
			                <option value="1">否</option>
			                <option value="2">是</option>
		              	</select>
		            </div>
	            </div>
        </div>
        
        </div>
      </form>
    </div>

 <script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script>
  <script type="text/javascript" src="../scripts/ajaxfileupload.js"></script>
  <script type="text/javascript" src="../assets/js/bui-min.js"></script>
  <script src="http://g.alicdn.com/bui/seajs/2.3.0/sea.js"></script>
  <script type="text/javascript" src="../assets/js/config-min.js"></script>

  <!-- 以上一行之前没有 这样在360浏览器 执行Search.createStore时候 报错  -->
  <script type="text/javascript" src="../assets/js/common/search-min.js"></script>


  <script src="http://g.alicdn.com/bui/seajs/2.3.0/sea.js"></script>
  <script src="http://g.alicdn.com/bui/bui/1.1.21/config.js"></script>


  <script type="text/javascript">
    BUI.use('common/page');
  </script>

<script type="text/javascript">


  BUI.use('common/search',function (Search) {

      editing = new BUI.Grid.Plugins.DialogEditing({
        contentId : 'content', //设置隐藏的Dialog内容
        autoSave : true, //添加数据或者修改数据时，自动保存
        triggerCls : 'btn-edit'
      }),

      columns = [
          //{title:'编号',dataIndex:'num',width:50},
          {title:'名称',dataIndex:'name',width:200,renderer:function(value){
        	  	var length = value.length;
        	  	
        	  	if(length > 15)
        	  	{
        	  		return value.substr(0,10)+"...";
        	  	}
        	  	
        	  	return value;
          }},
         /*  {title:'顺序',dataIndex:'sequence',width:50}, */
          {title:'课程',dataIndex:'course_name',width:400},
          {title:'难点',dataIndex:'is_difficult',width:50,renderer:BUI.Grid.Format.enumRenderer({'1':'是','2':'否'})},
          {title:'重点',dataIndex:'is_important',width:50,renderer:BUI.Grid.Format.enumRenderer({'1':'是','2':'否'})},
          {title:'操作',dataIndex:'',width:200,renderer : function(value,obj){
        	  	  
	        	  editStr1 = '<span class="grid-command btn-edit" title="编辑">编辑</span>',
		      delStr = '<span class="grid-command btn-del" title="删除">删除</span>';//页面操作不需要使用Search.createLink
	          
	          return editStr1 + delStr;
             
          }}
        ],
      store = Search.createStore('${pageContext.request.contextPath }/chapter/getAllChapter',{
        proxy : {
          save : { //也可以是一个字符串，那么增删改，都会往那么路径提交数据，同时附加参数saveType
            addUrl : "${pageContext.request.contextPath }/chapter/addChapter",
            updateUrl : "${pageContext.request.contextPath }/chapter/updateChapter",
            removeUrl : "${pageContext.request.contextPath }/chapter/delete"
          },
          method : 'POST',
        },
        autoSync : true ,//保存数据后，自动更新
        pageSize:10
      });
      
     
    var  gridCfg = Search.createGridCfg(columns,{
         tbar : {
           items : [
             {text : '<i class="icon-plus"></i>新建',btnCls : 'button button-small',handler:addFunction},
             {text : '<i class="icon-remove"></i>删除',btnCls : 'button button-small',handler : delFunction},
             {text : '<i class="icon-upload"></i>批量导入',btnCls : 'button button-small',handler : F_Open_dialog},
             {text : '<i class="icon-download"></i>下载模板',btnCls : 'button button-small',handler : download_temp}
           ]
         },
         emptyDataTpl:'<div class="centered"><img alt="Crying" src="__PUBLIC__/img/nodata.png"><h2>查询的数据不存在</h2></div>',
         plugins : [editing,BUI.Grid.Plugins.CheckSelection,BUI.Grid.Plugins.AutoFit] // 插件形式引入多选表格

    });
      
    var  search = new Search({
        store : store,
        gridCfg : gridCfg
      }),
      grid = search.get('grid');

    function addFunction(){
      var newData = {isNew : true}; //标志是新增加的记录
      editing.add(newData,'name'); //添加记录后，直接编辑
    }

    //删除操作
    function delFunction(){
      var selections = grid.getSelection();
      delItems(selections);
    }


    //批量导入
    function F_Open_dialog() 
	{ 
		document.getElementById("btn_file").click(); 
	}
    
  //下载模板
	function download_temp(){
		document.getElementById("download_href").click();
	}
	
    function delItems(items){
      var ids = [];
      BUI.each(items,function(item){
        ids.push(item.id);
      });

      if(ids.length){
        BUI.Message.Confirm('确认要删除选中的记录么？',function(){
          store.save('remove',{ids : ids});
        },'question');
      }
    }

    //监听事件，删除一条记录
    grid.on('cellclick',function(ev){
      var sender = $(ev.domTarget); //点击的Dom
      if(sender.hasClass('btn-del')){
        var record = ev.record;
        delItems([record]);
      }
    });
  });


</script>
 
<script>
	function uploadFile(obj) {  
		
	    $.ajaxFileUpload({  
	        url : "${pageContext.request.contextPath }/chapter/importChapters",  
	        secureuri : false,  
		    fileElementId : 'btn_file',  
		    success : function(res, status) { //服务器成功响应处理函数  
		        if (status) {  
		            alert("导入成功");
		            window.location.reload();
		        }
		    },  
		    error : function(res, status, e) {//服务器响应失败处理函数  
		            alert("导入数据异常：文件导入过程异常。");  
		    }  
		      
	    });  
	    return false;  
	} 
</script>
	

</body>
</html>
