<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE HTML>
<html>
 <head>
  <title> 编辑学生信息</title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
       <link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />   <!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
   <link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
   <style type="text/css">
    code {
      padding: 0px 4px;
      color: #d14;
      background-color: #f7f7f9;
      border: 1px solid #e1e1e8;
    }
   </style>
 </head>
 <body>
  
  <div class="container">
    <form id ="J_Form" class="form-horizontal" action="${pageContext.request.contextPath }/titles/updateTitles" method="POST">
    	   <input type="hidden" name="id" value="${title.id }">
       <div class="row">
          <div class="bui-form-group-select" data-url="${pageContext.request.contextPath }/course/getAllCourseChapterKnowledge">
		  <label>课程章节知识点联动：</label>
		  <select class="input-small" name="courseId" value="cou${title.courseId }">
		    <option>请选择课程</option>
		  </select>
		  <select class="input-small"  name="chapterId"  value="cha${title.chapterId }" ><option>请选择章</option></select>
		  <select class="input-small"  name="knowledgeId" value="kno${title.knowledgeId }" ><option>请选择节|知识点</option></select>
		</div>
        </div>
     
      <div class="row">
          <div class="control-group span12">
            <label class="control-label"><s>*</s>类别</label>
            <div class="controls">
				<select  data-rules="{required:true}"  name="category" class="input-normal" value="${title.category }">
	                <option value="0">请选择</option>
	                <option value="1">单选题</option>
	                <option value="2">填空题</option>
	                <option value="3">简答题</option>
	                <option value="4">程序题</option>
              	</select>
            </div>
          </div>
           	<div class="control-group span12">
		        	<label class="control-label"><s>*</s>难度</label>
		            <div class="controls">
		              	<select  data-rules="{required:true}"  name="difficulty" class="input-normal" value="${title.difficulty }">
			                <option value="0">请选择</option>
			                <option value="1">简单</option>
			                <option value="2">中等</option>
			                 <option value="3">难</option>
		              	</select>
		            </div>
	            </div>
        </div>
        
     
      <div class="row">
        		<div class="control-group span12">
            <label class="control-label">题目：</label>
            <div class="controls control-row4">
              <textarea name="title" value="${title.title }" class="input-large bui-form-field" data-tip="{text:'请填写备注信息！'}" type="text" aria-disabled="false" aria-pressed="false"></textarea>
            <div class="bui-form-tip bui-overlay bui-ext-position x-align-cl-cl" aria-disabled="false" style="visibility: visible; left: 140px; top: 160px; display: block;"><span class=""></span><span class="tip-text"></span></div></div>
          </div> 
                		<div class="control-group span12">
            <label class="control-label">答案：</label>
            <div class="controls control-row4">
              <textarea name="answer" value="${title.answer }" class="input-large bui-form-field" data-tip="{text:'请填写备注信息！'}" type="text" aria-disabled="false" aria-pressed="false"></textarea>
            <div class="bui-form-tip bui-overlay bui-ext-position x-align-cl-cl" aria-disabled="false" style="visibility: visible; left: 140px; top: 160px; display: block;"><span class=""></span><span class="tip-text"></span></div></div>
          </div> 
        </div>
        
      <hr/>
      <div class="row">
        		<div class="control-group span12">
            <label class="control-label">分析：</label>
            <div class="controls control-row4">
              <textarea name="analysis" value="${title.analysis }" class="input-large bui-form-field" data-tip="{text:'请填写备注信息！'}" type="text" aria-disabled="false" aria-pressed="false"></textarea>
            <div class="bui-form-tip bui-overlay bui-ext-position x-align-cl-cl" aria-disabled="false" style="visibility: visible; left: 140px; top: 160px; display: block;"><span class=""></span><span class="tip-text"></span></div></div>
          </div> 
          	<div class="control-group span12">
		        	<label class="control-label"><s>*</s>是否有序</label>
		            <div class="controls">
		              	<select  data-rules="{required:true}"  name="difficulty" class="input-normal" value="${title.difficulty }">
			                <option value="0">请选择</option>
			                <option value="1">简单</option>
			                <option value="2">中等</option>
			                 <option value="3">难</option>
		              	</select>
		            </div>
	            </div>
        </div>
       
         <div class="row">
        		<div class="control-group span12">
            <label class="control-label">选项A：</label>
            <div class="controls control-row4">
              <textarea name="sectiona"  value="${title.sectiona }" class="input-large bui-form-field" data-tip="{text:'请填写备注信息！'}" type="text" aria-disabled="false" aria-pressed="false"></textarea>
            <div class="bui-form-tip bui-overlay bui-ext-position x-align-cl-cl" aria-disabled="false" style="visibility: visible; left: 140px; top: 160px; display: block;"><span class=""></span><span class="tip-text"></span></div></div>
          </div> 
      
        		<div class="control-group span12">
            <label class="control-label">选项B：</label>
            <div class="controls control-row4">
              <textarea name="sectionb" value="${title.sectionb }" class="input-large bui-form-field" data-tip="{text:'请填写备注信息！'}" type="text" aria-disabled="false" aria-pressed="false"></textarea>
            <div class="bui-form-tip bui-overlay bui-ext-position x-align-cl-cl" aria-disabled="false" style="visibility: visible; left: 140px; top: 160px; display: block;"><span class=""></span><span class="tip-text"></span></div></div>
          </div> 
        </div>
         <div class="row">
        		<div class="control-group span12">
            <label class="control-label">选项C：</label>
            <div class="controls control-row4">
              <textarea name="sectionc" value="${title.sectionc }" class="input-large bui-form-field" data-tip="{text:'请填写备注信息！'}" type="text" aria-disabled="false" aria-pressed="false"></textarea>
            <div class="bui-form-tip bui-overlay bui-ext-position x-align-cl-cl" aria-disabled="false" style="visibility: visible; left: 140px; top: 160px; display: block;"><span class=""></span><span class="tip-text"></span></div></div>
          </div> 
       
        		<div class="control-group span12">
            <label class="control-label">选项D：</label>
            <div class="controls control-row4">
              <textarea name="sectiond" value="${title.sectiond }" class="input-large bui-form-field" data-tip="{text:'请填写备注信息！'}" type="text" aria-disabled="false" aria-pressed="false"></textarea>
            <div class="bui-form-tip bui-overlay bui-ext-position x-align-cl-cl" aria-disabled="false" style="visibility: visible; left: 140px; top: 160px; display: block;"><span class=""></span><span class="tip-text"></span></div></div>
          </div> 
        </div>
        
        <div class="row">
        <div class="form-actions offset3">
          <button type="submit" class="button button-primary">保存</button>
          <button class="button page-action jx-btn" data-type="close" >关闭</button>
        </div> 
        
      </div>
    </form>
  
  </div>
  <script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script>
  <script type="text/javascript" src="../assets/js/bui-min.js"></script>

  <script type="text/javascript" src="../assets/js/config-min.js"></script>
  <script type="text/javascript">
    BUI.use('common/page');
  </script>
  <script type="text/javascript">
    BUI.use('bui/form',function (Form) {
      var form = new Form.HForm({
        srcNode : '#J_Form'
      }).render();
    });
  </script>

<body>
</html>  