<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="http://g.alicdn.com/bui/bui/1.1.21/css/bs3/dpl.css" rel="stylesheet">
  <link href="http://g.alicdn.com/bui/bui/1.1.21/css/bs3/bui.css" rel="stylesheet">
    <!-- <link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" /> -->
    <link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />   


   <style type="text/css">
    code {
      padding: 0px 4px;
      color: #d14;
      background-color: #f7f7f9;
      border: 1px solid #e1e1e8;
    }
   </style>
 </head>
 <body>

  <div class="container">
  	<a id="download_href" href="${pageContext.request.contextPath }/resourse/import_timetablesInfo_file.xlsx" style="display:none"></a>
  	<form enctype="multipart/form-data">
  		<input type="file" id="btn_file" style="display:none" onchange="uploadFile(this)" name="excelFile">
    </form>
    <div class="row">
      <form id="searchForm" class="form-horizontal span24">
        <div class="row">

          <div class="control-group span8">
            <label class="control-label">班级编号：</label>
            <div class="controls">
              <input type="text" class="control-text" name="classNum">
            </div>
          </div>
          <div class="control-group span8">
            <label class="control-label">课程编号：</label>
            <div class="controls">
              <input type="text" class="control-text" name="courseNum">
            </div>
          </div>
          <div class="span3 offset2">
            <button  type="button" id="btnSearch" class="button button-small button-primary" style="height:25px;width:50px">搜索</button>
          </div>
        </div>
      </form>
    </div>
    <div class="search-grid-container">
      <div id="grid"></div>
    </div>

  </div>
  <div id="content" class="hide">
      <form id="J_Form" class="form-horizontal" action="<{:U('university/add')}>">
       <!--  <input type="hidden" name="id"> -->
        <!-- <div class="row">
          <div class="control-group span8">
            <label class="control-label"><s>*</s>知识点编号</label>
            <div class="controls">
              <input name="num" type="text" data-rules="{required:true}" class="input-normal control-text">
            </div>
          </div>
        </div> -->
        <div class="row">
          <div class="control-group span8">
            <label class="control-label"><s>*</s>知识点名称</label>
            <div class="controls">
              <input name="name" type="text" data-rules="{required:true}" class="input-normal control-text">
            </div>
          </div>
        </div>
       <div class="row">
	        	<div class="control-group span8">
		        	<label class="control-label"><s>*</s>是否重点</label>
		            <div class="controls">
		              	<select  data-rules="{required:true}"  name="is_important" class="input-normal">
			                <option value="">请选择</option>
			                <option value="0">否</option>
			                <option value="1">是</option>
		              	</select>
		            </div>
	            </div>
        </div>
        <div class="row">
	        	<div class="control-group span8">
		        	<label class="control-label"><s>*</s>是否难点</label>
		            <div class="controls">
		              	<select  data-rules="{required:true}"  name="is_difficult" class="input-normal">
			                <option value="">请选择</option>
			                <option value="0">否</option>
			                <option value="1">是</option>
		              	</select>
		            </div>
	            </div>
        </div>
        <div class="row">
          <div class="control-group span8">
            <label class="control-label"><s>*</s>上传视频</label>
            <div class="controls">
            	<div id="J_Uploader">
      			</div>
              	<input name="video" type="hidden" class="videourl">
            </div>
          </div>
        </div>
      </form>
    </div>
    
	
 <script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script>
  <script type="text/javascript" src="../scripts/ajaxfileupload.js"></script>
  <script type="text/javascript" src="../assets/js/bui-min.js"></script>
  <script src="http://g.alicdn.com/bui/seajs/2.3.0/sea.js"></script>
 <!--  <script src="http://g.alicdn.com/bui/bui/1.1.21/config.js"></script> -->
  <script type="text/javascript" src="../assets/js/config-min.js"></script>

  <!-- 以上一行之前没有 这样在360浏览器 执行Search.createStore时候 报错  -->
  <script type="text/javascript" src="../assets/js/common/search-min.js"></script>


  <script src="http://g.alicdn.com/bui/seajs/2.3.0/sea.js"></script>
  <script src="http://g.alicdn.com/bui/bui/1.1.21/config.js"></script>


  <script type="text/javascript">
    BUI.use('common/page');
  </script>

<script type="text/javascript">
	/* $('body').on('click','.btn-grant',function(){
		var videoURL = $(this).attr('videoURL');
		
		$('#showVideo').attr("src" , videoURL);
		
	}) */


  BUI.use('common/search',function (Search) {

      editing = new BUI.Grid.Plugins.DialogEditing({
        contentId : 'content', //设置隐藏的Dialog内容
        autoSave : true, //添加数据或者修改数据时，自动保存
        triggerCls : 'btn-edit'
      }),

      
      columns = [
          //{title:'编号',dataIndex:'num',width:50},
          {title:'学年',dataIndex:'year',width:100}, 
          {title:'学期',dataIndex:'term',width:100},
          {title:'课程编号',dataIndex:'courseNum',width:100,renderer:function(value){
        	  	var length = value.length;
        	  	
        	  	if(length > 15)
        	  	{
        	  		return value.substr(0,10)+"...";
        	  	}
        	  	
        	  	return value;
          }},
         /*  {title:'顺序',dataIndex:'sequence',width:50}, */
          
          {title:'班级编号',dataIndex:'classNum',width:100},
          {title:'教室',dataIndex:'classRoomNum',width:100},
          {title:'开始周',dataIndex:'fromWeek',width:80}, 
          {title:'结束周',dataIndex:'toWeek',width:80}, 
          {title:'单|双',dataIndex:'weekCycle',width:80}, 
          {title:'课程表',dataIndex:'courseArrangement',width:600}
         
        ],
      store = Search.createStore('${pageContext.request.contextPath }/timeTables/getAllTimeTables',{
        proxy : {
          save : { //也可以是一个字符串，那么增删改，都会往那么路径提交数据，同时附加参数saveType
            addUrl : "${pageContext.request.contextPath }/timetables/addTimetables",
            updateUrl : "${pageContext.request.contextPath }/timetables/updateTimetables",
            removeUrl : "${pageContext.request.contextPath }/timetables/delete"
          },
          method : 'GET',
        },
        autoSync : true ,//保存数据后，自动更新
        pageSize:10
      });
      
     
    var  gridCfg = Search.createGridCfg(columns,{
         tbar : {
           items : [
             {text : '<i class="icon-upload"></i>导入课程表',btnCls : 'button button-small',handler : F_Open_dialog},
             {text : '<i class="icon-download"></i>下载模板',btnCls : 'button button-small',handler : download_temp}
           ]
         },
         emptyDataTpl:'<div class="centered"><img alt="Crying" src="__PUBLIC__/img/nodata.png"><h2>查询的数据不存在</h2></div>',
         plugins : [editing,BUI.Grid.Plugins.CheckSelection,BUI.Grid.Plugins.AutoFit] // 插件形式引入多选表格

    });
      
    var  search = new Search({
        store : store,
        gridCfg : gridCfg
      }),
      grid = search.get('grid');

    function addFunction(){
      var newData = {isNew : true}; //标志是新增加的记录
      editing.add(newData,'name'); //添加记录后，直接编辑
    }

    //删除操作
    function delFunction(){
      var selections = grid.getSelection();
      delItems(selections);
    }

    //批量导入
    function F_Open_dialog() 
	{ 
		document.getElementById("btn_file").click(); 
	}
	
	//下载模板
	function download_temp(){
		document.getElementById("download_href").click();
	}
	
    function delItems(items){
      var ids = [];
      BUI.each(items,function(item){
        ids.push(item.id);
      });

      if(ids.length){
        BUI.Message.Confirm('确认要删除选中的记录么？',function(){
          store.save('remove',{ids : ids});
        },'question');
      }
    }

    //监听事件，删除一条记录
    grid.on('cellclick',function(ev){
      var sender = $(ev.domTarget); //点击的Dom
      if(sender.hasClass('btn-del')){
        var record = ev.record;
        delItems([record]);
      }
    });
  });


</script>
 <script type="text/javascript">
        BUI.use('bui/uploader',function (Uploader) {
          
      /**
       * 返回数据的格式
       *
       *  默认是 {url : 'url'},否则认为上传失败
       *  可以通过isSuccess 更改判定成功失败的结构
       */
      var uploader = new Uploader.Uploader({
        render: '#J_Uploader',
        url: '${pageContext.request.contextPath }/timetables/upload/',
        //rules: {
	         //文的类型
	         //ext: ['.mp4','文件类型只能为{0}'],
	         //上传的最大个数
	         //max: [1, '文件的最大个数不能超过{0}个'],
	         //文件大小的最小值,这个单位是kb
	         //minSize: [10, '文件的大小不能小于{0}KB'],
	         //文件大小的最大值,单位也是kb
	         //maxSize: [2048, '文件大小不能大于2M']
	    //},
	    //根据业务需求来判断上传是否成功
	    isSuccess: function(result){
	    	
	    	//console.log(JSON.parse(result));
	        if(result && result.url){
	        		//alert(result.url);
	        		
	        		$(".videourl").val(result.url);
	        		
	            BUI.Message.Alert("上传成功" );
	            
	        }else{
	        		BUI.Message.Alert(result);
	            BUI.Message.Alert("上传失败");
	        } 
	    }
      }).render();
    });
    </script>
    
    <script>
		function uploadFile(obj) {  
			
		    $.ajaxFileUpload({  
		        url : "${pageContext.request.contextPath }/timeTables/importTimeTables",  
		        secureuri : false,  
			    fileElementId : 'btn_file',  
			    success : function(res, status) { //服务器成功响应处理函数  
			        if (status) {  
			            alert("导入成功");
			            window.location.reload();
			        }
			    },  
			    error : function(res, status, e) {//服务器响应失败处理函数  
			            alert("导入数据异常：文件导入过程异常。");  
			    }  
			      
		    });  
		    return false;  
		} 
	</script>

</body>
</html>
