<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />   


   <style type="text/css">
    code {
      padding: 0px 4px;
      color: #d14;
      background-color: #f7f7f9;
      border: 1px solid #e1e1e8;
    }
   </style>
 </head>
 <body>

  <div class="container">
    <a id="download_href" href="${pageContext.request.contextPath }/resourse/import_titlesInfo_file.xls" style="display:none"></a>
  	<form enctype="multipart/form-data">
  		<input type="file" id="btn_file" style="display:none" onchange="uploadFile(this)" name="excelFile">
    </form>
    <div class="row">
      <form id="searchForm" class="form-horizontal span24">
        <div class="row">

          <div class="control-group span8">
            <label class="control-label">名称：</label>
            <div class="controls">
              <input type="text" class="control-text" name="searchname">
            </div>
          </div>
          <div class="span3 offset2">
            <button  type="button" id="btnSearch" class="button button-small button-primary">搜索</button>
          </div>
        </div>
      </form>
    </div>
    <div class="search-grid-container">
      <div id="grid"></div>
    </div>

  </div>
  <div id="content" class="hide">
      <form id="J_Form" class="form-horizontal" >
        <div class="row">
          <div class="bui-form-group-select" data-url="${pageContext.request.contextPath }/course/getAllCourseChapterKnowledge">
		  <label>课程章节知识点联动：</label>
		  <select class="input-small" name="courseId">
		    <option>请选择课程</option>
		  </select>
		  <select class="input-small"  name="chapterId" ><option>请选择章</option></select>
		  <select class="input-small"  name="knowledgeId" ><option>请选择节|知识点</option></select>
		</div>
        </div>
        <div class="row">
          <div class="control-group span12">
            <label class="control-label"><s>*</s>类别</label>
            <div class="controls">
				<select  data-rules="{required:true}"  name="category" class="input-normal">
	                <option value="0">请选择</option>
	                <option value="1">单选题</option>
	                <option value="2">填空题</option>
	                <option value="3">简答题</option>
	                <option value="4">程序题</option>
              	</select>
            </div>
          </div>
           	<div class="control-group span12">
		        	<label class="control-label"><s>*</s>难度</label>
		            <div class="controls">
		              	<select  data-rules="{required:true}"  name="difficulty" class="input-normal">
			                <option value="0">请选择</option>
			                <option value="1">简单</option>
			                <option value="2">中等</option>
			                 <option value="3">难</option>
		              	</select>
		            </div>
	            </div>
        </div>
        <div class="row">
        		<div class="control-group span12">
            <label class="control-label">题目：</label>
            <div class="controls control-row4">
              <textarea name="title" class="input-large bui-form-field" data-tip="{text:'请填写备注信息！'}" type="text" aria-disabled="false" aria-pressed="false"></textarea>
            <div class="bui-form-tip bui-overlay bui-ext-position x-align-cl-cl" aria-disabled="false" style="visibility: visible; left: 140px; top: 160px; display: block;"><span class=""></span><span class="tip-text"></span></div></div>
          </div> 
                		<div class="control-group span12">
            <label class="control-label">答案：</label>
            <div class="controls control-row4">
              <textarea name="answer" class="input-large bui-form-field" data-tip="{text:'请填写备注信息！'}" type="text" aria-disabled="false" aria-pressed="false"></textarea>
            <div class="bui-form-tip bui-overlay bui-ext-position x-align-cl-cl" aria-disabled="false" style="visibility: visible; left: 140px; top: 160px; display: block;"><span class=""></span><span class="tip-text"></span></div></div>
          </div> 
        </div>
         <div class="row">
        		<div class="control-group span12">
            <label class="control-label">分析：</label>
            <div class="controls control-row4">
              <textarea name="analysis" class="input-large bui-form-field" data-tip="{text:'请填写备注信息！'}" type="text" aria-disabled="false" aria-pressed="false"></textarea>
            <div class="bui-form-tip bui-overlay bui-ext-position x-align-cl-cl" aria-disabled="false" style="visibility: visible; left: 140px; top: 160px; display: block;"><span class=""></span><span class="tip-text"></span></div></div>
          </div> 
          <div class="control-group span12">
		        	<label class="control-label"><s>*</s>是否有序</label>
		            <div class="controls">
		              	<select  data-rules="{required:true}"  name="difficulty" class="input-normal" value="${title.difficulty }">
			                <option value="0">请选择</option>
			                <option value="1">简单</option>
			                <option value="2">中等</option>
			                 <option value="3">难</option>
		              	</select>
		            </div>
	            </div>
        </div>
       
         <div class="row">
        		<div class="control-group span12">
            <label class="control-label">选项A：</label>
            <div class="controls control-row4">
              <textarea name="sectiona" class="input-large bui-form-field" data-tip="{text:'请填写备注信息！'}" type="text" aria-disabled="false" aria-pressed="false"></textarea>
            <div class="bui-form-tip bui-overlay bui-ext-position x-align-cl-cl" aria-disabled="false" style="visibility: visible; left: 140px; top: 160px; display: block;"><span class=""></span><span class="tip-text"></span></div></div>
          </div> 
      
        		<div class="control-group span12">
            <label class="control-label">选项B：</label>
            <div class="controls control-row4">
              <textarea name="sectionb" class="input-large bui-form-field" data-tip="{text:'请填写备注信息！'}" type="text" aria-disabled="false" aria-pressed="false"></textarea>
            <div class="bui-form-tip bui-overlay bui-ext-position x-align-cl-cl" aria-disabled="false" style="visibility: visible; left: 140px; top: 160px; display: block;"><span class=""></span><span class="tip-text"></span></div></div>
          </div> 
        </div>
         <div class="row">
        		<div class="control-group span12">
            <label class="control-label">选项C：</label>
            <div class="controls control-row4">
              <textarea name="sectionc" class="input-large bui-form-field" data-tip="{text:'请填写备注信息！'}" type="text" aria-disabled="false" aria-pressed="false"></textarea>
            <div class="bui-form-tip bui-overlay bui-ext-position x-align-cl-cl" aria-disabled="false" style="visibility: visible; left: 140px; top: 160px; display: block;"><span class=""></span><span class="tip-text"></span></div></div>
          </div> 
       
        		<div class="control-group span12">
            <label class="control-label">选项D：</label>
            <div class="controls control-row4">
              <textarea name="sectiond" class="input-large bui-form-field" data-tip="{text:'请填写备注信息！'}" type="text" aria-disabled="false" aria-pressed="false"></textarea>
            <div class="bui-form-tip bui-overlay bui-ext-position x-align-cl-cl" aria-disabled="false" style="visibility: visible; left: 140px; top: 160px; display: block;"><span class=""></span><span class="tip-text"></span></div></div>
          </div> 
        </div>
      </form>
    </div>
   

  <script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script>
  <script type="text/javascript" src="../scripts/ajaxfileupload.js"></script>
  <script type="text/javascript" src="../assets/js/bui-min.js"></script>
  <script src="http://g.alicdn.com/bui/seajs/2.3.0/sea.js"></script>
 <!--  <script src="http://g.alicdn.com/bui/bui/1.1.21/config.js"></script> -->
  <script type="text/javascript" src="../assets/js/config-min.js"></script>

  <!-- 以上一行之前没有 这样在360浏览器 执行Search.createStore时候 报错  -->
  <script type="text/javascript" src="../assets/js/common/search-min.js"></script>


  <script src="http://g.alicdn.com/bui/seajs/2.3.0/sea.js"></script>
  <script src="http://g.alicdn.com/bui/bui/1.1.21/config.js"></script>




  <script type="text/javascript">
    BUI.use('common/page');
  </script>

<script type="text/javascript">
  BUI.use(['common/search' ],function (Search) {

      editing = new BUI.Grid.Plugins.DialogEditing({
        contentId : 'content', //设置隐藏的Dialog内容
        autoSave : true, //添加数据或者修改数据时，自动保存
        triggerCls : 'btn-edit'
      }),

      columns = [
         /*  {title:'编号',dataIndex:'num',width:150}, */
          //题的类型：0单选1多选2判断3填空4程序5画图6简答
           {title:'类型',dataIndex:'category',width:50,renderer:BUI.Grid.Format.enumRenderer({'1':'单选','2':'填空','3':'简答','4':'程序'})},
           {title:'题目',dataIndex:'title',width:600 , renderer : function(value , obj){
     	  		
     	  		var length = value.length;
     	  		if(length < 60)
     	  		{
     	  			return value;
     	  		}else
     	  		{
     	  			return value.substring(0,60)+"...";
     	  		}	  	
       		}},
           {title:'难度',dataIndex:'difficulty',width:40,renderer:BUI.Grid.Format.enumRenderer({'1':'初级','2':'中等','3':'难'})},
           //{title:'答案',dataIndex:'answer',width:40},
           {title:'知识点',dataIndex:'knowledgeName',width:200},
          {title:'操作',dataIndex:'roles',width:200,renderer : function(value,obj){
        	  //console.log(obj);
        	 
            	 var editStr =  Search.createLink({ //链接使用 此方式
                id : obj.id,
                title : '编辑题目',
                text : '编辑',
                href : '${pageContext.request.contextPath }/titles/updateTitles?id='+obj.id
              });
              //delStr = '<span class="grid-command btn-del" title="删除">删除</span>';//页面操作不需要使用Search.createLink
              
              return editStr;
              
          }}
        ],
      store = Search.createStore('${pageContext.request.contextPath }/titles/getAllTitles',{
        proxy : {
          save : { //也可以是一个字符串，那么增删改，都会往那么路径提交数据，同时附加参数saveType
            addUrl : "${pageContext.request.contextPath }/titles/addTitles",
            updateUrl : "${pageContext.request.contextPath }/titles/updateTitles",
            removeUrl : "${pageContext.request.contextPath }/titles/deleteTitles"
          },
          method : 'POST',
        },
        autoSync : true ,//保存数据后，自动更新
        pageSize:10
      });
      
      var  gridCfg = Search.createGridCfg(columns,{
          tbar : {
            items : [
              {text : '<i class="icon-plus"></i>新建',btnCls : 'button button-small',handler:addFunction},
              //{text : '<i class="icon-remove"></i>删除',btnCls : 'button button-small',handler : delFunction},
              {text : '<i class="icon-upload"></i>批量导入',btnCls : 'button button-small',handler : F_Open_dialog},
              {text : '<i class="icon-download"></i>下载模板',btnCls : 'button button-small',handler : download_temp}
            ]
          },
          emptyDataTpl:'<div class="centered"><img alt="Crying" src="__PUBLIC__/img/nodata.png"><h2>查询的数据不存在</h2></div>',
          plugins : [editing,BUI.Grid.Plugins.CheckSelection,BUI.Grid.Plugins.AutoFit] // 插件形式引入多选表格

     });
      
    var  search = new Search({
        store : store,
        gridCfg : gridCfg
      }),
      grid = search.get('grid');

    function addFunction(){
    	 
      var newData = {isNew : true}; //标志是新增加的记录
      editing.add(newData,'name'); //添加记录后，直接编辑
    }

    //删除操作
    function delFunction(){
      var selections = grid.getSelection();
      delItems(selections);
    }
    
 
   
    //批量导入
    function F_Open_dialog() 
	{ 
		document.getElementById("btn_file").click(); 
	}
	
	//下载模板
	function download_temp(){
		document.getElementById("download_href").click();
	}

    function delItems(items){
      var ids = [];
      BUI.each(items,function(item){
        ids.push(item.id);
      });

      if(ids.length){
        BUI.Message.Confirm('确认要删除选中的记录么？',function(){
          store.save('remove',{ids : ids});
        },'question');
      }
    }

    //监听事件，删除一条记录
    grid.on('cellclick',function(ev){
      var sender = $(ev.domTarget); //点击的Dom
      if(sender.hasClass('btn-del')){
        var record = ev.record;
        delItems([record]);
      }
    });
  });
  
  
  

</script>
	
	 <script>
		function uploadFile(obj) {  
			
		    $.ajaxFileUpload({  
		        url : "${pageContext.request.contextPath }/titles/importTitles",  
		        secureuri : false,  
			    fileElementId : 'btn_file',  
			    success : function(res, status) { //服务器成功响应处理函数  
			        if (status) {  
			            alert("导入成功");
			            window.location.reload();
			        }
			    },  
			    error : function(res, status, e) {//服务器响应失败处理函数  
			            alert("导入数据异常：文件导入过程异常。");  
			    }  
			      
		    });  
		    return false;  
		} 
	</script>
	
</body>
</html>
