<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />   


 </head>
 <body>
 	
 	<div class="container">
    <div class="row">
      <form id="J_Form" class="form-horizontal span24" action="${pageContext.request.contextPath }/user/updatePassword" method="POST">
       	<input type="hidden" name="id" value="${userinfo.id }" >
       	<input type="hidden" name="username" value="${userinfo.username }">
        <div class="row">
          <div class="control-group span8">
            <label class="control-label"><s>*</s>密码：</label>
            <div class="controls">
              <input name="password" type="password" data-rules="{required:true}" class="input-normal control-text">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="control-group span8">
            <label class="control-label"><s>*</s>确认密码：</label>
            <div class="controls">
              <input name="repassword"  type="password" data-rules="{required:true}" class="input-normal control-text">
            </div>
          </div>
        </div>
        <div class="row form-actions actions-bar">
            <div class="span13 offset3 ">
              <button type="submit" class="button button-primary">保存</button>
              <button type="reset" class="button">重置</button>
            </div>
        </div>
      </form>
    </div>
    

 	<h1>${updatePasswordResult }</h1>
	
	<script type="text/javascript">
	    BUI.use('common/page'); //页面链接跳转
	 
	    BUI.use('bui/form',function (Form) {
	      var form = new Form.HForm({
	        srcNode : '#J_Form'
	      });
	 
	      form.render();
	    });
	  </script>
</body>
</html>
