<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="http://g.alicdn.com/bui/bui/1.1.21/css/bs3/dpl.css" rel="stylesheet">
  <link href="http://g.alicdn.com/bui/bui/1.1.21/css/bs3/bui.css" rel="stylesheet">
    <!-- <link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" /> -->
    <link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />   


   <style type="text/css">
    code {
      padding: 0px 4px;
      color: #d14;
      background-color: #f7f7f9;
      border: 1px solid #e1e1e8;
    }
   </style>
 </head>
 <body>

  <div id="timer" style="color:red;position:fixed;left:700px;font-size:20px;"></div>
<br>
	
	<h1>第一大题：单选题</h1>
	
	<c:forEach items="${requestScope.titles }" var="title" varStatus="stat">
		<c:if test="${title.category==1 }">
			<h2>${stat.count}、${title.title }----------<a href="${pageContext.request.contextPath }/pages/changeTitle/pagesId/${pagesId}/courseId/${title.courseId}/chapterId/${title.chapterId}/tId/${title.id}/categoryNum/${title.category}">换一道</a></h2>
			<h3>A ${title.sectiona}</h3>
			<h3>B ${title.sectionb}</h3>
			<h3>C ${title.sectionc}</h3>
			<h3>D ${title.sectiond}</h3>
			<hr color="gray">
			
		</c:if>
	</c:forEach>
	<h1>第二大题：填空题</h1>
	<c:forEach items="${requestScope.titles }" var="title" varStatus="stat">
		<c:if test="${title.category==2 }">
			<h2>${stat.count}、${title.title }----------<a href="${pageContext.request.contextPath }/pages/changeTitle/pagesId/${pagesId}/courseId/${title.courseId}/chapterId/${title.chapterId}/tId/${title.id}/categoryNum/${title.category}">换一道</a></h2>
			<hr color="gray">
		</c:if>
		
	</c:forEach>
	<h1>第三大题：简答题</h1>
	<c:forEach items="${requestScope.titles }" var="title" varStatus="stat">
		
		<c:if test="${title.category==3 }">
			<h2>${stat.count}、${title.title }----------<a href="${pageContext.request.contextPath }/pages/changeTitle/pagesId/${pagesId}/courseId/${title.courseId}/chapterId/${title.chapterId}/tId/${title.id}/categoryNum/${title.category}">换一道</a></h2>
			<hr color="gray">
		</c:if>
		
	</c:forEach>
	<h1>第四大题：代码题</h1>
	<c:forEach items="${requestScope.titles }" var="title" varStatus="stat" >
		<c:if test="${title.category==4 }">
			<h2>${stat.count}、${title.title }----------<a href="${pageContext.request.contextPath }/pages/changeTitle/pagesId/${pagesId}/courseId/${title.courseId}/chapterId/${title.chapterId}/tId/${title.id}/categoryNum/${title.category}">换一道</a></h2>
			<hr color="gray">
		</c:if>
	</c:forEach>
</body>
</html>
