<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="http://g.alicdn.com/bui/bui/1.1.21/css/bs3/dpl.css" rel="stylesheet">
  <link href="http://g.alicdn.com/bui/bui/1.1.21/css/bs3/bui.css" rel="stylesheet">
    <!-- <link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" /> -->
    <link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />   


   <style type="text/css">
    code {
      padding: 0px 4px;
      color: #d14;
      background-color: #f7f7f9;
      border: 1px solid #e1e1e8;
    }
   </style>
 </head>
 <body>

  <div class="container">
  	<a id="download_href" href="${pageContext.request.contextPath }/exam/exportScores" style="display:none"></a>
  	
  
    <div class="row">
      <form id="searchForm" class="form-horizontal span24">
        <div class="row">

          <div class="control-group span8">
            <label class="control-label">学生：</label>
            <div class="controls">
              <input type="text" class="control-text" name="searchname">
            </div>
          </div>
          
        </div>
        <div class="row">
          <div class="control-group span8">
            <label class="control-label">试卷：</label>
            <div class="controls">
              <input type="text" class="control-text" name="pages_id" id="pages_id">
            </div>
          </div>
           <div class="control-group span8">
            <label class="control-label">班级编号：</label>
            <div class="controls">
              <input type="text" class="control-text" name="class_id" id="class_id">
            </div>
          </div>
          <div class="span3 offset2">
            <button  type="button" id="btnSearch" class="button button-small button-primary" style="height:25px;width:50px">搜索</button>
          </div>
        </div>
      </form>
    </div>
    <div class="search-grid-container">
      <div id="grid"></div>
    </div>

  </div>
  

 <script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script>
  <script type="text/javascript" src="../scripts/ajaxfileupload.js"></script>
  <script type="text/javascript" src="../assets/js/bui-min.js"></script>
  <script src="http://g.alicdn.com/bui/seajs/2.3.0/sea.js"></script>
 <!--  <script src="http://g.alicdn.com/bui/bui/1.1.21/config.js"></script> -->
  <script type="text/javascript" src="../assets/js/config-min.js"></script>

  <!-- 以上一行之前没有 这样在360浏览器 执行Search.createStore时候 报错  -->
  <script type="text/javascript" src="../assets/js/common/search-min.js"></script>


  <script src="http://g.alicdn.com/bui/seajs/2.3.0/sea.js"></script>
  <script src="http://g.alicdn.com/bui/bui/1.1.21/config.js"></script>


  <script type="text/javascript">
    BUI.use('common/page');
  </script>

<script type="text/javascript">
	/* $('body').on('click','.btn-grant',function(){
		var videoURL = $(this).attr('videoURL');
		
		$('#showVideo').attr("src" , videoURL);
		
	}) */


  BUI.use('common/search',function (Search) {

      editing = new BUI.Grid.Plugins.DialogEditing({
        contentId : 'content', //设置隐藏的Dialog内容
        autoSave : true, //添加数据或者修改数据时，自动保存
        triggerCls : 'btn-edit'
      }),

      BUI.Editor.DialogEditor

      granting = new BUI.Grid.Plugins.DialogEditing({

          contentId : 'contentForBindRole', //设置隐藏的Dialog内容
          autoSave : true, //添加数据或者修改数据时，自动保存
          triggerCls : 'btn-grant11',
          editor: {
        	  		buttons:[
                            /* { text:'点击绑定',
                              elCls : 'button button-primary',
                              handler : function(){

                            	  //console.log(this.__attrVals.editValue.id);
                            	  //alert($("input:checked").size());

                            	  var university_id = this.__attrVals.editValue.id;
								                
                                var role_ids = "";

                            	  $("input:checked").each(function(){
                            		  role_ids += "," + $(this).val()  ;
                            	  });

                            	  $.post("bind_Roles", { university_id: university_id, role_ids: role_ids },
                            			 function(data){
                            			     alert("绑定成功! ");
                            			     //this.hide();
                            	     }
                                );

                           	  }

                            }, */
                            { "text":'关闭', "elCls" : 'button', "handler" : function(){ this.hide(); } }
                ]
      		},
        }),

      columns = [
    	  	  {title:'id',dataIndex:'id',width:50},
          {title:'试卷',dataIndex:'pagesName',width:200},
          {title:'学生',dataIndex:'studentName',width:100},
          {title:'学号',dataIndex:'studentNumber',width:100},
          {title:'成绩',dataIndex:'score',width:100},
          {title:'时间',dataIndex:'time',width:150,renderer:function(value){
    	  	  
	    	  	  var datetime = new Date();
	          datetime.setTime(value);
	          var year = datetime.getFullYear();
	          var month = datetime.getMonth() + 1;
	          var date = datetime.getDate();
	          var hour = datetime.getHours();
	          var minute = datetime.getMinutes();
	          var second = datetime.getSeconds();
	          return year + "-" + month + "-" + date+" "+hour+":"+minute;
	    	  
	      }},
          {title:'班级',dataIndex:'className',width:100},
          //{title:'均分',dataIndex:'avgScore',width:100},
          /* {title:'操作',dataIndex:'',width:150,renderer : function(value,obj){
              editStr1 = '<span class="grid-command btn-edit" title="编辑">编辑</span>';
            return editStr1;
          }} */
        ],
      store = Search.createStore('${pageContext.request.contextPath }/exam/getAllStudentScore',{
        proxy : {
          save : { //也可以是一个字符串，那么增删改，都会往那么路径提交数据，同时附加参数saveType
            addUrl : "${pageContext.request.contextPath }/exam/addExam"
          },
          method : 'POST',
        },
        autoSync : true ,//保存数据后，自动更新
        pageSize:10
      });
      
     
    var  gridCfg = Search.createGridCfg(columns,{
         tbar : {
           items : [
        	   	{text : '<i class="icon-download"></i>导出成绩',btnCls : 'button button-small',handler : download_temp},
        	   	{text : '<i class="icon-download"></i>查看报表',btnCls : 'button button-small',handler : viewCharts}
             //{text : '<i class="icon-remove"></i>删除',btnCls : 'button button-small',handler : delFunction},
           ]
         },
         emptyDataTpl:'<div class="centered"><img alt="Crying" src="__PUBLIC__/img/nodata.png"><h2>查询的数据不存在</h2></div>',
         plugins : [editing,granting,BUI.Grid.Plugins.CheckSelection,BUI.Grid.Plugins.AutoFit] // 插件形式引入多选表格

    });
      
    var  search = new Search({
        store : store,
        gridCfg : gridCfg
      }),
      grid = search.get('grid');



	//下载模板
	function download_temp(){
		
		var class_id = document.getElementById("class_id").value;
		var pages_id = document.getElementById("pages_id").value;
		
		
		if(class_id != '' && class_id != null)
		{
			if(parseInt(class_id)<1 || parseInt(class_id)>14)
			{
				alert("输入的班级id不对！是id不是编号！");
				return;
			}
			
			location.href="${pageContext.request.contextPath }/exam/exportScores?class_id="+class_id+"&pages_id="+pages_id;
		}else
		{
			alert("班级id必须选！");
		}
		
	}
	
   function viewCharts()
   {
	   var class_id = document.getElementById("class_id").value;
	   var pages_id = document.getElementById("pages_id").value;
	   
	   if(pages_id != '' && pages_id != null)
		{
		   location.href="${pageContext.request.contextPath }/exam/viewCharts?pages_id="+pages_id;
		   
		}else
		{
			alert("试卷必填！");
		}
	   
   }

  });


</script>
 
    
    

</body>
</html>
