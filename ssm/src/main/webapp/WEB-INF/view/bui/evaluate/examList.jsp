<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="http://g.alicdn.com/bui/bui/1.1.21/css/bs3/dpl.css" rel="stylesheet">
  <link href="http://g.alicdn.com/bui/bui/1.1.21/css/bs3/bui.css" rel="stylesheet">
    <!-- <link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" /> -->
    <link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />   

<script type="text/javascript">

function grant_role(value){

    var arrobj = value.split(",");
    $(".roleclass").each(function(i,v){
          $(v).attr('checked',false)
    });
    for(i=0;i<arrobj.length;i++){
      var val=arrobj[i]
      $(".roleclass[value="+val+"]").attr('checked','checked')
    }
   
}

</script>
   <style type="text/css">
    code {
      padding: 0px 4px;
      color: #d14;
      background-color: #f7f7f9;
      border: 1px solid #e1e1e8;
    }
   </style>
 </head>
 <body>

  <div class="container">
  	<a id="download_href" href="${pageContext.request.contextPath }/resourse/import_pagessInfo_file.xlsx" style="display:none"></a>
  	<form enctype="multipart/form-data">
  		<input type="file" id="btn_file" style="display:none" onchange="uploadFile(this)" name="excelFile">
    </form>
    <div class="row">
      <form id="searchForm" class="form-horizontal span24">
        <div class="row">

          <div class="control-group span8">
            <label class="control-label">名称：</label>
            <div class="controls">
              <input type="text" class="control-text" name="searchname">
            </div>
          </div>
          <div class="control-group span8">
            <label class="control-label">所属课程：</label>
            <div class="controls">
              <select name="course_id" class="control-text" style="height:30px">
              	<option value="0">请选择</option>
                
              </select>
            </div>
          </div>
          <div class="span3 offset2">
            <button  type="button" id="btnSearch" class="button button-small button-primary" style="height:25px;width:50px">搜索</button>
          </div>
        </div>
      </form>
    </div>
    <div class="search-grid-container">
      <div id="grid"></div>
    </div>

  </div>
  <div id="content" class="hide">
      <form id="J_Form" class="form-horizontal" action="<{:U('university/add')}>">
       <!--  <input type="hidden" name="id"> -->
        <!-- <div class="row">
          <div class="control-group span8">
            <label class="control-label"><s>*</s>试卷编号</label>
            <div class="controls">
              <input name="num" type="text" data-rules="{required:true}" class="input-normal control-text">
            </div>
          </div>
        </div> -->
        <div class="row">
          <div class="control-group span8">
            <label class="control-label"><s>*</s>试卷编号</label>
            <div class="controls">
              <input name="pagesId" type="text" data-rules="{required:true}" class="input-normal control-text">
            </div>
          </div>
        </div>
       <div class="row">
          <div class="control-group span8">
            <label class="control-label"><s>*</s>班级编号</label>
            <div class="controls">
              <input name="classId" type="text" data-rules="{required:true}" class="input-normal control-text">
            </div>
          </div>
        </div>
         <div class="row">
          <div class="control-group span8">
            <label class="control-label"><s>*</s>开始时间</label>
            <div class="controls">
              <input name="startTime" type="text" data-rules="{datetime:true}" class="calendar calendar-time">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="control-group span8">
            <label class="control-label"><s>*</s>结束时间</label>
            <div class="controls">
              <input name="endTime" type="text" data-rules="{required:true}" class="calendar calendar-time">
            </div>
          </div>
        </div>
      </form>
    </div>
    
	<!-- 绑定角色 id="J_Form_Bind_Role" class="form-horizontal" action="<{:U('university/bindRoles')}>" -->
    <%-- <div id="contentForBindRole" class="hide">
      <form id="J_Form_Bind_Role" class="form-horizontal">
      	<video height="200px" width="200px" controls="controls" >
		  <source id="showVideo" type="video/mp4" />
		  Your browser does not support the video tag.
		</video>
      </form>
    </div> --%>

 <script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script>
  <script type="text/javascript" src="../scripts/ajaxfileupload.js"></script>
  <script type="text/javascript" src="../assets/js/bui-min.js"></script>
  <script src="http://g.alicdn.com/bui/seajs/2.3.0/sea.js"></script>
 <!--  <script src="http://g.alicdn.com/bui/bui/1.1.21/config.js"></script> -->
  <script type="text/javascript" src="../assets/js/config-min.js"></script>

  <!-- 以上一行之前没有 这样在360浏览器 执行Search.createStore时候 报错  -->
  <script type="text/javascript" src="../assets/js/common/search-min.js"></script>


  <script src="http://g.alicdn.com/bui/seajs/2.3.0/sea.js"></script>
  <script src="http://g.alicdn.com/bui/bui/1.1.21/config.js"></script>


  <script type="text/javascript">
    BUI.use('common/page');
  </script>

<script type="text/javascript">
	/* $('body').on('click','.btn-grant',function(){
		var videoURL = $(this).attr('videoURL');
		
		$('#showVideo').attr("src" , videoURL);
		
	}) */


  BUI.use('common/search',function (Search) {

      editing = new BUI.Grid.Plugins.DialogEditing({
        contentId : 'content', //设置隐藏的Dialog内容
        autoSave : true, //添加数据或者修改数据时，自动保存
        triggerCls : 'btn-edit'
      }),

      columns = [
    	  	  {title:'id',dataIndex:'id',width:50},
          {title:'试卷',dataIndex:'pagesName',width:200},
          {title:'班级',dataIndex:'className',width:100},
          {title:'开始时间',dataIndex:'startTime',width:150 ,renderer:function(value){
        	  	  
        	  	  var datetime = new Date();
              datetime.setTime(value);
              var year = datetime.getFullYear();
              var month = datetime.getMonth() + 1;
              var date = datetime.getDate();
              var hour = datetime.getHours();
              var minute = datetime.getMinutes();
              var second = datetime.getSeconds();
              return year + "-" + month + "-" + date+" "+hour+":"+minute;
        	  
          }},
          {title:'结束时间',dataIndex:'endTime',width:150,renderer:function(value){
    	  	  
	    	  	  var datetime = new Date();
	          datetime.setTime(value);
	          var year = datetime.getFullYear();
	          var month = datetime.getMonth() + 1;
	          var date = datetime.getDate();
	          var hour = datetime.getHours();
	          var minute = datetime.getMinutes();
	          var second = datetime.getSeconds();
	          return year + "-" + month + "-" + date+" "+hour+":"+minute;
	    	  
	      }},
          {title:'添加时间',dataIndex:'addTime',width:150,renderer:function(value){
    	  	  
	    	  	  var datetime = new Date();
	          datetime.setTime(value);
	          var year = datetime.getFullYear();
	          var month = datetime.getMonth() + 1;
	          var date = datetime.getDate();
	          var hour = datetime.getHours();
	          var minute = datetime.getMinutes();
	          var second = datetime.getSeconds();
	          return year + "-" + month + "-" + date+" "+hour+":"+minute;
	    	  
	      }},
          //{title:'状态',dataIndex:'status',width:100},
         /*  {title:'操作',dataIndex:'',width:150,renderer : function(value,obj){
              editStr1 = '<span class="grid-command btn-edit" title="编辑">编辑</span>';
            return editStr1;
          }} */
        ],
      store = Search.createStore('${pageContext.request.contextPath }/exam/getAllExam',{
        proxy : {
          save : { //也可以是一个字符串，那么增删改，都会往那么路径提交数据，同时附加参数saveType
            addUrl : "${pageContext.request.contextPath }/exam/addExam",
            //updateUrl : "${pageContext.request.contextPath }/exam/updateExam"
          },
          method : 'POST',
        },
        autoSync : true ,//保存数据后，自动更新
        pageSize:10
      });
      
     
    var  gridCfg = Search.createGridCfg(columns,{
         tbar : {
           items : [
             {text : '<i class="icon-plus"></i>新建',btnCls : 'button button-small',handler:addFunction},
           ]
         },
         emptyDataTpl:'<div class="centered"><img alt="Crying" src="__PUBLIC__/img/nodata.png"><h2>查询的数据不存在</h2></div>',
         plugins : [editing,BUI.Grid.Plugins.CheckSelection,BUI.Grid.Plugins.AutoFit] // 插件形式引入多选表格

    });
      
    var  search = new Search({
        store : store,
        gridCfg : gridCfg
      }),
      grid = search.get('grid');

    function addFunction(){
      var newData = {isNew : true}; //标志是新增加的记录
      editing.add(newData,'name'); //添加记录后，直接编辑
    }

    //删除操作
    function delFunction(){
      var selections = grid.getSelection();
      delItems(selections);
    }
	
    function delItems(items){
      var ids = [];
      BUI.each(items,function(item){
        ids.push(item.id);
      });

      if(ids.length){
        BUI.Message.Confirm('确认要删除选中的记录么？',function(){
          store.save('remove',{ids : ids});
        },'question');
      }
    }

    //监听事件，删除一条记录
    grid.on('cellclick',function(ev){
      var sender = $(ev.domTarget); //点击的Dom
      if(sender.hasClass('btn-del')){
        var record = ev.record;
        delItems([record]);
      }
    });
  });


</script>


</body>
</html>
