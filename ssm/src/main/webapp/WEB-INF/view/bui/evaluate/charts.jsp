<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE HTML>
<html>
 <head>
  <title> 资源文件结构</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />  
 </head>
 <body>
 
  <div class="container">
  	
     <div id="canvas">
     
     </div>
  	<hr>
       <a href="javascript:history.go(-1)">返回上一步</a>
  </div>
  <script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script>
  <script type="text/javascript" src="../assets/js/bui-min.js"></script>
  <!-- 如果不使用页面内部跳转，则下面 script 标签不需要,同时不需要引入 common/page -->
  <script type="text/javascript" src="../assets/js/config-min.js"></script>
  
 
  <script type="text/javascript">
    BUI.use('common/page'); //页面链接跳转
    
    BUI.use('bui/chart',function (Chart) {
    	  
    		var xValues = new Array();
    		var yValues = new Array();
    		
    		var studentScores = '${studentScores}';
    		studentScores = eval(studentScores);
    		
    		var pagesName = '${pagesName}';
    		var className = '${className}';
    		
    		for(var i=0 ; i<studentScores.length; i++)
    		{
    			xValues.push(studentScores[i].studentName);
    			yValues.push(studentScores[i].score);
    		}
    		
        var chart = new Chart.Chart({
              render : '#canvas',
              width : 1250,
              height : 400,
              title : {
                text : pagesName,
                'font-size' : '16px'
              },
            	  subTitle : {
                text : className+'考试情况' //'Source: WorldClimate.com'
              }, 
              xAxis : {
                categories: xValues
              },
              yAxis : {
                title : {
                  text : '成绩'
                },
                min : 0
              },  
              tooltip : {
                shared : true
              },
              seriesOptions : {
                  columnCfg : {
                      
                  }
              },
              series: [ {
                      name: '名单',
                      data: yValues
     
                  }]
                  
            });
     
            chart.render();
        });
     
    
  </script>
 
<body>
</html> 