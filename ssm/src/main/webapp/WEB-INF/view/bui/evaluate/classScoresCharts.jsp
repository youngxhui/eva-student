<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE HTML>
<html>
 <head>
  <title> 资源文件结构</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />  
 </head>
 <body>
 
 
  <div class="container">
     <div id="canvas">
     
     </div>
  <hr>
      <a href="javascript:history.go(-1)">返回上一步</a>
  </div>
  <script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script>
  <script type="text/javascript" src="../assets/js/bui-min.js"></script>
  <!-- 如果不使用页面内部跳转，则下面 script 标签不需要,同时不需要引入 common/page -->
  <script type="text/javascript" src="../assets/js/config-min.js"></script>
  <script src="http://g.tbcdn.cn/bui/acharts/1.0.18/acharts.js"></script> 
 
 <script type="text/javascript">
 	
		
		var xValues = new Array();
		var yValues = new Array();
		
		var pagesId = '${pagesId}';
		var pagesName = '${pagesName}';
		
		var classScores = '${classScores}';
		classScores = eval(classScores);
		
		for(var i=0 ; i<classScores.length; i++)
		{
			xValues.push(classScores[i].classId);
			yValues.push(classScores[i].score);
		}
	
	
        var chart = new AChart({
          id : 'canvas',
          theme : Chart.Theme.Smooth1,
          width : 950,
          height : 400,
          title : {
            text : pagesName,
            'font-size' : '16px'
          },
          subTitle : {
        	  	text :  "各班考试情况"
          },
     /*      subTitle : {
            text : 'Source: WorldClimate.com'//source渲染后是可以点击的href
          }, */
          xAxis : {
            //type : 'category'
            	categories: xValues
          },
          yAxis : {
            title : {
              text : '平均分'
            },
            min : 0
          },
          seriesOptions : {
              columnCfg : {
                allowPointSelect : true,
                xField : 'classId',
                item : {
                  cursor : 'pointer',
                  stroke : 'none'
                }
              }
          },
          series: [ {
            name: '班级',
            events : {
              itemclick : function (ev) {
                var point = ev.point;
                //console.log(point);
                  //item = ev.item, //点击的项
                 // var obj = point.obj; //point.obj是点击的配置项的信息
            /*     console.log(point); //执行一系列操作
                console.log(ev.item);
                console.log(point.obj); */
                
                location.href="${pageContext.request.contextPath }/exam/viewStudentsCharts?class_id="+point.xValue+"&pages_id="+pagesId;
                
              }
          
          /*,
              itemselected : function(ev){ //选中事件
 
              },
              itemunselected : function(ev){ //取消选中事件
  
              }
              */
            },
            data:yValues
          }]
 
        });
 
        chart.render();
 
 
      </script>
      
<body>
</html> 