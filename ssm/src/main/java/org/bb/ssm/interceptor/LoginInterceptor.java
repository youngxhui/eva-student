package org.bb.ssm.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bb.ssm.model.User;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class LoginInterceptor implements HandlerInterceptor {
    //必须提供set方法，否则不能获取配置文件中的信息
    // 自动读取springmvc配置文件中配置的不需要拦截的方法  <property name="notIncludeURIS">
    List<String> notIncludeURIS;

    @Override
    public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3) throws Exception {

    }

    @Override
    public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3) throws Exception {

    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {

        boolean f = false;

        // 获取session中的对象（登录成功会在session作用域放一个当前登录的对象）
        User user = (User) request.getSession().getAttribute("userinfo");

        // 获取当前访问 触发的URI路径
        String requestURI = request.getRequestURI();

        // 当前的请求路径 与 不需要拦截的URI集合 进行比较 判断是否包含

        if (notIncludeURIS.contains(requestURI) || requestURI.endsWith("txt") || requestURI.endsWith("docx") || requestURI.endsWith("doc") || requestURI.endsWith("pdf") || requestURI.endsWith("rar") || requestURI.endsWith("zip")) { // 不需要拦截
            f = true;
        } else {
            if (user != null) { // 已登录,不需要拦截
                f = true;
            } else { // 需要拦截
                f = false;
                //放置提示信息
                request.getSession().setAttribute("errorInfo", "非法访问！请先登录");
                // 转发到登录页面
                request.getRequestDispatcher("/WEB-INF/view/bui/login/login1.jsp").forward(request, response);
            }
        }

        return f;
    }

    /**
     * 功能:getters and setters
     *
     * @kuwei 2017年4月28日下午2:51:13
     */
    public List<String> getNotIncludeURIS() {
        return notIncludeURIS;
    }

    public void setNotIncludeURIS(List<String> notIncludeURIS) {
        this.notIncludeURIS = notIncludeURIS;
    }

}