package org.bb.ssm.model;

import java.sql.Timestamp;

public class Titles {
	private int id;
	private String title;
	private int category;
	private int difficulty;
	private String answer;
	private String analysis;
	private int teacherId;
	private Timestamp addTime;

	private String sectiona;
	private String sectionb;
	private String sectionc;
	private String sectiond;

	private int orderd;

	private String knowledgeId;
	// 通过uek_evaluate_knowledge_title 中的knowledge_id 找到uek_acdemic_knowlege表中的name
	private String knowledgeName;
	
	private String chapterId;
	private String chapterName;
	
	
	private String courseId;
	private String courseName;
	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public int getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getAnalysis() {
		return analysis;
	}
	public void setAnalysis(String analysis) {
		this.analysis = analysis;
	}
	public int getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(int teacherId) {
		this.teacherId = teacherId;
	}
	
	public Timestamp getAddTime() {
		return addTime;
	}
	public void setAddTime(Timestamp addTime) {
		this.addTime = addTime;
	}
	public String getSectiona() {
		return sectiona;
	}
	public void setSectiona(String sectiona) {
		this.sectiona = sectiona;
	}
	public String getSectionb() {
		return sectionb;
	}
	public void setSectionb(String sectionb) {
		this.sectionb = sectionb;
	}
	public String getSectionc() {
		return sectionc;
	}
	public void setSectionc(String sectionc) {
		this.sectionc = sectionc;
	}
	public String getSectiond() {
		return sectiond;
	}
	public void setSectiond(String sectiond) {
		this.sectiond = sectiond;
	}
	public int getOrderd() {
		return orderd;
	}
	public void setOrderd(int orderd) {
		this.orderd = orderd;
	}
	
	public String getKnowledgeName() {
		return knowledgeName;
	}
	public void setKnowledgeName(String knowledgeName) {
		this.knowledgeName = knowledgeName;
	}
	
	
	public String getChapterId() {
		return chapterId;
	}
	public void setChapterId(String chapterId) {
		this.chapterId = chapterId;
	}
	public void setKnowledgeId(String knowledgeId) {
		this.knowledgeId = knowledgeId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getChapterName() {
		return chapterName;
	}
	public void setChapterName(String chapterName) {
		this.chapterName = chapterName;
	}
	
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	
	
	public String getKnowledgeId() {
		return knowledgeId;
	}
	public String getCourseId() {
		return courseId;
	}
	@Override
	public String toString() {
		return "Titles [id=" + id + ", title=" + title + ", category=" + category + ", difficulty=" + difficulty
				+ ", answer=" + answer + ", analysis=" + analysis + ", teacherId=" + teacherId + ", addTime="
				+ addTime + ", sectiona=" + sectiona + ", sectionb=" + sectionb + ", sectionc=" + sectionc
				+ ", sectiond=" + sectiond + ", orderd=" + orderd + ", knowledgeId=" + knowledgeId + ", knowledgeName="
				+ knowledgeName + "]";
	}

	
}
