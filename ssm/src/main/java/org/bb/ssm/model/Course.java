package org.bb.ssm.model;

public class Course {
	private int id;

	private String num;

	private String name;

	private String about;

	private String coverImage;

	public Course() {
		super();
	}

	public Course(int id, String num, String name, String about) {
		super();
		this.id = id;
		this.num = num;
		this.name = name;
		this.about = about;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getCoverImage() {
		return coverImage;
	}

	public void setCoverImage(String coverImage) {
		this.coverImage = coverImage;
	}

	@Override
	public String toString() {
		return "Course [id=" + id + ", num=" + num + ", name=" + name + ", about=" + about + "]";
	}

}
