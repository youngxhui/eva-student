package org.bb.ssm.model;

import java.sql.Timestamp;
import java.util.List;

public class StudentScores {
	private int id;
	private int studentId;
	private String studentNumber;

	private String studentName;
	private int pagesId;
	private String pagesName;

	private List<Integer> score;
	private Timestamp time;

	private String className;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public int getPagesId() {
		return pagesId;
	}

	public void setPagesId(int pagesId) {
		this.pagesId = pagesId;
	}

	public String getPagesName() {
		return pagesName;
	}

	public void setPagesName(String pagesName) {
		this.pagesName = pagesName;
	}

	public List<Integer> getScore() {
		return score;
	}

	public void setScore(List<Integer> score) {
		this.score = score;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

}
