package org.bb.ssm.model;

public class Section {
	private int id;
	private String num;
	private String name;
	private int is_difficult;
	private int is_important;
	private String video;
	
	
	private int chapter_id;
	private String chapter_name;
	
	public Section() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIs_difficult() {
		return is_difficult;
	}

	public void setIs_difficult(int is_difficult) {
		this.is_difficult = is_difficult;
	}

	public int getIs_important() {
		return is_important;
	}

	public void setIs_important(int is_important) {
		this.is_important = is_important;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public int getChapter_id() {
		return chapter_id;
	}

	public void setChapter_id(int chapter_id) {
		this.chapter_id = chapter_id;
	}

	public String getChapter_name() {
		return chapter_name;
	}

	public void setChapter_name(String chapter_name) {
		this.chapter_name = chapter_name;
	}

	
	
	

}
