package org.bb.ssm.model;

public class TimeTables {

	private int id;
	private int year;
	private int term;
	private String classNum;
	private String courseNum;
	private int fromWeek;
	private int toWeek;
	private int weekCycle;
	private String courseArrangement;
	private String classRoomNum;
	private String teacherNum;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getClassNum() {
		return classNum;
	}
	public void setClassNum(String classNum) {
		this.classNum = classNum;
	}
	public String getCourseNum() {
		return courseNum;
	}
	public void setCourseNum(String courseNum) {
		this.courseNum = courseNum;
	}
	public String getClassRoomNum() {
		return classRoomNum;
	}
	public void setClassRoomNum(String classRoomNum) {
		this.classRoomNum = classRoomNum;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getTerm() {
		return term;
	}
	public void setTerm(int term) {
		this.term = term;
	}
	
	
	public int getFromWeek() {
		return fromWeek;
	}
	public void setFromWeek(int fromWeek) {
		this.fromWeek = fromWeek;
	}
	public int getToWeek() {
		return toWeek;
	}
	public void setToWeek(int toWeek) {
		this.toWeek = toWeek;
	}
	public int getWeekCycle() {
		return weekCycle;
	}
	public void setWeekCycle(int weekCycle) {
		this.weekCycle = weekCycle;
	}
	public String getCourseArrangement() {
		return courseArrangement;
	}
	public void setCourseArrangement(String courseArrangement) {
		this.courseArrangement = courseArrangement;
	}
	public String getTeacherNum() {
		return teacherNum;
	}
	public void setTeacherNum(String teacherNum) {
		this.teacherNum = teacherNum;
	}
	
	
	
	
	
}
