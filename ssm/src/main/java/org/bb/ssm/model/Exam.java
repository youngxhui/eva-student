package org.bb.ssm.model;

import java.sql.Timestamp;

public class Exam {
	private int id;
	private int pagesId;
	private String pagesName;
	private int classId;
	private String className;
	
	private Timestamp startTime;
	private Timestamp endTime;
	private Timestamp addTime;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPagesId() {
		return pagesId;
	}

	public void setPagesId(int pagesId) {
		this.pagesId = pagesId;
	}

	public int getClassId() {
		return classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public Timestamp getAddTime() {
		return addTime;
	}

	public void setAddTime(Timestamp addTime) {
		this.addTime = addTime;
	}

	
	public String getPagesName() {
		return pagesName;
	}

	public void setPagesName(String pagesName) {
		this.pagesName = pagesName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	@Override
	public String toString() {
		return "Exam [id=" + id + ", pagesId=" + pagesId + ", classId=" + classId + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", addTime=" + addTime + "]";
	}

	
}
