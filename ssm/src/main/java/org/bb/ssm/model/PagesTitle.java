package org.bb.ssm.model;

public class PagesTitle {
	private Integer id;
	private Integer pages_id;
	private Integer title_id;
	private Integer title_id_new;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPages_id() {
		return pages_id;
	}

	public void setPages_id(Integer pages_id) {
		this.pages_id = pages_id;
	}

	public Integer getTitle_id() {
		return title_id;
	}

	public void setTitle_id(Integer title_id) {
		this.title_id = title_id;
	}

	public Integer getTitle_id_new() {
		return title_id_new;
	}

	public void setTitle_id_new(Integer title_id_new) {
		this.title_id_new = title_id_new;
	}
	
	

}
