package org.bb.ssm.model;

import java.util.List;

public class BuiLeaf {
	//1 本来定义的是int 但是只有一级菜单可以出来，二三级都出不来 参考bui改成String 就可以了
	//2 参考中是 1 11 12 121 122这样的id 实际不用 因为 children
	private String id;		
	private String text;
	private List<BuiLeaf> children;
	private boolean leaf;
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<BuiLeaf> getChildren() {
		return children;
	}

	public void setChildren(List<BuiLeaf> children) {
		this.children = children;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BuiLeaf other = (BuiLeaf) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
	
}
