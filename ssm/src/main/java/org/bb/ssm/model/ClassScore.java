package org.bb.ssm.model;

public class ClassScore {
	private int pagesId;
	private String pagesName;
	
	private int score;
	private int classId;
	private int className;
	public int getPagesId() {
		return pagesId;
	}
	public void setPagesId(int pagesId) {
		this.pagesId = pagesId;
	}
	
	
	public String getPagesName() {
		return pagesName;
	}
	public void setPagesName(String pagesName) {
		this.pagesName = pagesName;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getClassId() {
		return classId;
	}
	public void setClassId(int classId) {
		this.classId = classId;
	}
	public int getClassName() {
		return className;
	}
	public void setClassName(int className) {
		this.className = className;
	}


	
}
