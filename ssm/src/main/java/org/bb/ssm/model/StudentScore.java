package org.bb.ssm.model;

import java.sql.Timestamp;

public class StudentScore {
	private int id;
	private int studentId;
	private String studentNumber;
	
	private String studentName;
	private int pagesId;
	private String pagesName;
	
	private int score;
	private Timestamp time;
	
	private String className;

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	
	public String getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public int getPagesId() {
		return pagesId;
	}

	public void setPagesId(int pagesId) {
		this.pagesId = pagesId;
	}

	public String getPagesName() {
		return pagesName;
	}

	public void setPagesName(String pagesName) {
		this.pagesName = pagesName;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
	
	
	
}
