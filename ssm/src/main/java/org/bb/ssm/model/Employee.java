package org.bb.ssm.model;

public class Employee {
	private Integer id;
	private String job_number;
	private String name;
	private String hiredate;
	private Integer status;
	private Integer position_id;
	private Integer sex;
	private String tel;
	private String photo;
	
	private Integer userId;
	
	private String posname;//找的职位表的名称
	
	private int departmentId;
	private String departmentName;//关联职位表之后继续关联部门表找部门名称
	
	
	
	public Employee() {
		super();
	}

	public Employee(Integer id, String job_number, String name,
			String hiredate, Integer status, Integer position_id,
			Integer user_id, Integer sex, String tel, String photo,
			String posname) {
		super();
		this.id = id;
		this.job_number = job_number;
		this.name = name;
		this.hiredate = hiredate;
		this.status = status;
		this.position_id = position_id;
		this.userId = user_id;
		this.sex = sex;
		this.tel = tel;
		this.photo = photo;
		this.posname = posname;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getJob_number() {
		return job_number;
	}

	public void setJob_number(String job_number) {
		this.job_number = job_number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHiredate() {
		return hiredate;
	}

	public void setHiredate(String hiredate) {
		this.hiredate = hiredate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getPosition_id() {
		return position_id;
	}

	public void setPosition_id(Integer position_id) {
		this.position_id = position_id;
	}

	

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getPosname() {
		return posname;
	}

	public void setPosname(String posname) {
		this.posname = posname;
	}

	
	
	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", job_number=" + job_number + ", name="
				+ name + ", hiredate=" + hiredate + ", status=" + status
				+ ", position_id=" + position_id + ", user_id=" + userId
				+ ", sex=" + sex + ", tel=" + tel + ", photo=" + photo
				+ ", posname=" + posname + "]";
	}

}