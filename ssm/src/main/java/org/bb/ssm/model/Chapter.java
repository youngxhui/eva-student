package org.bb.ssm.model;

public class Chapter {
	
	private int id;
	private String num;
	private String name;
	private int is_difficult;
	private int is_important;
	
	private int course_id;
	private String course_name;
	
	public Chapter() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIs_difficult() {
		return is_difficult;
	}

	public void setIs_difficult(int is_difficult) {
		this.is_difficult = is_difficult;
	}

	public int getIs_important() {
		return is_important;
	}

	public void setIs_important(int is_important) {
		this.is_important = is_important;
	}

	public int getCourse_id() {
		return course_id;
	}

	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}

	public String getCourse_name() {
		return course_name;
	}

	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}

	
}
