package org.bb.ssm.service;

import java.util.List;

import org.bb.ssm.model.News;

public interface NewsInfoService {

	List<News> findAll();
	List<News> findNewsByKnowledgeId(int knowledgeId);
	
	int deleteByPrimaryKey(Integer id);

	int insert(News record);

	int insertSelective(News record);

	News selectByPrimaryKey(Integer id);
	
	News selectByPwd(News record);

	int updateByPrimaryKeySelective(News record);

	int updateByPrimaryKey(News record);
	
	
}
