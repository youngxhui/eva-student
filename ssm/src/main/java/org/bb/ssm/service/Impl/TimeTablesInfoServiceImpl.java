package org.bb.ssm.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.bb.ssm.mapper.TimeTablesInfoMapper;
import org.bb.ssm.model.TimeTables;
import org.bb.ssm.model.Menu;
import org.bb.ssm.service.TimeTablesInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//可以消除xml中对bean的配置
@Service
// 此处使用spring的声明式事务，不在使用sqlsession和提交事务了
@Transactional
public class TimeTablesInfoServiceImpl implements TimeTablesInfoService {

	@Resource
	
	private TimeTablesInfoMapper mapper;

	/**
	 * 查询TimeTablesInfo表所有数据
	 */
	@Override
	public List<TimeTables> findAll() {
		List<TimeTables> list = mapper.findAll();
		return list;
	}
	
	/*@Override
	public int deleteByPrimaryKey(Integer id) {
		return mapper.deleteByPrimaryKey(id);
	}*/

	@Override
	public int insert(TimeTables record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(TimeTables record) {
		return mapper.insertSelective(record);
	}

	@Override
	public TimeTables selectByPrimaryKey(Integer id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(TimeTables record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(TimeTables record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public TimeTables selectByPwd(TimeTables record) {
		return mapper.selectByPwd(record);
	}

	@Override
	public int totalCount(String classNum, String courseNum) {
		return (int) mapper.getTimeTablesCount(classNum,courseNum);
	}

	@Override
	public List<TimeTables> findAllByPage(Integer limit, Integer pageIndex,
			String classNum, String courseNum) {
		List<TimeTables> list = mapper.findAllByPage(limit,pageIndex,classNum,courseNum);
		return list;
	}

	@Override
	public int deleteByPrimaryKey(String[] ids) {
		return mapper.deleteByPrimaryKey(ids);
	}

}
