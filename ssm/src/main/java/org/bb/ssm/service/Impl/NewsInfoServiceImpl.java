package org.bb.ssm.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.bb.ssm.mapper.NewsInfoMapper;
import org.bb.ssm.model.News;
import org.bb.ssm.service.NewsInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//可以消除xml中对bean的配置
@Service
// 此处使用spring的声明式事务，不在使用sqlsession和提交事务了
@Transactional
public class NewsInfoServiceImpl implements NewsInfoService {

	@Resource
	private NewsInfoMapper mapper;

	/**
	 * 查询NewsInfo表所有数据
	 */
	@Override
	public List<News> findAll() {
		List<News> list = mapper.findAll();
		return list;
	}
	
	/**
	 * 根据知识点id（也就是对应视频）查找视频评论
	 */
	@Override
	public List<News> findNewsByKnowledgeId(int knowledgeId) {
		List<News> list = mapper.findNewsByKnowledgeId(knowledgeId);
		return list;
	}
	
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(News record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(News record) {
		return mapper.insertSelective(record);
	}

	@Override
	public News selectByPrimaryKey(Integer id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(News record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(News record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public News selectByPwd(News record) {
	
		return mapper.selectByPwd(record);
	}

}
