package org.bb.ssm.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.bb.ssm.mapper.TitlesInfoMapper;
import org.bb.ssm.model.Titles;
import org.bb.ssm.service.TitlesInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//可以消除xml中对bean的配置
@Service
// 此处使用spring的声明式事务，不在使用sqlsession和提交事务了
@Transactional
public class TitlesInfoServiceImpl implements TitlesInfoService {

	@Resource
	private TitlesInfoMapper mapper;

	/**
	 * 查询TitlesInfo表所有数据
	 */
	@Override
	public List<Titles> findAll() {
		List<Titles> list = mapper.findAll();
		return list;
	}
	
	@Override
	public List<Titles> findAllByPage(Integer limit, Integer pageIndex,String searchname) {
		List<Titles> list = mapper.findAllByPage(limit,pageIndex,searchname);
		return list;
	}
	
	@Override
	public int totalCount(String searchname) {
		return (int) mapper.getTitlesCount(searchname);
	}
	
	/**
	 * 查询TitlesInfo表所有数据
	 */
	@Override
	public List<Titles> findByPagesId(Integer id) {
		List<Titles> list = mapper.findByPagesId(id);
		return list;
	}
	
	public List<Titles> findByCourseId(Integer courseId , Integer chapterId)
	{
		return mapper.findByCourseId(courseId , chapterId);
	}
	
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(Titles record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(Titles record) {
		return mapper.insertSelective(record);
	}

	@Override
	public Titles selectByPrimaryKey(Integer id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(Titles record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Titles record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public Titles selectByPwd(Titles record) {
		// TODO Auto-generated method stub
		return mapper.selectByPwd(record);
	}

}
