package org.bb.ssm.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.bb.ssm.mapper.CourseInfoMapper;
import org.bb.ssm.model.BuiLeaf;
import org.bb.ssm.model.Course;
import org.bb.ssm.model.CourseChapterKnowledge;
import org.bb.ssm.service.CourseInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//可以消除xml中对bean的配置
@Service
// 此处使用spring的声明式事务，不在使用sqlsession和提交事务了
@Transactional
public class CourseInfoServiceImpl implements CourseInfoService {

	@Resource
	private CourseInfoMapper mapper;

	/**
	 * 查询CourseInfo表所有数据
	 */
	@Override
	public List<Course> findAll() {
		List<Course> list = mapper.findAll();
		return list;
	}
	
	@Override
	public List<CourseChapterKnowledge> getAllCourseChapterKnowledge()
	{
		List<CourseChapterKnowledge> list = mapper.getAllCourseChapterKnowledge();
		
		return list;
	}
	
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return mapper.deleteByPrimaryKey(id);
	}

	public int deleteByPrimaryKeys(List<String> ids) {
		return mapper.deleteByPrimaryKeys(ids);
	}
	
	@Override
	public int insert(Course record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(Course record) {
		return mapper.insertSelective(record);
	}

	@Override
	public Course selectByPrimaryKey(Integer id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(Course record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Course record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public Course selectByPwd(Course record) {
		// TODO Auto-generated method stub
		return mapper.selectByPwd(record);
	}

	@Override
	public int getIdByNum(String num) {
		return mapper.getIdByNum(num);
	}

}
