package org.bb.ssm.service;

import java.util.List;

import org.bb.ssm.model.Banner;

/**
 * 
 * @author Administrator
 *
 */
public interface BannerInfoService {

	List<Banner> findAll();
	
	int deleteByPrimaryKey(Integer id);
	
	int deleteByPrimaryKeys(List<String> ids);
	
	int insert(Banner record);

	int insertSelective(Banner record);

	Banner selectByPrimaryKey(Integer id);
	
	Banner selectByPwd(Banner record);

	int updateByPrimaryKeySelective(Banner record);

	int updateByPrimaryKey(Banner record);

	int getIdByNum(String num);
}
