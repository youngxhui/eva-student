package org.bb.ssm.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.bb.ssm.mapper.BannerInfoMapper;
import org.bb.ssm.model.Banner;
import org.bb.ssm.service.BannerInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//可以消除xml中对bean的配置
@Service
// 此处使用spring的声明式事务，不在使用sqlsession和提交事务了
@Transactional
public class BannerInfoServiceImpl implements BannerInfoService {

	@Resource
	private BannerInfoMapper mapper;

	/**
	 * 查询BannerInfo表所有数据
	 */
	@Override
	public List<Banner> findAll() {
		List<Banner> list = mapper.findAll();
		return list;
	}
	
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return mapper.deleteByPrimaryKey(id);
	}
	
	public int deleteByPrimaryKeys(List<String> ids) {
		return mapper.deleteByPrimaryKeys(ids);
	}
	
	@Override
	public int insert(Banner record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(Banner record) {
		return mapper.insertSelective(record);
	}

	@Override
	public Banner selectByPrimaryKey(Integer id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(Banner record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Banner record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public Banner selectByPwd(Banner record) {
		// TODO Auto-generated method stub
		return mapper.selectByPwd(record);
	}

	@Override
	public int getIdByNum(String num) {
		return mapper.getIdByNum(num);
	}

}
