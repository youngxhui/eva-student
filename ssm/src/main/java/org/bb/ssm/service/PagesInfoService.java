package org.bb.ssm.service;

import java.util.List;

import org.bb.ssm.model.Pages;
import org.bb.ssm.model.PagesTitle;

/**
 * 
 * @author Administrator
 *
 */
public interface PagesInfoService {

	List<Pages> findAll();
	
	int deleteByPrimaryKey(String[] ids);

	int autoGeneratorPage(PagesTitle record);
	
	int changeTitle(PagesTitle record);
	
	int insert(Pages record);

	int insertSelective(Pages record);

	Pages selectByPrimaryKey(Integer id);
	
	Pages selectByPwd(Pages record);

	int updateByPrimaryKeySelective(Pages record);

	int updateByPrimaryKey(Pages record);

	int totalCount(String searchname, int course_id);
	
	List<Pages> findAllByPage(Integer limit, Integer pageIndex,
			String searchname, Integer course_id);
}
