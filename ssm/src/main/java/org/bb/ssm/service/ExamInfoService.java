package org.bb.ssm.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.bb.ssm.model.ClassScore;
import org.bb.ssm.model.Exam;
import org.bb.ssm.model.StudentScore;

/**
 * Dao层是和数据库打交道的，Service层会封装具体的业务。有点抽象.. e.g. 用户管理系统
 * dao封装了用户的增删改查。而业务上要求批量删除用户，Service就可以封装出一个批量删除用户的功能
 * ，但是实现只是循环调用dao的单个删除  
 * 此处没有特殊的业务需求，所以和dao层写的一模一样
 * 
 * @author Administrator
 *
 */
public interface ExamInfoService {

	List<Exam> findAll();
	
	/*
	 * 查看班级考试安排 
	 */
	List<Exam> findAllByClassId(Integer classId);
	
	/*
	 * 查看某门课程考试安排
	 */
	List<Exam> findAllByPageId(Integer pageId);
	
	
	List<StudentScore> lookScore();
	List<StudentScore> lookScoreByPage(Integer limit, Integer pageIndex,String searchname, Integer pages_id , Integer class_id);
	int totalCountOfStudentScore(String searchname, Integer pages_id , Integer class_id);
	
	List<ClassScore> getClassAvgScoreByPagesId(@Param(value = "pages_id") Integer pages_id);
	
	int deleteByPrimaryKey(String[] ids);

	int insert(Exam record);

	int insertSelective(Exam record);

	Exam selectByPrimaryKey(Integer id);
	
	Exam selectByPwd(Exam record);

	int updateByPrimaryKeySelective(Exam record);

	int updateByPrimaryKey(Exam record);
	
	
	int totalCount(String searchname, int course_id);
	
	List<Exam> findAllByPage(Integer limit, Integer pageIndex,
			String searchname, Integer course_id);
}
