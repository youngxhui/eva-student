package org.bb.ssm.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.bb.ssm.mapper.PagesInfoMapper;
import org.bb.ssm.model.Pages;
import org.bb.ssm.model.PagesTitle;
import org.bb.ssm.service.PagesInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//可以消除xml中对bean的配置
@Service
// 此处使用spring的声明式事务，不在使用sqlsession和提交事务了
@Transactional
public class PagesInfoServiceImpl implements PagesInfoService {

	@Resource
	
	private PagesInfoMapper mapper;

	/**
	 * 查询PagesInfo表所有数据
	 */
	@Override
	public List<Pages> findAll() {
		List<Pages> list = mapper.findAll();
		return list;
	}
	
	public int autoGeneratorPage(PagesTitle record)
	{
		return mapper.autoGeneratorPage(record);
	}
	
	public int changeTitle(PagesTitle record)
	{
		return mapper.changeTitle(record);
	}
	
	/*@Override
	public int deleteByPrimaryKey(Integer id) {
		return mapper.deleteByPrimaryKey(id);
	}*/

	@Override
	public int insert(Pages record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(Pages record) {
		return mapper.insertSelective(record);
	}

	@Override
	public Pages selectByPrimaryKey(Integer id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(Pages record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Pages record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public Pages selectByPwd(Pages record) {
		return mapper.selectByPwd(record);
	}

	@Override
	public int totalCount(String searchname, int course_id) {
		return (int) mapper.getPagesCount(searchname,course_id);
	}

	@Override
	public List<Pages> findAllByPage(Integer limit, Integer pageIndex,
			String searchname, Integer course_id) {
		List<Pages> list = mapper.findAllByPage(limit,pageIndex,searchname,course_id);
		return list;
	}

	@Override
	public int deleteByPrimaryKey(String[] ids) {
		return mapper.deleteByPrimaryKey(ids);
	}

}
