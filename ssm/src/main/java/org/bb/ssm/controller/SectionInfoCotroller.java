package org.bb.ssm.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.bb.ssm.model.Section;
import org.bb.ssm.service.SectionInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value = "/section")
public class SectionInfoCotroller {

	@Autowired
	private SectionInfoService sectionInfoService;

	/**
	 * 节列表页
	 *
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/index")
	public String index(Map<String, Object> map) {
		return "bui/acdemic/sectionList";
	}

	/**
	 * 查看视频
	 *
	 * @param map
	 * @return
	 */
	/*
	 * @RequestMapping(value = "/index") public String showVideo(int sectionId) {
	 *
	 *
	 * return "bui/acdemic/showVideo"; }
	 */

	/**
	 * 得到所有节信息
	 *
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/getAllSection", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public String getAllSection(@RequestParam(value = "limit", required = false) Integer limit,
								@RequestParam(value = "pageIndex", required = false) Integer pageIndex,
								@RequestParam(value = "searchname", required = false) String searchname,
								@RequestParam(value = "chapter_id", required = false) Integer chapter_id) {
		pageIndex = pageIndex * limit;
		List<Section> sectionList = sectionInfoService.findAllByPage(limit, pageIndex, searchname, chapter_id);

		HashMap<String, Object> tsection = new HashMap<String, Object>();

		tsection.put("rows", sectionList);
		tsection.put("results", sectionInfoService.totalCount(searchname, chapter_id));

		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(tsection);

			// System.out.println(jsondata);

			return jsondata;

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
	public String upload(HttpServletRequest request) throws IOException {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		String targetFile = null;
		String fileName = null;
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			MultipartFile myfile = entity.getValue();

			String uploadPath = request.getSession().getServletContext().getRealPath("/resourse/upload");
			fileName = myfile.getOriginalFilename();

			targetFile = uploadPath + "/" + fileName;

			byte[] bs = myfile.getBytes();
			File file = new File(targetFile);
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(bs);
			fos.close();
		}

		// return "{\"url\" : \"" + fileName + "\"}";
		return "{\"url\" : \"/resourse/upload/upload1.mp4\"}";

	}

	/**
	 * 添加节操作
	 *
	 * @param sectioninfo
	 * @return
	 */
	@RequestMapping(value = "/addSection", method = { RequestMethod.GET, RequestMethod.POST })
	public String addSection(Section sectioninfo) {
		System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxx" + sectioninfo);
		try {
			sectioninfo.setName(new String(sectioninfo.getName().getBytes("iso-8859-1"), "utf-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int result = sectionInfoService.insert(sectioninfo);

		System.out.println("添加节的操作结果为：" + result);

		return "redirect:/section/getAllSection?limit=10&pageIndex=0&searchname=null&course_id=0";
	}

	/**
	 * 删除节操作
	 *
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete", method = { RequestMethod.POST, RequestMethod.GET })
	public String delete(@RequestParam(value = "ids[]") String[] ids) {
		sectionInfoService.deleteByPrimaryKey(ids);
		return "redirect:/section/getAllSection?limit=10&pageIndex=0&searchname=null&course_id=0";
	}
	/*
	 * @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE) public
	 * String delete(@PathVariable(value = "id") int id) { int result =
	 * sectionInfoService.deleteByPrimaryKey(id); System.out.println("删除节的操作结果为：" +
	 * result + "传递进来的id为：" + id); return
	 * "redirect:/section/getAllSection?limit=10&pageIndex=0&searchname=null&course_id=0";
	 * }
	 */

	/**
	 * 更新前先根据id找到节信息，回显到页面上
	 *
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public String input(@PathVariable(value = "id") Integer id, Map<String, Object> map) {
		map.put("command", sectionInfoService.selectByPrimaryKey(id));
		return "addsection";
	}

	@ModelAttribute
	public void getsectionInfo(@RequestParam(value = "sectionId", required = false) Integer id,
							   Map<String, Object> map) {
		System.out.println("每个controller 方法都会先调用我哦");
		if (id != null) {
			System.out.println("update 操作");
			map.put("sectionInfo", sectionInfoService.selectByPrimaryKey(id));
		}
		System.out.println("insert  操作");
	}

	/*
	 * 更新节，经常用的是上传节对应的视频之后点击确定，更新节记录当中的video字段
	 */
	@RequestMapping(value = "/addsection", method = RequestMethod.PUT)
	public String update(Section sectioninfo) {
		sectionInfoService.updateByPrimaryKey(sectioninfo);
		return "redirect:/section/getAllSection?limit=10&pageIndex=0&searchname=null&course_id=0";
	}
}
