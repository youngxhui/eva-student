package org.bb.ssm.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.bb.ssm.model.BuiLeaf;
import org.bb.ssm.model.Course;
import org.bb.ssm.model.CourseChapterKnowledge;
import org.bb.ssm.service.CourseInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value="/course")
public class CourseInfoCotroller {
	
	@Autowired
	private CourseInfoService courseInfoService;
	
	/**
	 * 课程列表页
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/index")
	public String index(Map<String, Object> map){
		//List<course> courseList = courseInfoService.findAll();
		//map.put("ALLcourse", courseList);
		return "bui/acdemic/courseList";
	}
	
	/**
	 * 得到所有课程信息
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/getAllCourse",method= {RequestMethod.GET , RequestMethod.POST})
	@ResponseBody
	public String getAllCourse(Object pageinfo,Map<String, Object> map){
		System.out.println(pageinfo);
		List<Course> courseList = courseInfoService.findAll();
		
		HashMap<String,Object > tcourse = new HashMap<String,Object >();
		
		tcourse.put("rows", courseList);
		tcourse.put("results", courseList.size());
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(tcourse);

			//System.out.println(jsondata);
			
			return jsondata;
			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 得到所有课程信息
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/getAllCourseChapterKnowledge",method= {RequestMethod.GET , RequestMethod.POST})
	@ResponseBody
	public String getAllCourseChapterKnowledge(){
		
		List<BuiLeaf> root = new ArrayList<BuiLeaf>();
		
		List<CourseChapterKnowledge> leafs = courseInfoService.getAllCourseChapterKnowledge();
		
		for (CourseChapterKnowledge course : leafs) {
			
			BuiLeaf courseLeaf = new BuiLeaf();
			courseLeaf.setId("cou"+course.getCourseId());
			
			if(root.contains(courseLeaf) )
			{
				continue;
			}
			
			courseLeaf.setText(course.getCourseName());
			
			
			List<BuiLeaf> chapters = new ArrayList<BuiLeaf>();
			
			for (CourseChapterKnowledge chapter : leafs) {
				
				if(("cou"+chapter.getCourseId()).equals(courseLeaf.getId()))
				{
					BuiLeaf chapterLeaf = new BuiLeaf();
					chapterLeaf.setId("cha"+chapter.getChapterId());
					if(chapters.contains(chapterLeaf))
					{
						continue;
					}
					
					chapterLeaf.setText(chapter.getChapterName());
					List<BuiLeaf> knowledges = new ArrayList<BuiLeaf>();
					
					for (CourseChapterKnowledge knowledge : leafs) {
						
						if(("cha"+knowledge.getChapterId()).equals(chapterLeaf.getId()))
						{
							BuiLeaf knowledgeLeaf = new BuiLeaf();
							knowledgeLeaf.setId("kno"+knowledge.getKnowledgeId());
							
							if(knowledges.contains(knowledgeLeaf))
							{
								continue;
							}
							
							knowledgeLeaf.setText(knowledge.getKnowledgeName());
							knowledgeLeaf.setLeaf(true);
								
							knowledges.add(knowledgeLeaf);
							
						}
							
					chapterLeaf.setChildren(knowledges);
					chapterLeaf.setLeaf(false);
						
					}
					
					chapters.add(chapterLeaf);
					
					
				}
				
				
				courseLeaf.setChildren(chapters);
				courseLeaf.setLeaf(false);
			}
			
			
			root.add(courseLeaf);
			
		}
		
	
		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(root);

			//System.out.println(jsondata);
			
			return jsondata;
			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return null;
		
		//return "[{\"id\" : \"x1\",\"text\":\"面向\",\"children\":[{\"id\":\"1\",\"text\":\"概述\",\"leaf\":false,\"children\":[{\"id\":\"143\",\"text\":\"1.1 编程语言的几个发展阶段\",\"leaf\":true}]}]}]";
		
		//return "[{\"id\" : \"1\",\"text\":\"面向\",\"children\":[{\"id\":\"12\",\"text\":\"概述\",\"leaf\":false,\"children\":[{\"id\":\"1433\",\"text\":\"中华\",\"leaf\":true}]}]}]";
		//return "[{\"id\" : \"1\",\"text\":\"山东\",\"children\":[{\"id\":\"12\",\"text\":\"淄博\",\"leaf\":false,\"children\":[{\"id\":\"121\",\"text\":\"高青\",\"leaf\":true}]}]}]";
		
	}
	
	/**
	 * 添加课程操作
	 * @param courseinfo
	 * @return
	 */
	@RequestMapping(value="/addCourse",method=RequestMethod.POST)
	public String save(Course courseinfo){
		int result = courseInfoService.insert(courseinfo);
		System.out.println("添加课程的操作结果为："+result);
		return "redirect:/course/getAllCourse";
	}
	/**
	 * 删除课程操作
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	public String delete(String ids){
		
		
		List<String> idss = Arrays.asList(ids.split(","));
		
		
		int result = courseInfoService.deleteByPrimaryKeys(idss);
		System.out.println("删除课程的操作结果为："+result+"传递进来的id为："+ids);
		return "redirect:/course/getAllCourse";
	}
	/**
	 * 更新前先根据id找到课程信息，回显到页面上
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/detail/{id}",method=RequestMethod.GET)
	public String input(@PathVariable(value="id") Integer id,Map<String, Object> map){
		map.put("command", courseInfoService.selectByPrimaryKey(id));
		return "addcourse";
	}
	
	@ModelAttribute
	public void getcourseInfo(@RequestParam(value="courseId",required=false) Integer id
			,Map<String, Object> map){
		System.out.println("每个controller 方法都会先调用我哦");
		if(id != null){
			System.out.println("update 操作");
			map.put("courseInfo", courseInfoService.selectByPrimaryKey(id));
		}
		System.out.println("insert  操作");
	}
	
	@RequestMapping(value="/updateCourse",method=RequestMethod.POST)
	public String update(Course courseinfo){
		courseInfoService.updateByPrimaryKeySelective(courseinfo);
		return "redirect:/course/getAllCourse";
	}
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
	public String upload(HttpServletRequest request) throws IOException {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		String targetFile = null;
		String fileName = null;
		String newFileName = null;
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			MultipartFile myfile = entity.getValue();
			String uploadPath = request.getSession().getServletContext().getRealPath("/resourse/uploadCourseCoverImage");
			fileName = myfile.getOriginalFilename();
			
			newFileName = new Date().getTime() + fileName.substring(fileName.lastIndexOf('.'));
			
			targetFile = uploadPath + "/" + newFileName;
			
			
			byte[] bs = myfile.getBytes();
			File file = new File(targetFile);
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(bs);
			fos.close();
		}
		
		return "{\"url\" : \"" + newFileName + "\"}";
		//return "{\"url\" : \"/resourse/upload/upload1.mp4\"}";

	}
}
