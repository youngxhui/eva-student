package org.bb.ssm.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bb.ssm.model.Course;
import org.bb.ssm.model.News;
import org.bb.ssm.service.NewsInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value="/news")
public class NewsInfoCotroller {
	
	@Autowired
	private NewsInfoService newsInfoService;

	
	/**
	 * 新闻列表页
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/index")
	public String index(Map<String, Object> map){
		
		return "bui/acdemic/newsList";
	}
	
	
	/**
	 * 得到所有新闻信息
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/getAllNews",method= {RequestMethod.GET , RequestMethod.POST})
	@ResponseBody
	public String getAllCourse(Object pageinfo,Map<String, Object> map){
		System.out.println(pageinfo);
		List<News> newsList = newsInfoService.findAll();
		
		HashMap<String,Object > tNews = new HashMap<String,Object >();
		
		tNews.put("rows", newsList);
		tNews.put("results", newsList.size());
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(tNews);

			//System.out.println(jsondata);
			
			return jsondata;
			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 通过handler前往添加新闻页面
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/addNews",method= RequestMethod.GET)
	public String addNews(Map<String, Object> map){
		//因为页面使用spring的form标签，其中属性modelAttribute需要存在bean 要不会报错
		map.put("command", new News());
		return "bui/acdemic/addNews";
	}
	
	
	
	/**
	 * 添加新闻操作
	 * @param newsinfo
	 * @return
	 */
	@RequestMapping(value="/addNews",method=RequestMethod.POST)
	public String save(News newsinfo){
		newsinfo.setAddTime(new Timestamp(new Date().getTime()));
		
		int result = newsInfoService.insert(newsinfo);
		System.out.println("添加新闻的操作结果为："+result);
		
		return "redirect:/news/index";
	}
	/**
	 * 删除新闻操作
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public String delete(@PathVariable(value="id") int id){
		int result = newsInfoService.deleteByPrimaryKey(id);
		System.out.println("删除新闻的操作结果为："+result+"传递进来的id为："+id);
		return "redirect:/news/getAllNews";
	}
	/**
	 * 更新前先根据id找到新闻信息，回显到页面上
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/detail/{id}",method=RequestMethod.GET)
	public String input(@PathVariable(value="id") Integer id,Map<String, Object> map){
		map.put("command", newsInfoService.selectByPrimaryKey(id));
		return "addNews";
	}
	
	@ModelAttribute
	public void getNewsInfo(@RequestParam(value="newsId",required=false) Integer id
			,Map<String, Object> map){
		System.out.println("每个controller 方法都会先调用我哦");
		if(id != null){
			System.out.println("update 操作");
			map.put("newsInfo", newsInfoService.selectByPrimaryKey(id));
		}
		System.out.println("insert  操作");
	}
	
	@RequestMapping(value="/addNews",method=RequestMethod.PUT)
	public String update(News newsinfo){
		newsInfoService.updateByPrimaryKey(newsinfo);
		return "redirect:/news/getAllNews";
	}
}
