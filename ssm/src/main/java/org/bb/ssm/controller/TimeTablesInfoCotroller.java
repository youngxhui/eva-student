package org.bb.ssm.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.bb.ssm.model.TimeTables;
import org.bb.ssm.service.TimeTablesInfoService;
import org.bb.ssm.tool.ImportExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value = "/timeTables")
public class TimeTablesInfoCotroller {

    @Autowired
    private TimeTablesInfoService timeTablesInfoService;

    /**
     * 课程表列表页
     *
     * @param map
     * @return
     */
    @RequestMapping(value = "/index")
    public String index(Map<String, Object> map) {
        // List<timeTables> timeTablesList = timeTablesInfoService.findAll();
        // map.put("ALLtimeTables", timeTablesList);
        return "bui/acdemic/timeTablesList";
    }

    /**
     * 查看视频
     *
     * @param map
     * @return
     */
	/*@RequestMapping(value = "/index")
	public String showVideo(int timeTablesId) {


		return "bui/acdemic/showVideo";
	}*/


    /**
     * 得到所有课程表信息
     *
     * @param map
     * @return
     */
    @RequestMapping(value = "/getAllTimeTables", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public String getAllTimeTables(@RequestParam(value = "limit", required = false) Integer limit, @RequestParam(value = "pageIndex", required = false) Integer pageIndex, @RequestParam(value = "classNum", required = false) String classNum, @RequestParam(value = "courseNum", required = false) String courseNum) {
        pageIndex = pageIndex * limit;
        List<TimeTables> timeTablesList = timeTablesInfoService.findAllByPage(limit, pageIndex, classNum, courseNum);

        HashMap<String, Object> ttimeTables = new HashMap<String, Object>();

        ttimeTables.put("rows", timeTablesList);
        ttimeTables.put("results", timeTablesInfoService.totalCount(classNum, courseNum));

        ObjectMapper mapper = new ObjectMapper();
        try {
            String jsondata = mapper.writeValueAsString(ttimeTables);

            // System.out.println(jsondata);

            return jsondata;

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }


    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public String upload(HttpServletRequest request) throws IOException {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        String targetFile = null;
        String fileName = null;
        String newFileName = null;
        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
            MultipartFile myfile = entity.getValue();

            String uploadPath = request.getSession().getServletContext().getRealPath("/resourse/upload");
            //String uploadPath = request.getSession().getServletContext().getRealPath("http://localhost:8080/ssmStage/resourse/upload");
            fileName = myfile.getOriginalFilename();

            newFileName = new Date().getTime() + fileName.substring(fileName.lastIndexOf('.'));

            targetFile = uploadPath + "/" + newFileName;


            //byte[] bs = myfile.getBytes();
            File file = new File(targetFile);
            FileOutputStream fos = new FileOutputStream(file);
            //fos.write(bs);

            IOUtils.copy(myfile.getInputStream(), fos);
            fos.close();
        }

        return "{\"url\" : \"" + newFileName + "\"}";
        //return "{\"url\" : \"/resourse/upload/upload1.mp4\"}";

    }

    /**
     * 查看某个课程表对应的视频
     *
     * @param timeTablesinfo
     * @return
     */
    @RequestMapping(value = "/showVideo/{video}", method = {RequestMethod.GET})
    public String showVideo(@PathVariable("video") String video) {


        return "bui/acdemic/showVideo";
    }

    /**
     * 添加课程表操作
     *
     * @param timeTablesinfo
     * @return
     */
    @RequestMapping(value = "/addTimeTables", method = {RequestMethod.GET, RequestMethod.POST})
    public String addTimeTables(TimeTables timeTablesinfo) {
        System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxx" + timeTablesinfo);

        int result = timeTablesInfoService.insert(timeTablesinfo);

        System.out.println("添加课程表的操作结果为：" + result);

        return "redirect:/timeTables/getAllTimeTables?limit=10&pageIndex=0&searchname=null&course_id=0";
    }

    /**
     * 删除课程表操作
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete", method = {RequestMethod.POST, RequestMethod.GET})
    public String delete(@RequestParam(value = "ids[]") String[] ids) {
        timeTablesInfoService.deleteByPrimaryKey(ids);
        return "redirect:/timeTables/getAllTimeTables?limit=10&pageIndex=0&searchname=null&course_id=0";
    }
	/*@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "id") int id) {
		int result = timeTablesInfoService.deleteByPrimaryKey(id);
		System.out.println("删除课程表的操作结果为：" + result + "传递进来的id为：" + id);
		return "redirect:/timeTables/getAllTimeTables?limit=10&pageIndex=0&searchname=null&course_id=0";
	}*/

    /**
     * 更新前先根据id找到课程表信息，回显到页面上
     *
     * @param id
     * @param map
     * @return
     */
    @RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
    public String input(@PathVariable(value = "id") Integer id,
                        Map<String, Object> map) {
        map.put("command", timeTablesInfoService.selectByPrimaryKey(id));
        return "addtimeTables";
    }

    @ModelAttribute
    public void gettimeTablesInfo(
            @RequestParam(value = "timeTablesId", required = false) Integer id,
            Map<String, Object> map) {
        System.out.println("每个controller 方法都会先调用我哦");
        if (id != null) {
            System.out.println("update 操作");
            map.put("timeTablesInfo",
                    timeTablesInfoService.selectByPrimaryKey(id));
        }
        System.out.println("insert  操作");
    }

    @RequestMapping(value = "/updateTimeTables", method = RequestMethod.POST)
    public String update(TimeTables timeTablesinfo) {

        timeTablesInfoService.updateByPrimaryKey(timeTablesinfo);
        return "redirect:/timeTables/getAllTimeTables?limit=10&pageIndex=0&searchname=null&course_id=0";
    }

    //上传文件会自动绑定到MultipartFile中
    @RequestMapping(value = "/importTimeTables", method = RequestMethod.POST)
    public String upload(HttpServletRequest request,
                         @RequestParam("excelFile") MultipartFile file) throws Exception {

        //如果文件不为空，写入上传路径
        if (!file.isEmpty()) {
            //上传文件路径
            String uploadPath = request.getSession().getServletContext().getRealPath("/resourse/uploadTimeTablessInfo");
            //上传文件名
            String filename = file.getOriginalFilename();
            File filepath = new File(uploadPath, filename);

            //判断路径是否存在，如果不存在就创建一个
            if (!filepath.getParentFile().exists()) {
                filepath.getParentFile().mkdirs();
            }
            //将上传文件保存到一个目标文件当中
            file.transferTo(new File(uploadPath + File.separator + filename));

            InputStream is = new FileInputStream(uploadPath + File.separator + filename);

            List<List<Object>> TimeTabless = new ImportExcelUtil().getBankListByExcel(is, filename);

            for (List<Object> timeTables : TimeTabless) {
                TimeTables timeTable = new TimeTables();
                timeTable.setYear(new Integer(timeTables.get(0).toString()));
                timeTable.setTerm(new Integer(timeTables.get(1).toString()));
                timeTable.setCourseNum(timeTables.get(2).toString());
                timeTable.setClassNum(timeTables.get(3).toString());
                timeTable.setFromWeek(new Integer(timeTables.get(4).toString()));
                timeTable.setToWeek(new Integer(timeTables.get(5).toString()));
                timeTable.setWeekCycle(new Integer(timeTables.get(6).toString()));

                timeTable.setCourseArrangement(timeTables.get(7).toString());
                timeTable.setClassRoomNum(timeTables.get(8).toString());
                timeTable.setTeacherNum(timeTables.get(9).toString());

                //2 添加课程表
                int result = timeTablesInfoService.insert(timeTable);

                System.out.println("添加用户的操作结果为：" + result);

            }
            return "success";
        } else {
            return "error";
        }

    }


}
