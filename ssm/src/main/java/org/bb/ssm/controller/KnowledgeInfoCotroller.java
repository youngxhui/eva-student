package org.bb.ssm.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.bb.ssm.model.Knowledge;
import org.bb.ssm.service.KnowledgeInfoService;
import org.bb.ssm.tool.ImportExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value = "/knowledge")
public class KnowledgeInfoCotroller {

	@Autowired
	private KnowledgeInfoService knowledgeInfoService;

	/**
	 * 知识点列表页
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/index")
	public String index(Map<String, Object> map) {
		// List<knowledge> knowledgeList = knowledgeInfoService.findAll();
		// map.put("ALLknowledge", knowledgeList);
		return "bui/acdemic/knowledgeList";
	}
	
	/**
	 * 查看视频
	 * 
	 * @param map
	 * @return
	 */
	/*@RequestMapping(value = "/index")
	public String showVideo(int knowledgeId) {
		
		
		return "bui/acdemic/showVideo";
	}*/
	

	/**
	 * 得到所有知识点信息
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/getAllKnowledge", method = {RequestMethod.GET,RequestMethod.POST})
	@ResponseBody
	public String getAllKnowledge(@RequestParam(value="limit",required=false) Integer limit,@RequestParam(value="pageIndex",required=false) Integer pageIndex,@RequestParam(value="searchname",required=false) String searchname,@RequestParam(value="course_id",required=false) Integer course_id) {
		pageIndex=pageIndex*limit;
		List<Knowledge> knowledgeList = knowledgeInfoService.findAllByPage(limit, pageIndex, searchname, course_id);

		HashMap<String, Object> tknowledge = new HashMap<String, Object>();

		tknowledge.put("rows", knowledgeList);
		tknowledge.put("results", knowledgeInfoService.totalCount(searchname,course_id));

		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(tknowledge);

			// System.out.println(jsondata);

			return jsondata;

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}


	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
	public String upload(HttpServletRequest request) throws IOException {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		String targetFile = null;
		String fileName = null;
		String newFileName = null;
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			MultipartFile myfile = entity.getValue();
			
			String uploadPath = request.getSession().getServletContext().getRealPath("/resourse/upload");
			//String uploadPath = request.getSession().getServletContext().getRealPath("http://localhost:8080/ssmStage/resourse/upload");
			fileName = myfile.getOriginalFilename();
			
			newFileName = new Date().getTime() + fileName.substring(fileName.lastIndexOf('.'));
			
			targetFile = uploadPath + "/" + newFileName;
			
			
			
			//byte[] bs = myfile.getBytes();
			File file = new File(targetFile);
			FileOutputStream fos = new FileOutputStream(file);
			//fos.write(bs);
			
			IOUtils.copy(myfile.getInputStream(), fos);
			fos.close();
		}
		
		return "{\"url\" : \"" + newFileName + "\"}";
		//return "{\"url\" : \"/resourse/upload/upload1.mp4\"}";

	}
	
	/**
	 * 查看某个知识点对应的视频
	 * 
	 * @param knowledgeinfo
	 * @return
	 */
	@RequestMapping(value = "/showVideo/{video}", method = {RequestMethod.GET})
	public String showVideo(@PathVariable("video") String video) {
		
		
	
		return "bui/acdemic/showVideo";
	}

	/**
	 * 添加知识点操作
	 * 
	 * @param knowledgeinfo
	 * @return
	 */
	@RequestMapping(value = "/addKnowledge", method = {RequestMethod.GET,RequestMethod.POST})
	public String addKnowledge(Knowledge knowledgeinfo) {
		System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxx"+knowledgeinfo);
		try {
			knowledgeinfo.setName(new String(knowledgeinfo.getName().getBytes("iso-8859-1"),"utf-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int result = knowledgeInfoService.insert(knowledgeinfo);
		
		System.out.println("添加知识点的操作结果为：" + result);
		
		return "redirect:/knowledge/getAllKnowledge?limit=10&pageIndex=0&searchname=null&course_id=0";
	}

	/**
	 * 删除知识点操作
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/delete",method={RequestMethod.POST,RequestMethod.GET})
	public String delete(@RequestParam(value="ids[]") String[] ids){
		knowledgeInfoService.deleteByPrimaryKey(ids);
		return "redirect:/knowledge/getAllKnowledge?limit=10&pageIndex=0&searchname=null&course_id=0";
	}
	/*@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "id") int id) {
		int result = knowledgeInfoService.deleteByPrimaryKey(id);
		System.out.println("删除知识点的操作结果为：" + result + "传递进来的id为：" + id);
		return "redirect:/knowledge/getAllKnowledge?limit=10&pageIndex=0&searchname=null&course_id=0";
	}*/

	/**
	 * 更新前先根据id找到知识点信息，回显到页面上
	 * 
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public String input(@PathVariable(value = "id") Integer id,
			Map<String, Object> map) {
		map.put("command", knowledgeInfoService.selectByPrimaryKey(id));
		return "addknowledge";
	}

	@ModelAttribute
	public void getknowledgeInfo(
			@RequestParam(value = "knowledgeId", required = false) Integer id,
			Map<String, Object> map) {
		System.out.println("每个controller 方法都会先调用我哦");
		if (id != null) {
			System.out.println("update 操作");
			map.put("knowledgeInfo",
					knowledgeInfoService.selectByPrimaryKey(id));
		}
		System.out.println("insert  操作");
	}

	@RequestMapping(value = "/updateKnowledge", method = RequestMethod.POST)
	public String update(Knowledge knowledgeinfo) {
		
		knowledgeInfoService.updateByPrimaryKey(knowledgeinfo);
		return "redirect:/knowledge/getAllKnowledge?limit=10&pageIndex=0&searchname=null&course_id=0";
	}
	
	//上传文件会自动绑定到MultipartFile中
    @RequestMapping(value="/importKnowledges",method=RequestMethod.POST)
    public String upload(HttpServletRequest request,
           @RequestParam("excelFile") MultipartFile file) throws Exception {

       //如果文件不为空，写入上传路径
       if(!file.isEmpty()) {
           //上传文件路径
    	   	   String uploadPath = request.getSession().getServletContext().getRealPath("/resourse/uploadKnowledgesInfo");
           //上传文件名
           String filename = file.getOriginalFilename();
           File filepath = new File(uploadPath,filename);
           
           //判断路径是否存在，如果不存在就创建一个
           if (!filepath.getParentFile().exists()) { 
               filepath.getParentFile().mkdirs();
           }
           //将上传文件保存到一个目标文件当中
           file.transferTo(new File(uploadPath + File.separator + filename));
           
           InputStream is = new FileInputStream(uploadPath + File.separator + filename);
           
           List<List<Object>> Knowledges = new ImportExcelUtil().getBankListByExcel(is, filename);
           
           for(List<Object> knowledge : Knowledges)
           {
        	   		Knowledge know = new Knowledge();
	   	   		know.setName(knowledge.get(0).toString());
	   	   		know.setIs_difficult(new Integer(knowledge.get(1).toString()));
	   	   		know.setIs_important(new Integer(knowledge.get(2).toString()));
	   	   		know.setCourse_id(new Integer(knowledge.get(3).toString()));
	   	   		know.setChapter_id(new Integer(knowledge.get(4).toString()));
        	   		
        	   		//2 添加学生
        	   		int result = knowledgeInfoService.insert(know);
        	   		
        	   		System.out.println("添加用户的操作结果为："+result);
        	   		
           }
           
           
           return "success";
       } else {
           return "error";
       }

    }

    
}
