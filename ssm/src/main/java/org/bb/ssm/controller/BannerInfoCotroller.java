package org.bb.ssm.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.bb.ssm.model.Banner;
import org.bb.ssm.service.BannerInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value="/banner")
public class BannerInfoCotroller {
	
	@Autowired
	private BannerInfoService bannerInfoService;
	
	/**
	 * 课程列表页
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/index")
	public String index(Map<String, Object> map){
		//List<banner> bannerList = bannerInfoService.findAll();
		//map.put("ALLbanner", bannerList);
		return "bui/acdemic/bannerList";
	}
	
	/**
	 * 得到所有课程信息
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/getAllBanner",method= {RequestMethod.GET , RequestMethod.POST})
	@ResponseBody
	public String getAllBanner(Object pageinfo,Map<String, Object> map){
		System.out.println(pageinfo);
		List<Banner> bannerList = bannerInfoService.findAll();
		
		HashMap<String,Object > tbanner = new HashMap<String,Object >();
		
		tbanner.put("rows", bannerList);
		tbanner.put("results", bannerList.size());
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(tbanner);

			//System.out.println(jsondata);
			
			return jsondata;
			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 添加课程操作
	 * @param bannerinfo
	 * @return
	 */
	@RequestMapping(value="/addBanner",method=RequestMethod.POST)
	public String save(Banner bannerinfo){
		int result = bannerInfoService.insert(bannerinfo);
		System.out.println("添加课程的操作结果为："+result);
		return "redirect:/banner/getAllBanner";
	}
	/**
	 * 删除课程操作
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	public String delete(String ids){
		
		List<String> idss = Arrays.asList(ids.split(","));
		int result = bannerInfoService.deleteByPrimaryKeys(idss);
		System.out.println("删除课程的操作结果为："+result+"传递进来的id为："+ids);
		return "redirect:/banner/getAllBanner";
	}
	/**
	 * 更新前先根据id找到课程信息，回显到页面上
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/detail/{id}",method=RequestMethod.GET)
	public String input(@PathVariable(value="id") Integer id,Map<String, Object> map){
		map.put("command", bannerInfoService.selectByPrimaryKey(id));
		return "addbanner";
	}
	
	@ModelAttribute
	public void getbannerInfo(@RequestParam(value="bannerId",required=false) Integer id
			,Map<String, Object> map){
		System.out.println("每个controller 方法都会先调用我哦");
		if(id != null){
			System.out.println("update 操作");
			map.put("bannerInfo", bannerInfoService.selectByPrimaryKey(id));
		}
		System.out.println("insert  操作");
	}
	
	@RequestMapping(value="/addbanner",method=RequestMethod.PUT)
	public String update(Banner bannerinfo){
		bannerInfoService.updateByPrimaryKey(bannerinfo);
		return "redirect:/banner/getAllBanner";
	}
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
	public String upload(HttpServletRequest request) throws IOException {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		String targetFile = null;
		String fileName = null;
		String newFileName = null;
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			MultipartFile myfile = entity.getValue();
			String uploadPath = request.getSession().getServletContext().getRealPath("/resourse/uploadBanner");
			fileName = myfile.getOriginalFilename();
			
			newFileName = new Date().getTime() + fileName.substring(fileName.lastIndexOf('.'));
			
			targetFile = uploadPath + "/" + newFileName;
			
			
			byte[] bs = myfile.getBytes();
			File file = new File(targetFile);
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(bs);
			fos.close();
		}
		
		return "{\"url\" : \"" + newFileName + "\"}";
		//return "{\"url\" : \"/resourse/upload/upload1.mp4\"}";

	}
}
