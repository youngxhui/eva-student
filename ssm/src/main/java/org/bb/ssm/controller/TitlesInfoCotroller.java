package org.bb.ssm.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.bb.ssm.model.Titles;
import org.bb.ssm.service.TitlesInfoService;
import org.bb.ssm.tool.ImportExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value="/titles")
public class TitlesInfoCotroller {

	@Autowired
	private TitlesInfoService titlesInfoService;

	/**
	 * 用户列表页
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/index")
	public String index(Map<String, Object> map){

		return "bui/acdemic/titlesList";
	}


	/**
	 * 得到所有知识点信息
	 *
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/getAllTitles", method = {RequestMethod.GET,RequestMethod.POST})
	@ResponseBody
	public String getAllTitles(@RequestParam(value="limit",required=false) Integer limit,@RequestParam(value="pageIndex",required=false) Integer pageIndex,@RequestParam(value="searchname",required=false) String searchname) {
		pageIndex=pageIndex*limit;
		List<Titles> titlesList = titlesInfoService.findAllByPage(limit, pageIndex, searchname);

		HashMap<String, Object> tknowledge = new HashMap<String, Object>();

		tknowledge.put("rows", titlesList);
		tknowledge.put("results", titlesInfoService.totalCount(searchname));

		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(tknowledge);

			// System.out.println(jsondata);

			return jsondata;

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}




	/**
	 * 添加用户操作
	 * @param Titlesinfo
	 * @return
	 */
	@RequestMapping(value="/addTitles",method=RequestMethod.POST)
	public String save(Titles Titlesinfo){
		int result = titlesInfoService.insert(Titlesinfo);
		System.out.println("添加用户的操作结果为："+result);

		return "redirect:/titles/getAllTitles?limit=10&pageIndex=0&searchname=null";
	}
	/**
	 * 删除用户操作
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public String delete(@PathVariable(value="id") int id){
		int result = titlesInfoService.deleteByPrimaryKey(id);
		System.out.println("删除用户的操作结果为："+result+"传递进来的id为："+id);
		return "redirect:/titles/getAllTitles";
	}
	/**
	 * 更新前先根据id找到用户信息，回显到页面上
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/detail/{id}",method=RequestMethod.GET)
	public String input(@PathVariable(value="id") Integer id,Map<String, Object> map){
		map.put("command", titlesInfoService.selectByPrimaryKey(id));
		return "addTitles";
	}

	@ModelAttribute
	public void getTitlesInfo(@RequestParam(value="TitlesId",required=false) Integer id
			,Map<String, Object> map){
		System.out.println("每个controller 方法都会先调用我哦");
		if(id != null){
			System.out.println("update 操作");
			map.put("TitlesInfo", titlesInfoService.selectByPrimaryKey(id));
		}
		System.out.println("insert  操作");
	}

	@RequestMapping(value="/updateTitles",method=RequestMethod.GET)
	public String update(Integer id , Map<String , Object> map){
		Titles title = titlesInfoService.selectByPrimaryKey(id);

		map.put("title" , title);

		return "bui/acdemic/titlesUpdateInput";
	}

	@RequestMapping(value="/updateTitles",method=RequestMethod.POST)
	public String update(Titles Titlesinfo){

		Titlesinfo.setKnowledgeId(Titlesinfo.getKnowledgeId().substring(3));

		titlesInfoService.updateByPrimaryKey(Titlesinfo);

		return "redirect:/titles/updateTitles?id="+Titlesinfo.getId();
	}

	//上传文件会自动绑定到MultipartFile中
	@RequestMapping(value="/importTitles",method=RequestMethod.POST)
	public String upload(HttpServletRequest request,
						 @RequestParam("excelFile") MultipartFile file) throws Exception {

		//如果文件不为空，写入上传路径
		if(!file.isEmpty()) {
			//上传文件路径
			String uploadPath = request.getSession().getServletContext().getRealPath("/resourse/uploadTitlesInfo");
			//上传文件名
			String filename = file.getOriginalFilename();
			File filepath = new File(uploadPath,filename);

			//判断路径是否存在，如果不存在就创建一个
			if (!filepath.getParentFile().exists()) {
				filepath.getParentFile().mkdirs();
			}
			//将上传文件保存到一个目标文件当中
			file.transferTo(new File(uploadPath + File.separator + filename));

			InputStream is = new FileInputStream(uploadPath + File.separator + filename);

			List<List<Object>> titles = new ImportExcelUtil().getBankListByExcel(is, filename);

			System.out.println("读到了"+titles.size()+"条数据，准备导入...");

			for(List<Object> title : titles)
			{
				Titles tit = new Titles();
				//设置好题目考察的知识点id 一个题目可能考察多个知识点 但是一定有一个主要考察的知识点
				tit.setKnowledgeId(title.get(3).toString());

				tit.setTitle(title.get(4).toString());
				tit.setCategory(new Integer(title.get(5).toString()));
				tit.setDifficulty(new Integer(title.get(6).toString()));
				tit.setAnswer(title.get(7).toString());
				tit.setAnalysis(title.get(8).toString());
				tit.setTeacherId(new Integer(title.get(9).toString()));
				tit.setAddTime(new Timestamp(new Date().getTime()));

				if(tit.getCategory() == 1)//如果是单选题
				{
					tit.setSectiona(title.get(10).toString());
					tit.setSectionb(title.get(11).toString());
					tit.setSectionc(title.get(12).toString());
					tit.setSectiond(title.get(13).toString());
				}

				if(tit.getCategory() == 1)
				{
					tit.setOrderd(new Integer(title.get(14).toString()));
				}

				System.out.println(tit);

				titlesInfoService.insert(tit);

			}



			return "success";
		} else {
			return "error";
		}

	}
}
