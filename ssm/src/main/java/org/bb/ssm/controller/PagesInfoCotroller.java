
package org.bb.ssm.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.bb.ssm.model.Pages;
import org.bb.ssm.model.PagesTitle;
import org.bb.ssm.model.Titles;
import org.bb.ssm.service.PagesInfoService;
import org.bb.ssm.service.TitlesInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value = "/pages")
public class PagesInfoCotroller {

	@Autowired
	private PagesInfoService pagesInfoService;
	
	@Autowired
	private TitlesInfoService titlesInfoService;

	/**
	 * 试卷列表页
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/index")
	public String index(Map<String, Object> map) {
		
		return "bui/evaluate/pagesList";
	}
	
	
	
	/**
	 * 得到所有试卷信息
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/getAllPages", method = {RequestMethod.GET,RequestMethod.POST})
	@ResponseBody
	public String getAllPages(@RequestParam(value="limit",required=false) Integer limit,@RequestParam(value="pageIndex",required=false) Integer pageIndex,@RequestParam(value="searchname",required=false) String searchname,@RequestParam(value="courseId",required=false,defaultValue="0") Integer courseId) {
		pageIndex=pageIndex*limit;
		List<Pages> pagesList = pagesInfoService.findAllByPage(limit, pageIndex, searchname, courseId);

		HashMap<String, Object> tpages = new HashMap<String, Object>();

		tpages.put("rows", pagesList);
		tpages.put("results", pagesInfoService.totalCount(searchname,courseId));

		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(tpages);

			// System.out.println(jsondata);

			return jsondata;

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}


	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
	public String upload(HttpServletRequest request) throws IOException {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		String targetFile = null;
		String fileName = null;
		String newFileName = null;
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			MultipartFile myfile = entity.getValue();
			
			String uploadPath = request.getSession().getServletContext().getRealPath("/resourse/upload");
			//String uploadPath = request.getSession().getServletContext().getRealPath("http://localhost:8080/ssmStage/resourse/upload");
			fileName = myfile.getOriginalFilename();
			
			newFileName = new Date().getTime() + fileName.substring(fileName.lastIndexOf('.'));
			
			targetFile = uploadPath + "/" + newFileName;
			
			
			byte[] bs = myfile.getBytes();
			File file = new File(targetFile);
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(bs);
			fos.close();
		}
		
		return "{\"url\" : \"" + newFileName + "\"}";
		//return "{\"url\" : \"/resourse/upload/upload1.mp4\"}";

	}
	
	

	
	/**
	 * 自动组卷
	 * 
	 * @param pagesId 试卷id 实际会根据试卷id找
	 * @return
	 */
	@RequestMapping(value = "/autoGeneratorPage/pagesId/{pagesId}/courseId/{courseId}/chapterId/{chapterId}" , method = {RequestMethod.GET} )
	public String autoGeneratorPage(@PathVariable("pagesId") Integer pagesId , @PathVariable("courseId") Integer courseId , @PathVariable("chapterId") Integer chapterId ,  Map<String, Object> map) {
		
		//1. 根据pagesId找到课程id，根据规则产生一套该课程的试卷
		//   组卷规则默认是4种题型都具备 单选10道 每题4分 填空5道 每题4分 简答2道 每题10分 编程2道 每题10分
 		
		List<Titles> titles = titlesInfoService.findByCourseId(courseId , chapterId);
		
		List<Titles> singleChoiceTitles = new ArrayList<Titles>();
		List<Titles> fillInBlankTitles = new ArrayList<Titles>();
		List<Titles> shortAnswerTitles = new ArrayList<Titles>();
		List<Titles> programTitles = new ArrayList<Titles>();
		
		for(Titles title : titles)
		{
			int category = title.getCategory();
			
			if(category == 1)
			{
				singleChoiceTitles.add(title);
			}
			
			if(category == 2)
			{
				fillInBlankTitles.add(title);
			}
			
			if(category == 3)
			{
				shortAnswerTitles.add(title);
			}
			
			if(category == 4)
			{
				programTitles.add(title);
			}
		}
		
		int singleChoiceTitlesCount = singleChoiceTitles.size();
		
		if(singleChoiceTitles.size() <8 || fillInBlankTitles.size()<4 || shortAnswerTitles.size() < 4)
		{
			System.out.println(singleChoiceTitles.size()+"--"+fillInBlankTitles.size() + "--" + shortAnswerTitles.size() + "--" + programTitles.size());
			return "redirect:/pages/index";
		}
		List<Integer> titleIds = new ArrayList<Integer>();
		// 随机生成10道单选题
		for(int i = 0; i<8 ;i++)
		{
			Random rand = new Random();
			int titleId = rand.nextInt(singleChoiceTitlesCount);
			
			if(titleIds.contains(titleId))
			{
				i--;
			}else
			{
				titleIds.add(titleId);
				
				PagesTitle pt = new PagesTitle();
				pt.setPages_id(pagesId);
				pt.setTitle_id(singleChoiceTitles.get(titleId).getId());
				pagesInfoService.autoGeneratorPage(pt);
			}
			
		}
		
		titleIds.clear();
		// 随机生成4道填空题
		for(int i = 0; i<4 ;i++)
		{
			Random rand = new Random();
			int titleId = rand.nextInt(fillInBlankTitles.size());
			
			if(titleIds.contains(titleId))
			{
				i--;
			}else
			{
				titleIds.add(titleId);
				
				PagesTitle pt = new PagesTitle();
				pt.setPages_id(pagesId);
				pt.setTitle_id(fillInBlankTitles.get(titleId).getId());
				pagesInfoService.autoGeneratorPage(pt);
			}
			
		}
		
		titleIds.clear();
		// 随机生成4道简答题
		for(int i = 0; i<4 ;i++)
		{
			Random rand = new Random();
			int titleId = rand.nextInt(shortAnswerTitles.size());
			
			if(titleIds.contains(titleId))
			{
				i--;
			}else
			{
				titleIds.add(titleId);
				
				PagesTitle pt = new PagesTitle();
				pt.setPages_id(pagesId);
				pt.setTitle_id(shortAnswerTitles.get(titleId).getId());
				pagesInfoService.autoGeneratorPage(pt);
			}
			
		}
		
//		titleIds.clear();
		// 随机生成2道程序题
//		for(int i = 0; i<2 ;i++)
//		{
//			Random rand = new Random();
//			int titleId = rand.nextInt(programTitles.size());
//			
//			if(titleIds.contains(titleId))
//			{
//				i--;
//			}else
//			{
//				titleIds.add(titleId);
//				
//				PagesTitle pt = new PagesTitle();
//				pt.setPages_id(pagesId);
//				pt.setTitle_id(programTitles.get(titleId).getId());
//				pagesInfoService.autoGeneratorPage(pt);
//			}
//			
//		}
		
		//2. 1成功之后 ，根据pagesId更新下这条pages的status，组卷状态改成已经组卷2
		Pages pages = pagesInfoService.selectByPrimaryKey(pagesId);
		pages.setPagesStatus(2);;//将试卷状态修改成已经组卷
		
		pagesInfoService.updateByPrimaryKey(pages);
		
		return "redirect:/pages/index";
	}
	

	/**
	 * 查看某个试卷详情
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/getPage/pagesId/{pagesId}" , method = {RequestMethod.GET} )
	public String showDetail(@PathVariable("pagesId") Integer pagesId , Map<String, Object> map) {
		
		
		List<Titles> titles = titlesInfoService.findByPagesId(pagesId);
		
		map.put("pagesId", pagesId);
		map.put("titles", titles);
		
		return "bui/evaluate/getPage";
	}
	

	/**
	 * 换题
	 * 删除一道题
	 * 添加一道题
	 * @param pagesId 试卷id 实际会根据试卷id找
	 * @return
	 */
	@RequestMapping(value = "/changeTitle/pagesId/{pagesId}/courseId/{courseId}/chapterId/{chapterId}/tId/{tId}/categoryNum/{categoryNum}" , method = {RequestMethod.GET} )
	public String changeTitle(@PathVariable("pagesId") Integer pagesId , @PathVariable("courseId") Integer courseId 
			, @PathVariable("chapterId") Integer chapterId , @PathVariable("tId") Integer tId , @PathVariable("categoryNum") Integer categoryNum ,  Map<String, Object> map) {
		
		List<Titles> titlesByPageId = titlesInfoService.findByPagesId(pagesId);
		List<Integer> titleIds = new ArrayList<Integer>();
		
		for (Titles t : titlesByPageId) {
			titleIds.add(t.getId());
		}
		
	
		//1. 根据pagesId找到课程id，根据规则产生一套该课程的试卷
		//   组卷规则默认是4种题型都具备 单选10道 每题4分 填空5道 每题4分 简答2道 每题10分 编程2道 每题10分
 		
		List<Titles> titles = titlesInfoService.findByCourseId(courseId , chapterId);
		
		List<Titles> singleChoiceTitles = new ArrayList<Titles>();
		List<Titles> fillInBlankTitles = new ArrayList<Titles>();
		List<Titles> shortAnswerTitles = new ArrayList<Titles>();
		List<Titles> programTitles = new ArrayList<Titles>();
		
		for(Titles title : titles)
		{
			int category = title.getCategory();
			
			if(category == 1)
			{
				singleChoiceTitles.add(title);
			}
			
			if(category == 2)
			{
				fillInBlankTitles.add(title);
			}
			
			if(category == 3)
			{
				shortAnswerTitles.add(title);
			}
			
			if(category == 4)
			{
				programTitles.add(title);
			}
		}
		
		switch (categoryNum) {
		case 1:
			
			while(true)
			{
				Random rand = new Random();
				int index = rand.nextInt(singleChoiceTitles.size());
				Titles title = singleChoiceTitles.get(index);
				
				if(!titleIds.contains(title.getId()))
				{
					PagesTitle pt = new PagesTitle();
				
					pt.setPages_id(pagesId);
					pt.setTitle_id(tId);
					pt.setTitle_id_new(title.getId());
					
					pagesInfoService.changeTitle(pt);
					break;
				}
			}
		
			break;
			
		case 2:
			
			while(true)
			{
				Random rand = new Random();
				int index = rand.nextInt(fillInBlankTitles.size());
				Titles title = fillInBlankTitles.get(index);
				
				if(!titleIds.contains(title.getId()))
				{
					PagesTitle pt = new PagesTitle();
				
					pt.setPages_id(pagesId);
					pt.setTitle_id(tId);
					pt.setTitle_id_new(title.getId());
					
					pagesInfoService.changeTitle(pt);
					break;
				}
			}
			
			break;
			
		case 3:
			
			while(true)
			{
				Random rand = new Random();
				int index = rand.nextInt(shortAnswerTitles.size());
				Titles title = shortAnswerTitles.get(index);
				
				if(!titleIds.contains(title.getId()))
				{
					PagesTitle pt = new PagesTitle();
				
					pt.setPages_id(pagesId);
					pt.setTitle_id(tId);
					pt.setTitle_id_new(title.getId());
					
					pagesInfoService.changeTitle(pt);
					break;
				}
			}
			
			break;
			
		case 4:
		
			while(true)
			{
				Random rand = new Random();
				int index = rand.nextInt(programTitles.size());
				Titles title = programTitles.get(index);
				
				if(!titleIds.contains(title.getId()))
				{
					PagesTitle pt = new PagesTitle();
				
					pt.setPages_id(pagesId);
					pt.setTitle_id(tId);
					pt.setTitle_id_new(title.getId());
					
					pagesInfoService.changeTitle(pt);
					break;
				}
			}
			
			break;
		
		default:
			break;
		}
		
	
		
		return "redirect:/pages//getPage/pagesId/"+pagesId;
	}
	
	
	
	/**
	 * 查看某个试卷对应的视频
	 * 
	 * @param pagesinfo
	 * @return
	 */
	@RequestMapping(value = "/showVideo/{video}", method = {RequestMethod.GET})
	public String showVideo(@PathVariable("video") String video) {
		
		
	
		return "bui/acdemic/showVideo";
	}

	
	
	/**
	 * 添加试卷操作
	 * 
	 * @param pagesinfo
	 * @return
	 */
	@RequestMapping(value = "/addPages", method = {RequestMethod.POST})
	public String addPages(String courseId , String chapterId , String name) {
		
		Pages pages = new Pages();
		pages.setName(name);
		pages.setCourseId(new Integer(courseId.substring(3)));
		
		if(chapterId != null && !"".equals(chapterId+""))
		{
			pages.setChapterId(new Integer(chapterId.substring(3)));
		}
		
		pages.setCreateTime(new Timestamp(new Date().getTime()));
		pages.setPagesStatus(1);
		
		pagesInfoService.insert(pages);
		
		
		return "redirect:/pages/getAllPages?limit=10&pageIndex=0&searchname=null&course_id=0";
	}

	/**
	 * 删除试卷操作
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/delete",method={RequestMethod.POST,RequestMethod.GET})
	public String delete(@RequestParam(value="ids[]") String[] ids){
		pagesInfoService.deleteByPrimaryKey(ids);
		return "redirect:/pages/getAllPages?limit=10&pageIndex=0&searchname=null&course_id=0";
	}
	/*@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "id") int id) {
		int result = pagesInfoService.deleteByPrimaryKey(id);
		System.out.println("删除试卷的操作结果为：" + result + "传递进来的id为：" + id);
		return "redirect:/pages/getAllPages?limit=10&pageIndex=0&searchname=null&course_id=0";
	}*/

	/**
	 * 更新前先根据id找到试卷信息，回显到页面上
	 * 
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public String input(@PathVariable(value = "id") Integer id,
			Map<String, Object> map) {
		map.put("command", pagesInfoService.selectByPrimaryKey(id));
		return "addpages";
	}

	@ModelAttribute
	public void getpagesInfo(
			@RequestParam(value = "pagesId", required = false) Integer id,
			Map<String, Object> map) {
		System.out.println("每个controller 方法都会先调用我哦");
		if (id != null) {
			System.out.println("update 操作");
			map.put("pagesInfo",
					pagesInfoService.selectByPrimaryKey(id));
		}
		System.out.println("insert  操作");
	}

	@RequestMapping(value = "/updatePages", method = RequestMethod.POST)
	public String update(Pages pagesinfo) {
		
		pagesInfoService.updateByPrimaryKey(pagesinfo);
		return "redirect:/pages/getAllPages?limit=10&pageIndex=0&searchname=null&course_id=0";
	}
	

    
}
