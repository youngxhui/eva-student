package org.bb.ssm.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.bb.ssm.model.ClassScore;
import org.bb.ssm.model.Exam;
import org.bb.ssm.model.StudentScore;
import org.bb.ssm.model.StudentScores;
import org.bb.ssm.model.Students;
import org.bb.ssm.service.ClassInfoService;
import org.bb.ssm.service.ExamInfoService;
import org.bb.ssm.service.StudentsInfoService;
import org.bb.ssm.tool.PoiExcelExport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value = "/exam")
public class ExamInfoCotroller {

	@Autowired
	private ExamInfoService examInfoService;
	
	@Autowired
	private StudentsInfoService studentsInfoService;
	
	@Autowired
	private ClassInfoService classInfoService;

	/**
	 * 试卷列表页
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/index")
	public String index(Map<String, Object> map) {
		
		 List<Exam> examList = examInfoService.findAll();
		 map.put("exams", examList);
		 
		 return "bui/evaluate/examList";
	}
	
	
	
	/**
	 * 查看视频
	 * 
	 * @param map
	 * @return
	 */
	/*@RequestMapping(value = "/index")
	public String showVideo(int examId) {
		
		
		return "bui/acdemic/showVideo";
	}*/
	

	/**
	 * 得到所有试卷信息
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/getAllExam", method = {RequestMethod.GET,RequestMethod.POST})
	@ResponseBody
	public String getAllExam(@RequestParam(value="limit",required=false) Integer limit,@RequestParam(value="pageIndex",required=false) Integer pageIndex,@RequestParam(value="searchname",required=false) String searchname,@RequestParam(value="course_id",required=false) Integer course_id) {
		pageIndex=pageIndex*limit;
		List<Exam> examList = examInfoService.findAllByPage(limit, pageIndex, searchname, course_id);

		HashMap<String, Object> texam = new HashMap<String, Object>();

		texam.put("rows", examList);
		texam.put("results", examInfoService.totalCount(searchname,course_id));

		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(texam);

			// System.out.println(jsondata);

			return jsondata;

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 成绩查看页
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/lookScore")
	public String lookScore(Map<String, Object> map) {
		 
		 return "bui/evaluate/lookScore";
	}
	
	
	/**
	 * 得到所有学生的考试成绩
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/getAllStudentScore", method = {RequestMethod.GET,RequestMethod.POST})
	@ResponseBody
	public String getAllStudentScore(@RequestParam(value="limit",required=false) Integer limit,@RequestParam(value="pageIndex",required=false) Integer pageIndex,@RequestParam(value="searchname",required=false) String searchname,@RequestParam(value="pages_id",required=false) Integer pages_id ,@RequestParam(value="class_id",required=false) Integer class_id) {
		pageIndex=pageIndex*limit;
		
		pages_id = pages_id==null? 0 : pages_id;
		
		List<StudentScore> studentScores = examInfoService.lookScoreByPage(limit, pageIndex, searchname, pages_id , class_id);

		HashMap<String, Object> texam = new HashMap<String, Object>();

		texam.put("rows", studentScores);
		texam.put("results", examInfoService.totalCountOfStudentScore(searchname,pages_id , class_id));

		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(texam);

			// System.out.println(jsondata);

			return jsondata;

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 导出成绩到excel
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/exportScores")
	public ResponseEntity<byte[]> exportScores(HttpServletRequest request, Integer pages_id  , Integer class_id ) throws IOException {  
		
		String downLoadPath = request.getSession().getServletContext().getRealPath("/resourse/downloadScores/export_student_scores.xlsx");
		
		
		PoiExcelExport pee = new PoiExcelExport(downLoadPath,"sheet1");  
		
		List<Students> students = studentsInfoService.findStudentsByClassId(class_id);
		List<Integer> studentIds = new ArrayList<Integer>();
		for (Students student : students) {
			studentIds.add(student.getId());
		}
		
		
		List<Exam> exams = examInfoService.findAllByClassId(class_id);
		
		List<Integer> pageIds = new ArrayList<Integer>();
		
		for(Exam exam : exams)
		{
			pageIds.add(exam.getPagesId());
		}
		
		List<List<StudentScore>> sssss = new ArrayList<List<StudentScore>>();
		List<List<Integer>> idss = new ArrayList<List<Integer>>();
		
		org.bb.ssm.model.Class clazz = classInfoService.selectByPrimaryKey(class_id);
		
	
		Integer class_num = new Integer(clazz.getNum());
		
		for(Integer i : pageIds)
		{
			List<StudentScore> studentScores1 = examInfoService.lookScoreByPage(100, 0, null, i , class_id);
			sssss.add(studentScores1);
			
			List<Integer> studentIds1 = new ArrayList<Integer>();
			for (StudentScore ss : studentScores1) {
				studentIds1.add(ss.getStudentId());
			}
			idss.add(studentIds1);
		}
		
		List<StudentScores> stus = new ArrayList<StudentScores>();
		
		for (Students stu : students) {
			
			StudentScores ss = new StudentScores();
			
			ss.setStudentNumber(stu.getStudent_number());
			ss.setStudentName(stu.getName());
			
			List<Integer> scores = new ArrayList<Integer>();
			
			for(int i=0 ;i<idss.size() ;i++)
			{
				List<Integer> idsss = idss.get(i);
				List<StudentScore> studentScores1 = sssss.get(i);
				
				if(idsss.contains(stu.getId()))
				{
					int index = idsss.indexOf(idsss);
					for(StudentScore ssss : studentScores1)
					{
						if(ssss.getStudentId() == stu.getId())
						{
							scores.add(ssss.getScore());
							break;
							
						}
					}
					
				}else
				{
					scores.add(0);
				}
			}
			
			
			
			ss.setScore(scores);
			
			stus.add(ss);
			
		}
	
		
			
      
		
		String titleColumn[] = new String[2+sssss.size()];
		titleColumn[0] = "studentNumber";
		titleColumn[1] = "studentName";
		
		
		
        String[] titleNames = new String[2+sssss.size()];
        titleNames[0] = "学号";
        titleNames[1] = "姓名";
        
        int titleSize[] = new int[2+sssss.size()]; 
        titleSize[0] = 20;
        titleSize[1] = 13;
        
		for(int i=0;i<sssss.size() ;i++)
		{
			titleNames[2+i] = "score"+(i+1);
			titleSize[2+i] = 13;
			titleColumn[2+i] = "score["+i+"]";
		}
		
        
          
        pee.wirteExcel(titleColumn, titleNames, titleSize, stus);  
	        
        
        File file=new File(downLoadPath);  
        HttpHeaders headers = new HttpHeaders();    
        String fileName=new String((class_num+"班级成绩.xlsx").getBytes("UTF-8"),"iso-8859-1");//为了解决中文名称乱码问题  
        headers.setContentDispositionFormData("attachment", fileName);   
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);   
        return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file),    
                                          headers, HttpStatus.CREATED);    
    }    
	
//	/**
//	 * 导出成绩到excel
//	 * 
//	 * @param map
//	 * @return
//	 */
//	@RequestMapping(value = "/exportScores")
//	public ResponseEntity<byte[]> exportScores(HttpServletRequest request, Integer pages_id  , Integer class_id ) throws IOException {  
//		
//		String downLoadPath = request.getSession().getServletContext().getRealPath("/resourse/downloadScores/export_student_scores.xlsx");
//		
//		int examSizeOfClass = 2;
//		
//		PoiExcelExport pee = new PoiExcelExport(downLoadPath,"sheet1");  
//		
//		List<Students> students = studentsInfoService.findStudentsByClassId(1);
//		List<Integer> studentIds = new ArrayList<Integer>();
//		for (Students student : students) {
//			studentIds.add(student.getId());
//		}
//		
//        //数据  
//
//		List<StudentScore> studentScores = examInfoService.lookScoreByPage(100, 0, null, 1 , class_id);
//		List<StudentScore> studentScores1 = examInfoService.lookScoreByPage(100, 0, null, 1 , class_id);
//		List<Integer> studentIds1 = new ArrayList<Integer>();
//		for (StudentScore ss : studentScores1) {
//			studentIds1.add(ss.getStudentId());
//		}
//		
//		List<StudentScore> studentScores2 = examInfoService.lookScoreByPage(100, 0, null, 2 , class_id);
//		List<Integer> studentIds2 = new ArrayList<Integer>();
//		for (StudentScore ss : studentScores2) {
//			studentIds2.add(ss.getStudentId());
//		}
//		
//		List<StudentScores> stus = new ArrayList<StudentScores>();
//		
//		for (Students stu : students) {
//			
//			StudentScores ss = new StudentScores();
//			
//			ss.setStudentNumber(stu.getStudent_number());
//			ss.setStudentName(stu.getName());
//			
//			List<Integer> scores = new ArrayList<Integer>();
//			
//			
//			if(studentIds1.contains(stu.getId()))
//			{
//				for(StudentScore ssss : studentScores1)
//				{
//					if(ssss.getStudentId() == stu.getId())
//					{
//						scores.add(ssss.getScore());
//						break;
//						
//					}
//				}
//				
//			}else
//			{
//				scores.add(0);
//			}
//			
//			
//			if(studentIds2.contains(stu.getId()))
//			{
//				for(StudentScore ssss : studentScores2)
//				{
//					if(ssss.getStudentId() == stu.getId())
//					{
//						scores.add(ssss.getScore());
//						break;
//						
//					}
//				}
//				
//			}else
//			{
//				scores.add(0);
//			}
//			
//			
//		
//			
//			ss.setScore(scores);
//			
//			stus.add(ss);
//			
//		}
//	
//		
//		String pagesName = "";
//		String className = "";
//		if(studentScores.size() > 0)
//		{
//			pagesName = studentScores1.get(0).getPagesName();
//			className = studentScores2.get(1).getClassName();
//		}
//			
//      
//		
//		String titleColumn[] = {"studentNumber","studentName","score[0]" , "score[1]"};  
//		
//        String[] titleNames = new String[4];
//        titleNames[0] = "学号";
//        titleNames[1] = "姓名";
//        
//		for(int i=0;i<2 ;i++)
//		{
//			titleNames[2+i] = "score"+i;
//		}
//		
//        int titleSize[] = {20,13,13,13};  
//          
//        pee.wirteExcel(titleColumn, titleNames, titleSize, stus);  
//	        
//        
//        File file=new File(downLoadPath);  
//        HttpHeaders headers = new HttpHeaders();    
//        String fileName=new String((pagesName+className+"成绩.xlsx").getBytes("UTF-8"),"iso-8859-1");//为了解决中文名称乱码问题  
//        headers.setContentDispositionFormData("attachment", fileName);   
//        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);   
//        return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file),    
//                                          headers, HttpStatus.CREATED);    
//    } 
//	
	
	/**
	 * 查看某一试卷  所有班级的平均分
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/viewCharts")
	public String viewCharts(Map<String, Object> map , Integer pages_id) throws IOException {  
		
		List<ClassScore> classScores = examInfoService.getClassAvgScoreByPagesId(pages_id);
		
		String jsondata = "";
	
		ObjectMapper mapper = new ObjectMapper();
		try {
			jsondata = mapper.writeValueAsString(classScores);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		map.put("pagesId", pages_id);
		if(classScores.size() > 0 )
		{
			map.put("pagesName", classScores.get(0).getPagesName());
		}
		
		map.put("classScores", jsondata);
		
		return "bui/evaluate/classScoresCharts";
    }  
	
	/**
	 * 查看班级学生报表
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/viewStudentsCharts")
	public String viewStudentsCharts(Map<String, Object> map , Integer pages_id , Integer class_id ) throws IOException {  
		
		List<StudentScore> studentScores = examInfoService.lookScoreByPage(100, 0, null, pages_id , class_id);
		
		String jsondata = "";
	
		ObjectMapper mapper = new ObjectMapper();
		try {
			jsondata = mapper.writeValueAsString(studentScores);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		
		map.put("studentScores", jsondata);
		if(studentScores.size() > 0 )
		{
			map.put("pagesName", studentScores.get(0).getPagesName());
			map.put("className", studentScores.get(0).getClassName());
		}
		
		
		return "bui/evaluate/charts";
    }   
	
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
	public String upload(HttpServletRequest request) throws IOException {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		String targetFile = null;
		String fileName = null;
		String newFileName = null;
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			MultipartFile myfile = entity.getValue();
			
			String uploadPath = request.getSession().getServletContext().getRealPath("/resourse/upload");
			//String uploadPath = request.getSession().getServletContext().getRealPath("http://localhost:8080/ssmStage/resourse/upload");
			fileName = myfile.getOriginalFilename();
			
			newFileName = new Date().getTime() + fileName.substring(fileName.lastIndexOf('.'));
			
			targetFile = uploadPath + "/" + newFileName;
			
			
			byte[] bs = myfile.getBytes();
			File file = new File(targetFile);
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(bs);
			fos.close();
		}
		
		return "{\"url\" : \"" + newFileName + "\"}";
		//return "{\"url\" : \"/resourse/upload/upload1.mp4\"}";

	}
	
	/**
	 * 查看某个试卷对应的视频
	 * 
	 * @param examinfo
	 * @return
	 */
	@RequestMapping(value = "/showVideo/{video}", method = {RequestMethod.GET})
	public String showVideo(@PathVariable("video") String video) {
		
		
	
		return "bui/acdemic/showVideo";
	}

	/**
	 * 进入到添加试卷页面
	 * 
	 * @param examinfo
	 * @return
	 */
	@RequestMapping(value = "/addExam", method = {RequestMethod.GET})
	public String addExam() {
		
		
		return "redirect:/exam/getAllExam?limit=10&pageIndex=0&searchname=null&course_id=0";
	}

	
	/**
	 * 添加试卷操作
	 * 
	 * @param examinfo
	 * @return
	 */
	@RequestMapping(value = "/addExam", method = {RequestMethod.POST})
	public String addExam(Exam examinfo) {
		System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxx"+examinfo);
		
		examinfo.setAddTime(new Timestamp(new Date().getTime()));
		int result = examInfoService.insert(examinfo);
		
		System.out.println("添加试卷的操作结果为：" + result);
		
		return "redirect:/exam/getAllExam?limit=10&pageIndex=0&searchname=null&course_id=0";
	}

	/**
	 * 删除试卷操作
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/delete",method={RequestMethod.POST,RequestMethod.GET})
	public String delete(@RequestParam(value="ids[]") String[] ids){
		examInfoService.deleteByPrimaryKey(ids);
		return "redirect:/exam/getAllExam?limit=10&pageIndex=0&searchname=null&course_id=0";
	}
	/*@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "id") int id) {
		int result = examInfoService.deleteByPrimaryKey(id);
		System.out.println("删除试卷的操作结果为：" + result + "传递进来的id为：" + id);
		return "redirect:/exam/getAllExam?limit=10&pageIndex=0&searchname=null&course_id=0";
	}*/

	/**
	 * 更新前先根据id找到试卷信息，回显到页面上
	 * 
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public String input(@PathVariable(value = "id") Integer id,
			Map<String, Object> map) {
		map.put("command", examInfoService.selectByPrimaryKey(id));
		return "addexam";
	}

	@ModelAttribute
	public void getexamInfo(
			@RequestParam(value = "examId", required = false) Integer id,
			Map<String, Object> map) {
		System.out.println("每个controller 方法都会先调用我哦");
		if (id != null) {
			System.out.println("update 操作");
			map.put("examInfo",
					examInfoService.selectByPrimaryKey(id));
		}
		System.out.println("insert  操作");
	}

	@RequestMapping(value = "/updateExam", method = RequestMethod.POST)
	public String update(Exam examinfo) {
		
		examInfoService.updateByPrimaryKey(examinfo);
		
		return "redirect:/exam/getAllExam?limit=10&pageIndex=0&searchname=null&course_id=0";
	}
	

    
}
