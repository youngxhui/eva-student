package org.bb.ssm.mapper;

import java.util.List;

import org.bb.ssm.model.News;

public interface NewsInfoMapper {
	
	List<News> findAll();
	
	List<News> findNewsByKnowledgeId(Integer knowledgeId);
	
    int deleteByPrimaryKey(Integer newsId);

    int insert(News record);

    int insertSelective(News record);

    News selectByPrimaryKey(Integer newsId);

    int updateByPrimaryKeySelective(News record);

    int updateByPrimaryKey(News record);

	News selectByPwd(News record);
	
}