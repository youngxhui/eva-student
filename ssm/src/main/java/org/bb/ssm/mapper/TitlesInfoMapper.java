package org.bb.ssm.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.bb.ssm.model.Knowledge;
import org.bb.ssm.model.Titles;

public interface TitlesInfoMapper {
	
	List<Titles> findAll();
	
	List<Titles> findAllByPage(@Param(value="limit") Integer limit,@Param(value="pageIndex") Integer pageIndex,@Param(value="searchname") String searchname);
	
	int getTitlesCount(@Param(value="searchname") String searchname);
	
	List<Titles> findByPagesId(Integer pagesId);
	
	List<Titles> findByCourseId(@Param(value="courseId")Integer courseId , @Param(value="chapterId")Integer chapterId);
	
    int deleteByPrimaryKey(Integer TitlesId);

    int insert(Titles record);

    int insertSelective(Titles record);

    Titles selectByPrimaryKey(Integer TitlesId);

    int updateByPrimaryKeySelective(Titles record);

    int updateByPrimaryKey(Titles record);

	Titles selectByPwd(Titles record);
}