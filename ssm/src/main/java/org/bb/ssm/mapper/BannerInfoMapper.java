package org.bb.ssm.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.bb.ssm.model.Banner;

public interface BannerInfoMapper {
	
	List<Banner> findAll();
	
    int deleteByPrimaryKey(Integer BannerId);
    
    int deleteByPrimaryKeys(@Param("ids")List<String> ids);
    
    int insert(Banner record);

    int insertSelective(Banner record);

    Banner selectByPrimaryKey(Integer BannerId);

    int updateByPrimaryKeySelective(Banner record);

    int updateByPrimaryKey(Banner record);

	Banner selectByPwd(Banner record);

	int getIdByNum(String num);
}