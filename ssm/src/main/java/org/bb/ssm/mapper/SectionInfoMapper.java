package org.bb.ssm.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.bb.ssm.model.Section;

public interface SectionInfoMapper {
	
	List<Section> findAll();
	
    int deleteByPrimaryKey(@Param(value="ids") String[] ids);

    int insert(Section record);

    int insertSelective(Section record);

    Section selectByPrimaryKey(Integer SectionId);

    int updateByPrimaryKeySelective(Section record);

    int updateByPrimaryKey(Section record);

	Section selectByPwd(Section record);

	List<Section> findAllByPage(@Param(value="limit") Integer limit,@Param(value="pageIndex") Integer pageIndex,@Param(value="searchname") String searchname,@Param(value="course_id") Integer course_id);

	int getSectionCount(@Param(value="searchname") String searchname,@Param(value="course_id") Integer course_id);
}