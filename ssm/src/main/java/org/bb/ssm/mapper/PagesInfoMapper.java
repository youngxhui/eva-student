package org.bb.ssm.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.bb.ssm.model.Pages;
import org.bb.ssm.model.PagesTitle;

public interface PagesInfoMapper {
	
	List<Pages> findAll();
	
    int deleteByPrimaryKey(@Param(value="ids") String[] ids);

    int autoGeneratorPage(PagesTitle record);
    
    int changeTitle(PagesTitle record);
    
    int insert(Pages record);

    int insertSelective(Pages record);

    Pages selectByPrimaryKey(Integer PagesId);

    int updateByPrimaryKeySelective(Pages record);

    int updateByPrimaryKey(Pages record);

	Pages selectByPwd(Pages record);

	List<Pages> findAllByPage(@Param(value="limit") Integer limit,@Param(value="pageIndex") Integer pageIndex,@Param(value="searchname") String searchname,@Param(value="course_id") Integer course_id);

	int getPagesCount(@Param(value="searchname") String searchname,@Param(value="course_id") Integer course_id);
}