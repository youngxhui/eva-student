package org.bb.ssm.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.bb.ssm.model.TimeTables;

public interface TimeTablesInfoMapper {
	
	List<TimeTables> findAll();
	
    int deleteByPrimaryKey(@Param(value="ids") String[] ids);

    int insert(TimeTables record);

    int insertSelective(TimeTables record);

    TimeTables selectByPrimaryKey(Integer TimeTablesId);

    int updateByPrimaryKeySelective(TimeTables record);

    int updateByPrimaryKey(TimeTables record);

	TimeTables selectByPwd(TimeTables record);

	List<TimeTables> findAllByPage(@Param(value="limit") Integer limit,@Param(value="pageIndex") Integer pageIndex,@Param(value="classNum") String classNum,@Param(value="courseNum") String courseNum);

	int getTimeTablesCount(@Param(value="classNum") String classNum,@Param(value="courseNum") String courseNum);
}