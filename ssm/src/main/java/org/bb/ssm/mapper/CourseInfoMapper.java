package org.bb.ssm.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.bb.ssm.model.Course;
import org.bb.ssm.model.CourseChapterKnowledge;

public interface CourseInfoMapper {
	
	List<Course> findAll();
	
	List<CourseChapterKnowledge> getAllCourseChapterKnowledge();
	
    int deleteByPrimaryKey(Integer CourseId);

    int deleteByPrimaryKeys(@Param("ids")List<String> ids);
    
    int insert(Course record);

    int insertSelective(Course record);

    Course selectByPrimaryKey(Integer CourseId);

    int updateByPrimaryKeySelective(Course record);

    int updateByPrimaryKey(Course record);

	Course selectByPwd(Course record);

	int getIdByNum(String num);
}