<!DOCTYPE html>
<html lang="utf-8">
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1">
		<meta name="keywords" content="">
		<meta name="description" content="">
		<title>个人主页</title>
		<link href="res/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="res/css/reset.css" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="res/js/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="res/js/jquery.form.js"></script>
		<script type="text/javascript" src="res/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="res/js/oc.min.js"></script>

		<!--[if lt IE 9]>
		  <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<link rel="icon" type="image/png" href="res/i/ico.png" sizes="16x16">

		<link rel="stylesheet" href="css/base.css">
	    <link rel="stylesheet" href="css/header.css">
	    <script src="js/jquery-3.2.1.js"></script>

		<script type="text/javascript">
		CONTEXT_PATH = '${s.base}';

		<@shiro.guest>
		SHIRO_LOGIN = false;
		</@shiro.guest>

		<@shiro.user>
		SHIRO_LOGIN = true;//是否已经登录 
		</@shiro.user>

		$(function(){  
		    // 设置jQuery Ajax全局的参数  
		    $.ajaxSetup({  
		        type: "POST",  
		        complete:function(xhr,status){
		        	if(xhr.responseJSON.errcode == 1001){//登录超时 
		        		window.location.href=CONTEXT_PATH+"/auth/login.html";
		        	}
		        }
		    });  
		});  
		</script>
	</head>

	<body>
		<header id="header">
	      <div class="logo">
	          <img src="image/home/logo.png" alt="">
	          <div class="name">
	              <h3>在线视屏交流平台</h3>
	              <h6>ENDUCATION.COM</h6>
	          </div>
	      </div>
	      <!--logo -->

	      <div class="search">
	          <img src="image/home/sseachleft.png" alt='' class="sleft">
	          <input type="text" placeholder="请输入您想要搜索的课程" class="sinput">
	          <img src="image/home/ssearchright.png" alt='' class="sright">
	      </div>
	      <!--search-->

	      <div class="notice">
	          <div class="nicon">
	          </div>
	          <div class="ncontent">
	              通知
	              <p>
	                   到教学楼查询您的课程
	              </p>
	          </div>
	      </div>
	      <!--notice -->

	      <ul class="nav">
	          <!-- 需要选中状态  加hot-->
	          <li class="list">
	              <ul class="point">
	                  <li></li>
	                  <li></li>
	              </ul>
	              <a href="index.html">网站首页</a>
	          </li>
	          <li class="list hot">
	              <ul class="point">
	                  <li></li>
	                  <li></li>
	              </ul>
	              <a href="">用户中心</a>
	          </li>
	          <li class="list">
	              <ul class="point">
	                  <li></li>
	                  <li></li>
	              </ul>
	              <a href="">提供帮助</a>
	          </li>
	          <li class="list">
	              <ul class="point">
	                  <li></li>
	                  <li></li>
	              </ul>
	              <a href="">后台管理</a>
	          </li>
	      </ul>
	      <!--nav-->
	  </header>
		<div class="f-main clearfix">
			<div class="setting-left">
					
				<img id="userNavHeader" class="setting-header" src='<@shiro.principal property="header"/>'></img>
					<div>大帅哥</div>
				<div class="split-line" style="margin-bottom: 20px;"></div>

				<ul class="user-menu-nav-block">
					<a href="home.html">
						<li class="user-menu-nav-cur">主页 <span>&gt;</span></li>
					</a>
					<a href="course.html">
						<li class="user-menu-nav">我的课程  <span>&gt;</span></li>
					</a>
					<a href="collect.html">
						<li class="user-menu-nav">我的收藏  <span>&gt;</span></li>
					</a>
					<a href="info.html">
						<li class="user-menu-nav">个人信息  <span>&gt;</span></li>
					</a>
					<a href="passwd.html">
						<li class="user-menu-nav">修改密码  <span>&gt;</span></li>
					</a>
					<a href="qa.html">
						<li class="user-menu-nav">答疑  <span>&gt;</span></li>
					</a>
				</ul>
				<script type="text/javascript">
					$(function(){
						$('.user-menu-nav').hover(function(){
							$(this).find('span').css('color','#0089D2');
						},function(){
							$(this).find('span').css('color','#777');
						});
						
						var headPhoto = $('#userNavHeader').attr('src');
						if(headPhoto == null || headPhoto == '' || headPhoto == 'null'){
							var headPhoto = "${s.base}/res/i/header.jpg";
							$('#userNavHeader').attr('src',headPhoto);
						}
					});	
				</script>
			</div>
						
			<div class="setting-right"  >
				<div><span class="f-16">最新动态</span></div>
				<div class="split-line" style="margin: 20px 0px;"></div>
				
				<form id="queryPageForm" action="">
					<div class="comment clearfix">
						<div class="comment-header">
							<img class="lecturer-uimg" src="/ocPortal/res/i/header.jpg">
						</div>
						
						<div class="comment-main" style="width: 840px;">
							<div class="user-name">wangwu</div>
							<div class="comment-content">
								<div>
									<a href="/ocPortal/course/learn/1.html" target="_blank" class="link-a"  style="font-weight:bold;">
									带您完成神秘的涟漪按钮效果-入门篇
									</a>
									</div>
								<div style="margin-top:3px;">
									<a  href="/ocPortal/course/video/9.html"  target="_blank" class="link-a">
									2-2 包括录制到文件、播放文件，可以实现类似于微信的语音消息发送与播放
									</a>
								</div>
							</div>
							<div class="comment-footer">时间：2017-05-04</div>
						</div>
					</div>	
					<div class="comment clearfix">
						<div class="comment-header">
							<img class="lecturer-uimg" src="/ocPortal/res/i/header.jpg">
						</div>
						
						<div class="comment-main" style="width: 840px;">
							<div class="user-name">wangwu</div>
							<div class="comment-content">
								<div>
									<a href="/ocPortal/course/learn/1.html" target="_blank" class="link-a"  style="font-weight:bold;">
									带您完成神秘的涟漪按钮效果-入门篇
									</a>
									</div>
								<div style="margin-top:3px;">
									<a  href="/ocPortal/course/video/21.html"  target="_blank" class="link-a">
									4-4 list排序内建函数、常用指令
									</a>
								</div>
							</div>
							<div class="comment-footer">时间：2017-05-04</div>
						</div>
					</div>	
					<div class="comment clearfix">
						<div class="comment-header">
							<img class="lecturer-uimg" src="/ocPortal/res/i/header.jpg">
						</div>
						
						<div class="comment-main" style="width: 840px;">
							<div class="user-name">zhaoliu</div>
							<div class="comment-content">
								<div>
									<a href="/ocPortal/course/learn/1.html" target="_blank" class="link-a"  style="font-weight:bold;">
									带您完成神秘的涟漪按钮效果-入门篇
									</a>
									</div>
								<div style="margin-top:3px;">
									<a  href="/ocPortal/course/video/14.html"  target="_blank" class="link-a">
									3-3 集合List的遍历
									</a>
								</div>
							</div>
							<div class="comment-footer">时间：2017-05-04</div>
						</div>
					</div>	
					<div class="comment clearfix">
						<div class="comment-header">
							<img class="lecturer-uimg" src="/ocPortal/res/i/header.jpg">
						</div>
						
						<div class="comment-main" style="width: 840px;">
							<div class="user-name">zhaoliu</div>
							<div class="comment-content">
								<div>
									<a href="/ocPortal/course/learn/1.html" target="_blank" class="link-a"  style="font-weight:bold;">
									带您完成神秘的涟漪按钮效果-入门篇
									</a>
									</div>
								<div style="margin-top:3px;">
									<a  href="/ocPortal/course/video/13.html"  target="_blank" class="link-a">
									3-2 自定义对象User变量的取值 
									</a>
								</div>
							</div>
							<div class="comment-footer">时间：2017-05-04</div>
						</div>
					</div>	
					<div class="comment clearfix">
						<div class="comment-header">
							<img class="lecturer-uimg" src="/ocPortal/res/i/header.jpg">
						</div>
						
						<div class="comment-main" style="width: 840px;">
							<div class="user-name">lisi</div>
							<div class="comment-content">
								<div>
									<a href="/ocPortal/course/learn/1.html" target="_blank" class="link-a"  style="font-weight:bold;">
									带您完成神秘的涟漪按钮效果-入门篇
									</a>
									</div>
								<div style="margin-top:3px;">
									<a  href="/ocPortal/course/video/6.html"  target="_blank" class="link-a">
									1-1 使用RecyclerView优雅实现复杂布局-课程介绍
									</a>
								</div>
							</div>
							<div class="comment-footer">时间：2017-05-04</div>
						</div>
					</div>	
					<div class="comment clearfix">
						<div class="comment-header">
							<img class="lecturer-uimg" src="/ocPortal/res/i/header.jpg">
						</div>
						
						<div class="comment-main" style="width: 840px;">
							<div class="user-name">lisi</div>
							<div class="comment-content">
								<div>
									<a href="/ocPortal/course/learn/1.html" target="_blank" class="link-a"  style="font-weight:bold;">
									带您完成神秘的涟漪按钮效果-入门篇
									</a>
									</div>
								<div style="margin-top:3px;">
									<a  href="/ocPortal/course/video/24.html"  target="_blank" class="link-a">
									5-2 类创建
									</a>
								</div>
							</div>
							<div class="comment-footer">时间：2017-05-04</div>
						</div>
					</div>	
<script type="text/javascript">
	function _queryPage(page){
		if(page == undefined){
			page = 1;
		}
		$('#_id_pageNum').val(page);
		
		var query = $('#queryPageForm').attr('queryPage');
		if(query && Number(query) == 1){
			queryPage(page);
		}else{
			$('#queryPageForm').submit();
		}
	}
</script>				</form>
			</div>
		</div>
	  <!-- 底部开始 -->
	<footer>
	  <div class="contain">
		<div class="main">
		  <div class="text">
			<p>版权所有 中北大学软件学院&nbsp;&nbsp;&nbsp;&nbsp;综合科：0351-3924578&nbsp;&nbsp;&nbsp;&nbsp;教学科：0351-3925275&nbsp;&nbsp;&nbsp;&nbsp;学生科、团委：0351-3924613&nbsp;&nbsp;&nbsp;&nbsp;科研与对外合作科：0351-3924595</p>
			<p>邮箱：sti@nuc.edu.cn&nbsp;&nbsp;&nbsp;&nbsp;本站由山西优逸客科技有限公司维护&nbsp;&nbsp;&nbsp;&nbsp;
			<!-- <a href="#" target="_blank">后台管理</a> -->
			</p>
		  </div>
		</div>
	  </div>
	</footer>
	<!-- 底部结束 -->
		
	</body>
</html>
