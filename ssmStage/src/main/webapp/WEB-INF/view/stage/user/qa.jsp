<!DOCTYPE html>
<html lang="utf-8">
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1">
		<meta name="keywords" content="">
		<meta name="description" content="">
		<title>个人主页</title>
		<link href="res/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="res/css/reset.css" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="res/js/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="res/js/jquery.form.js"></script>
		<script type="text/javascript" src="res/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="res/js/oc.min.js"></script>

		<!--[if lt IE 9]>
		  <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<link rel="icon" type="image/png" href="res/i/ico.png" sizes="16x16">

		<link rel="stylesheet" href="css/base.css">
	    <link rel="stylesheet" href="css/header.css">
	    <script src="js/jquery-3.2.1.js"></script>

		<script type="text/javascript">
		CONTEXT_PATH = '${s.base}';

		<@shiro.guest>
		SHIRO_LOGIN = false;
		</@shiro.guest>

		<@shiro.user>
		SHIRO_LOGIN = true;//是否已经登录 
		</@shiro.user>

		$(function(){  
		    // 设置jQuery Ajax全局的参数  
		    $.ajaxSetup({  
		        type: "POST",  
		        complete:function(xhr,status){
		        	if(xhr.responseJSON.errcode == 1001){//登录超时 
		        		window.location.href=CONTEXT_PATH+"/auth/login.html";
		        	}
		        }
		    });  
		});  
		</script>
	</head>

	<body>
		<header id="header">
	      <div class="logo">
	          <img src="image/home/logo.png" alt="">
	          <div class="name">
	              <h3>在线视屏交流平台</h3>
	              <h6>ENDUCATION.COM</h6>
	          </div>
	      </div>
	      <!--logo -->

	      <div class="search">
	          <img src="image/home/sseachleft.png" alt='' class="sleft">
	          <input type="text" placeholder="请输入您想要搜索的课程" class="sinput">
	          <img src="image/home/ssearchright.png" alt='' class="sright">
	      </div>
	      <!--search-->

	      <div class="notice">
	          <div class="nicon">
	          </div>
	          <div class="ncontent">
	              通知
	              <p>
	                   到教学楼查询您的课程
	              </p>
	          </div>
	      </div>
	      <!--notice -->

	      <ul class="nav">
	          <!-- 需要选中状态  加hot-->
	          <li class="list">
	              <ul class="point">
	                  <li></li>
	                  <li></li>
	              </ul>
	              <a href="index.html">网站首页</a>
	          </li>
	          <li class="list hot">
	              <ul class="point">
	                  <li></li>
	                  <li></li>
	              </ul>
	              <a href="">用户中心</a>
	          </li>
	          <li class="list">
	              <ul class="point">
	                  <li></li>
	                  <li></li>
	              </ul>
	              <a href="">提供帮助</a>
	          </li>
	          <li class="list">
	              <ul class="point">
	                  <li></li>
	                  <li></li>
	              </ul>
	              <a href="">后台管理</a>
	          </li>
	      </ul>
	      <!--nav-->
	  </header>
		<div class="f-main clearfix">
			<div class="setting-left">
					
				<img id="userNavHeader" class="setting-header" src='<@shiro.principal property="header"/>'></img>
					<div>大帅哥</div>
				<div class="split-line" style="margin-bottom: 20px;"></div>

				<ul class="user-menu-nav-block">
					<a href="home.html">
						<li class="user-menu-nav">主页 <span>&gt;</span></li>
					</a>
					<a href="course.html">
						<li class="user-menu-nav">我的课程  <span>&gt;</span></li>
					</a>
					<a href="collect.html">
						<li class="user-menu-nav">我的收藏  <span>&gt;</span></li>
					</a>
					<a href="info.html">
						<li class="user-menu-nav">个人信息  <span>&gt;</span></li>
					</a>
					<a href="passwd.html">
						<li class="user-menu-nav">修改密码  <span>&gt;</span></li>
					</a>
					<a href="qa.html">
						<li class="user-menu-nav-cur">答疑  <span>&gt;</span></li>
					</a>
				</ul>
				<script type="text/javascript">
					$(function(){
						$('.user-menu-nav').hover(function(){
							$(this).find('span').css('color','#0089D2');
						},function(){
							$(this).find('span').css('color','#777');
						});
						
						var headPhoto = $('#userNavHeader').attr('src');
						if(headPhoto == null || headPhoto == '' || headPhoto == 'null'){
							var headPhoto = "${s.base}/res/i/header.jpg";
							$('#userNavHeader').attr('src',headPhoto);
						}
					});	
				</script>
			</div>
						
			<div class="setting-right"  >
				<div><span class="f-16">我的课程QA</span></div>
				<div class="split-line" style="margin: 20px 0px;"></div>
				
				<form id="queryPageForm" action="">
				<div class="comment clearfix">
					<div class="comment-main" style="width: 100%">
						<div style="font-weight: bold;">
							<a href="/ocPortal/course/learn/1.html" target="_blank">
							带您完成神秘的涟漪按钮效果-入门篇
							</a>
						</div>
						<div style="margin-top:5px;padding-left:20px;">3-1 变量的定义、赋值、运算 </div>
						
						<div class="comment-content" style="padding-left:20px;">
							<div style="margin-bottom:5px;">
								<span style="font-weight:bold">wangyangming 评论道： </span>
							</div>
							
							<div style="padding: 5px;border:1px solid #ccc;background-color:#eee;">
								我是新的评论我是新的评论我是新的评论
							</div>
							
							<div style="margin-top:5px;">
								我是新的回答我是新的回答
							</div>
						</div>
						<div class="comment-footer">
							<span>时间：
							2017-05-06
							</span>
							<a href="#qaModal" data-toggle="modal" onclick="doQA(59)"  style="color:#0089D2;">回 答</a>
						</div>
					</div>
				</div>		
				<div class="comment clearfix">
					<div class="comment-main" style="width: 100%">
						<div style="font-weight: bold;">
							<a href="/ocPortal/course/learn/1.html" target="_blank">
							带您完成神秘的涟漪按钮效果-入门篇
							</a>
						</div>
						<div style="margin-top:5px;padding-left:20px;">3-1 变量的定义、赋值、运算 </div>
						
						<div class="comment-content" style="padding-left:20px;">
							<div style="margin-bottom:5px;">
								<span style="font-weight:bold">wangyangming 评论道： </span>
							</div>
							
							
							<div style="margin-top:5px;">
								我是新的评论我是新的评论我是新的评论
							</div>
						</div>
						<div class="comment-footer">
							<span>时间：
							2017-05-06
							</span>
							<a href="#qaModal" data-toggle="modal" onclick="doQA(58)"  style="color:#0089D2;">回 答</a>
						</div>
					</div>
				</div>		
				<div class="comment clearfix">
					<div class="comment-main" style="width: 100%">
						<div style="font-weight: bold;">
							<a href="/ocPortal/course/learn/1.html" target="_blank">
							带您完成神秘的涟漪按钮效果-入门篇
							</a>
						</div>
						<div style="margin-top:5px;padding-left:20px;">3-1 变量的定义、赋值、运算 </div>
						
						<div class="comment-content" style="padding-left:20px;">
							<div style="margin-bottom:5px;">
								<span style="font-weight:bold">wangyangming 评论道： </span>
							</div>
							
							
							<div style="margin-top:5px;">
								fdafdsaf
							</div>
						</div>
						<div class="comment-footer">
							<span>时间：
							2017-05-06
							</span>
							<a href="#qaModal" data-toggle="modal" onclick="doQA(57)"  style="color:#0089D2;">回 答</a>
						</div>
					</div>
				</div>		
				<div class="comment clearfix">
					<div class="comment-main" style="width: 100%">
						<div style="font-weight: bold;">
							<a href="/ocPortal/course/learn/1.html" target="_blank">
							带您完成神秘的涟漪按钮效果-入门篇
							</a>
						</div>
						<div style="margin-top:5px;padding-left:20px;">2-2 包括录制到文件、播放文件，可以实现类似于微信的语音消息发送与播放</div>
						
						<div class="comment-content" style="padding-left:20px;">
							<div style="margin-bottom:5px;">
								<span style="font-weight:bold">wangyangming 评论道： </span>
							</div>
							
							<div style="padding: 5px;border:1px solid #ccc;background-color:#eee;">
								我回答你我回答你我的回答你
							</div>
							
							<div style="margin-top:5px;">
								我我我我我我我我我我我我我我我
							</div>
						</div>
						<div class="comment-footer">
							<span>时间：
							2017-05-06
							</span>
							<a href="#qaModal" data-toggle="modal" onclick="doQA(56)"  style="color:#0089D2;">回 答</a>
						</div>
					</div>
				</div>		
				<div class="comment clearfix">
					<div class="comment-main" style="width: 100%">
						<div style="font-weight: bold;">
							<a href="/ocPortal/course/learn/1.html" target="_blank">
							带您完成神秘的涟漪按钮效果-入门篇
							</a>
						</div>
						<div style="margin-top:5px;padding-left:20px;">2-2 包括录制到文件、播放文件，可以实现类似于微信的语音消息发送与播放</div>
						
						<div class="comment-content" style="padding-left:20px;">
							<div style="margin-bottom:5px;">
								<span style="font-weight:bold">wangyangming 评论道： </span>
							</div>
							
							<div style="padding: 5px;border:1px solid #ccc;background-color:#eee;">
								fdsafdsaf
							</div>
							
							<div style="margin-top:5px;">
								我回答你我回答你我的回答你
							</div>
						</div>
						<div class="comment-footer">
							<span>时间：
							2017-05-06
							</span>
							<a href="#qaModal" data-toggle="modal" onclick="doQA(55)"  style="color:#0089D2;">回 答</a>
						</div>
					</div>
				</div>		
				<div class="comment clearfix">
					<div class="comment-main" style="width: 100%">
						<div style="font-weight: bold;">
							<a href="/ocPortal/course/learn/1.html" target="_blank">
							带您完成神秘的涟漪按钮效果-入门篇
							</a>
						</div>
						<div style="margin-top:5px;padding-left:20px;">2-2 包括录制到文件、播放文件，可以实现类似于微信的语音消息发送与播放</div>
						
						<div class="comment-content" style="padding-left:20px;">
							<div style="margin-bottom:5px;">
								<span style="font-weight:bold">wangyangming 评论道： </span>
							</div>
							
							<div style="padding: 5px;border:1px solid #ccc;background-color:#eee;">
								fdsafdsaf
							</div>
							
							<div style="margin-top:5px;">
								我是回答我是回答
							</div>
						</div>
						<div class="comment-footer">
							<span>时间：
							2017-05-06
							</span>
							<a href="#qaModal" data-toggle="modal" onclick="doQA(54)"  style="color:#0089D2;">回 答</a>
						</div>
					</div>
				</div>		
				<div class="comment clearfix">
					<div class="comment-main" style="width: 100%">
						<div style="font-weight: bold;">
							<a href="/ocPortal/course/learn/1.html" target="_blank">
							带您完成神秘的涟漪按钮效果-入门篇
							</a>
						</div>
						<div style="margin-top:5px;padding-left:20px;">2-2 包括录制到文件、播放文件，可以实现类似于微信的语音消息发送与播放</div>
						
						<div class="comment-content" style="padding-left:20px;">
							<div style="margin-bottom:5px;">
								<span style="font-weight:bold">wangyangming 评论道： </span>
							</div>
							
							<div style="padding: 5px;border:1px solid #ccc;background-color:#eee;">
								我是评论我是评论
							</div>
							
							<div style="margin-top:5px;">
								fdsafdsaf
							</div>
						</div>
						<div class="comment-footer">
							<span>时间：
							2017-05-04
							</span>
							<a href="#qaModal" data-toggle="modal" onclick="doQA(53)"  style="color:#0089D2;">回 答</a>
						</div>
					</div>
				</div>		
				<div class="comment clearfix">
					<div class="comment-main" style="width: 100%">
						<div style="font-weight: bold;">
							<a href="/ocPortal/course/learn/1.html" target="_blank">
							带您完成神秘的涟漪按钮效果-入门篇
							</a>
						</div>
						<div style="margin-top:5px;padding-left:20px;">2-2 包括录制到文件、播放文件，可以实现类似于微信的语音消息发送与播放</div>
						
						<div class="comment-content" style="padding-left:20px;">
							<div style="margin-bottom:5px;">
								<span style="font-weight:bold">wangyangming 评论道： </span>
							</div>
							
							<div style="padding: 5px;border:1px solid #ccc;background-color:#eee;">
								我是评论我是评论
							</div>
							
							<div style="margin-top:5px;">
								fdsafadsfdsaf
							</div>
						</div>
						<div class="comment-footer">
							<span>时间：
							2017-05-04
							</span>
							<a href="#qaModal" data-toggle="modal" onclick="doQA(52)"  style="color:#0089D2;">回 答</a>
						</div>
					</div>
				</div>		
				<div class="comment clearfix">
					<div class="comment-main" style="width: 100%">
						<div style="font-weight: bold;">
							<a href="/ocPortal/course/learn/1.html" target="_blank">
							带您完成神秘的涟漪按钮效果-入门篇
							</a>
						</div>
						<div style="margin-top:5px;padding-left:20px;">1-1 使用RecyclerView优雅实现复杂布局-课程介绍</div>
						
						<div class="comment-content" style="padding-left:20px;">
							<div style="margin-bottom:5px;">
								<span style="font-weight:bold">wangyangming 评论道： </span>
							</div>
							
							
							<div style="margin-top:5px;">
								我是评论
							</div>
						</div>
						<div class="comment-footer">
							<span>时间：
							2017-05-04
							</span>
							<a href="#qaModal" data-toggle="modal" onclick="doQA(51)"  style="color:#0089D2;">回 答</a>
						</div>
					</div>
				</div>		
				<div class="comment clearfix">
					<div class="comment-main" style="width: 100%">
						<div style="font-weight: bold;">
							<a href="/ocPortal/course/learn/1.html" target="_blank">
							带您完成神秘的涟漪按钮效果-入门篇
							</a>
						</div>
						<div style="margin-top:5px;padding-left:20px;">3-1 变量的定义、赋值、运算 </div>
						
						<div class="comment-content" style="padding-left:20px;">
							<div style="margin-bottom:5px;">
								<span style="font-weight:bold">wangyangming 评论道： </span>
							</div>
							
							<div style="padding: 5px;border:1px solid #ccc;background-color:#eee;">
								我是评论我是评论
							</div>
							
							<div style="margin-top:5px;">
								我是问答我是问答
							</div>
						</div>
						<div class="comment-footer">
							<span>时间：
							2017-05-04
							</span>
							<a href="#qaModal" data-toggle="modal" onclick="doQA(50)"  style="color:#0089D2;">回 答</a>
						</div>
					</div>
				</div>		
				
<div class="page-box clearfix">
	<div class="page clearfix">
		<div style="float:left;">
			<input type="hidden" id="_id_pageNum" name="pageNum" value="1"/>
				<a class="page-cur" href="javascript:void(0);" >1</a>
				<a class="page-num" href="javascript:void(0);" onclick="_queryPage('2');">2</a>
				<a class="page-num" href="javascript:void(0);" onclick="_queryPage('3');">3</a>
				<a class="page-num" href="javascript:void(0);" onclick="_queryPage('4');">4</a>
				<a class="page-num" href="javascript:void(0);" onclick="_queryPage('5');">5</a>
				<a class="page-num" href="javascript:void(0);" onclick="_queryPage('6');">6</a>
			<a class="page-next" href="javascript:void(0);"  onclick="_queryPage('2')">下一页</a> 
			<a class="page-next" href="javascript:void(0);"  onclick="_queryPage('6')">尾 页</a> 
		</div>
	</div>
</div>
<script type="text/javascript">
	function _queryPage(page){
		if(page == undefined){
			page = 1;
		}
		$('#_id_pageNum').val(page);
		
		var query = $('#queryPageForm').attr('queryPage');
		if(query && Number(query) == 1){
			queryPage(page);
		}else{
			$('#queryPageForm').submit();
		}
	}
</script>				</form>
			</div>
		</div>
	</div>
		<footer>
		  <div class="contain">
		    <img src="image/footer/footer_06.png" alt="">
		  </div>
		</footer>
		
	</body>
</html>
