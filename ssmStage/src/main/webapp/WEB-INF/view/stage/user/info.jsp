<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/base.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/header.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/more.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/footer.css">
	<script src="${pageContext.request.contextPath }/stageRescourse/js/jquery-3.2.1.js"></script>
	<script src="${pageContext.request.contextPath }/stageRescourse/js/more.js"></script>
	
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/wq-index1.css">
</head>
<body>
<header id="header">
      <div class="logo">
		  <a href="${pageContext.request.contextPath }/login/main">
			<img src="${pageContext.request.contextPath }/stageRescourse/image/home/logo3.png" alt="">
		  </a>
          <div class="name">
              <h3>中北在线交流平台</h3>
              <h6>中北在线</h6>
          </div>
      </div>
      <!--logo -->

      <div class="search">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/sseachleft.png" alt='' class="sleft">
          <input type="text" placeholder="请输入搜索的课程" class="sinput">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/ssearchright.png" alt='' class="sright">
      </div>
      <!--search-->

      <!--notice -->

      <ul class="nav">
          <!-- 需要选中状态  加hot-->
          <li class="list hot">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/login/main">网站首页</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
			  	<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">用户中心</a>
				</c:if>
				<c:if test="${userinfo != null }">
					 <a href="${pageContext.request.contextPath }/personCenter/info">用户中心</a>
				</c:if>
          </li>
		  <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="">后台管理</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
				<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">&nbsp;&nbsp;&nbsp;&nbsp;登录</a>
				</c:if>
				<c:if test="${userinfo != null }">
					<a href="${pageContext.request.contextPath }/login/loginout">&nbsp;&nbsp;&nbsp;&nbsp;退出</a>
				</c:if>
          </li>
          
      </ul>
      <!--nav-->
    </header>
	
<div id="wq-main">
    <div class="wq-leftside">
        <div class="wq-headpic">
            <img src="${pageContext.request.contextPath }/stageRescourse/images/wq-header.jpg" alt="">
        </div>
        <div class="wq-username">大帅哥</div>
        <div class="wq-typewrapper">
            <div class="wq-bars active">个人信息 <span>&gt;</span></div>
            <div class="wq-bars"><a href="password">修改密码</a> <span>&gt;</span></div>
        </div>
    </div>
    <div class="wq-rightside">
        <div class="wq-title">
            <p class="wq-zhcn">带你打开互联网全新时代</p>
            <p class="wq-en">Take you to open the new age
                of the Internet</p>
            <div class="wq-line"></div>
        </div>
        <div class="wq-dashline"></div>
        <form action="" id="wq-myform">
            <label>
                <span>学号</span>
                <input type="text" value="${userinfo.username }" readonly>
            </label>
            <label>
                <span>姓名</span>
                <input type="text" value="${userinfo.sname }" readonly >
            </label>
            <label>
                <span>学校</span>
                <input type="text" value="中北大学" readonly>
            </label>
            <label>
                <span>学历</span>
                <input type="text" value="本科" readonly>
            </label>
            <label>
                <span>个性签名</span>
                <input type="text">
            </label>
        </form>
        <div class="wq-bottomline">
            <div class="wq-inner"></div>
        </div>
        <div class="wq-save">确定</div>
    </div>
</div>

<!-- 底部开始 -->
    <footer>
      <div class="contain">
        <img src="${pageContext.request.contextPath }/stageRescourse/image/footer/footer_06.png" alt="">
      </div>
    </footer>
<!-- 底部结束 -->

<script src="${pageContext.request.contextPath }/stageRescourse/js/jquery-3.2.1.js"></script>
<script src="${pageContext.request.contextPath }/stageRescourse/js/wq-index1.js"></script>
</body>
</html>