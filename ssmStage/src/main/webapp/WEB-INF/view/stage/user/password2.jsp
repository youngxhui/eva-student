<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>个人中心-修改密码</title>
    <!-- 这是原来页面的css和js -->
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/base.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/index.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/header.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/swiper.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/spider.css">
    
    <script src="${pageContext.request.contextPath }/stageRescourse/js/jquery-3.2.1.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/swiper.min.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/index.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/spider.js"></script>
    
    <!-- 这里是个人中心整合过来的css 和 js -->
    <link href="${pageContext.request.contextPath }/stageRescourse/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="${pageContext.request.contextPath }/stageRescourse/css/reset.css" rel="stylesheet" type="text/css"/>
	
	<script type="text/javascript" src="${pageContext.request.contextPath }/stageRescourse/js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/stageRescourse/js/jquery.form.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/stageRescourse/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/stageRescourse/js/oc.min.js"></script>

	<!--[if lt IE 9]>
	  <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<link rel="icon" type="image/png" href="${pageContext.request.contextPath }/stageRescourse/images/personCenter/ico.png" sizes="16x16">
	    
</head>
<body>
    <header id="header">
      <div class="logo">
		  <a href="${pageContext.request.contextPath }/login/main">
			<img src="${pageContext.request.contextPath }/stageRescourse/image/home/logo3.png" alt="">
		  </a>
          <div class="name">
              <h3>中北在线交流平台</h3>
              <h6>中北在线</h6>
          </div>
      </div>
      <!--logo -->

      <div class="search">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/sseachleft.png" alt='' class="sleft">
          <input type="text" placeholder="请输入搜索的课程" class="sinput">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/ssearchright.png" alt='' class="sright">
      </div>
      <!--search-->

      <!--notice -->

      <ul class="nav">
          <!-- 需要选中状态  加hot-->
          <li class="list hot">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/login/main">网站首页</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              	<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">用户中心</a>
				</c:if>
				<c:if test="${userinfo != null }">
					 <a href="${pageContext.request.contextPath }/personCenter/info">用户中心</a>
				</c:if>
				
          </li>
		  <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="">后台管理</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
				<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">&nbsp;&nbsp;&nbsp;&nbsp;登录</a>
				</c:if>
				<c:if test="${userinfo != null }">
					<a href="${pageContext.request.contextPath }/login/loginout">&nbsp;&nbsp;&nbsp;&nbsp;退出</a>
				</c:if>
          </li>
          
      </ul>
      <!--nav-->
    </header>
    
    <div class="f-main clearfix">
			<div class="setting-left">
					
				<img id="userNavHeader" class="setting-header" src='<@shiro.principal property="header"/>'></img>
					<div>大帅哥</div>
				<div class="split-line" style="margin-bottom: 20px;"></div>

				<ul class="user-menu-nav-block">
					<a href="info">
						<li class="user-menu-nav">个人信息  <span>&gt;</span></li>
					</a>
					<!--<a href="course">
						<li class="user-menu-nav">我的课程  <span>&gt;</span></li>
					</a>
					<a href="collect">
						<li class="user-menu-nav">我的收藏  <span>&gt;</span></li>
					</a>
					-->
					<a href="password">
						<li class="user-menu-nav-cur">修改密码  <span>&gt;</span></li>
					</a>
				</ul>
				<script type="text/javascript">
					$(function(){
						$('.user-menu-nav').hover(function(){
							$(this).find('span').css('color','#0089D2');
						},function(){
							$(this).find('span').css('color','#777');
						});
						
						var headPhoto = $('#userNavHeader').attr('src');
						if(headPhoto == null || headPhoto == '' || headPhoto == 'null'){
							var headPhoto = "${s.base}/res/i/header.jpg";
							$('#userNavHeader').attr('src',headPhoto);
						}
					});	
				</script>
			</div>
						
			<div class="setting-right"  >
				
				<div class="split-line" style="margin: 20px 0px;"></div>
				
				<form class="oc-form" id="passwdForm" method="post" action="${pageContext.request.contextPath }/personCenter/updatePassword">
					<input type="hidden" name="id" value="${userinfo.id }" >
					<input type="hidden" name="username" value="${userinfo.username }">
					<li><label>新密码</label> <input name="password" type="password" class="input-text"  placeholder="请输入新密码" autocomplete="off" /></li>
					<li><label>确认密码</label> <input name="rePassword" type="password" class="input-text"  placeholder="请输入确认密码" autocomplete="off" /></li>
					
					<li class="clearfix" style="margin-top: 50px;padding-left: 170px;">
						<input type="submit" value="确实修改">
					</li>
					
					<li>
						<div id="infoAlert" class="alert alert-success" style="display: none;">
							<span id="infoAlert_msg" class="color-oc f-16"></span>
						</div>
					</li>
				</form>
			</div>
		</div>
	</div>

   
	 <!-- 底部开始 -->
    <footer>
      <div class="contain">
        <img src="${pageContext.request.contextPath }/stageRescourse/image/footer/footer_06.png" alt="">
      </div>
    </footer>
    <!-- 底部结束 -->
	
</body>
</html>