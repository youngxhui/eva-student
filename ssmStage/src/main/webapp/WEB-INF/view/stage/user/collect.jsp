<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>个人中心-修改密码</title>
    <!-- 这是原来页面的css和js -->
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/base.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/index.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/header.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/swiper.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/spider.css">
    
    <script src="${pageContext.request.contextPath }/stageRescourse/js/jquery-3.2.1.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/swiper.min.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/index.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/spider.js"></script>
    
    <!-- 这里是个人中心整合过来的css 和 js -->
    <link href="${pageContext.request.contextPath }/stageRescourse/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="${pageContext.request.contextPath }/stageRescourse/css/reset.css" rel="stylesheet" type="text/css"/>
	
	<script type="text/javascript" src="${pageContext.request.contextPath }/stageRescourse/js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/stageRescourse/js/jquery.form.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/stageRescourse/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/stageRescourse/js/oc.min.js"></script>

	<!--[if lt IE 9]>
	  <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<link rel="icon" type="image/png" href="${pageContext.request.contextPath }/stageRescourse/images/personCenter/ico.png" sizes="16x16">
	    
</head>
<body>
    <header id="header">
      <div class="logo">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/logo.png" alt="">
          <div class="name">
              <h3><a href="${pageContext.request.contextPath }/login/main">中北在线交流平台</a></h3>
              <h6>软件学院</h6>
          </div>
      </div>

      <ul class="nav">
          <!-- 需要选中状态  加hot-->
          <li class="list hot">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/ssmStage/login/main">网站首页</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/personCenter/info">用户中心</a>
          </li>
      </ul>
      <!--nav-->
	  
	   <div class="notice">
          <div class="nicon">
          </div>
         <!--  <div class="ncontent">
              通知
              <p>
                   到教学楼查询您的课程
              </p>
          </div> -->
          <div class="login">
          	<c:if test="${userinfo == null }">
	          	<a href="${pageContext.request.contextPath }/login/login">&nbsp;&nbsp;&nbsp;&nbsp;登录</a>
          	</c:if>
          	<c:if test="${userinfo != null }">
          		<a href="${pageContext.request.contextPath }/login/loginout">&nbsp;&nbsp;&nbsp;&nbsp;退出</a>
          	</c:if>
          </div>
      </div>
    </header>
    
    <div class="f-main clearfix">
			<div class="setting-left">
					
				<img id="userNavHeader" class="setting-header" src='<@shiro.principal property="header"/>'></img>
					<div>大帅哥</div>
				<div class="split-line" style="margin-bottom: 20px;"></div>

				<ul class="user-menu-nav-block">
					<a href="info">
						<li class="user-menu-nav">个人信息  <span>&gt;</span></li>
					</a>
					<a href="course">
						<li class="user-menu-nav">我的课程  <span>&gt;</span></li>
					</a>
					<a href="collect">
						<li class="user-menu-nav-cur">我的收藏  <span>&gt;</span></li>
					</a>
					<a href="password">
						<li class="user-menu-nav">修改密码  <span>&gt;</span></li>
					</a>
				</ul>
				<script type="text/javascript">
					$(function(){
						$('.user-menu-nav').hover(function(){
							$(this).find('span').css('color','#0089D2');
						},function(){
							$(this).find('span').css('color','#777');
						});
						
						var headPhoto = $('#userNavHeader').attr('src');
						if(headPhoto == null || headPhoto == '' || headPhoto == 'null'){
							var headPhoto = "${s.base}/res/i/header.jpg";
							$('#userNavHeader').attr('src',headPhoto);
						}
					});	
				</script>
			</div>
						
			<div class="setting-right"  >
				
				<div class="split-line" style="margin: 20px 0px;"></div>
				
				<form id="queryPageForm" action="">
					<div class="comment clearfix">
						<div class="comment-main" style="width: 100%">
							<a href="/ocPortal/course/learn/20.html" target="_blank" class="user-name link-a" style="font-size:20px;">
								面向对象程序设计
							</a>
							<div class="comment-content">
								<span class="learn-rate">分类：
										课程
								</span>
								<span>收藏时间：
								2018-03-15
								</span>
							</div>
						</div>
					</div>	
				</form>
			</div>
		</div>
	</div>

   
    <!-- 底部开始 -->
   	  <!-- 底部开始 -->
	<footer>
	  <div class="contain">
		<div class="main">
		  <div class="text">
			<p>版权所有 中北大学软件学院&nbsp;&nbsp;&nbsp;&nbsp;综合科：0351-3924578&nbsp;&nbsp;&nbsp;&nbsp;教学科：0351-3925275&nbsp;&nbsp;&nbsp;&nbsp;学生科、团委：0351-3924613&nbsp;&nbsp;&nbsp;&nbsp;科研与对外合作科：0351-3924595</p>
			<p>邮箱：sti@nuc.edu.cn&nbsp;&nbsp;&nbsp;&nbsp;本站由山西优逸客科技有限公司维护&nbsp;&nbsp;&nbsp;&nbsp;
			<!-- <a href="#" target="_blank">后台管理</a> -->
			</p>
		  </div>
		</div>
	  </div>
	</footer>
	<!-- 底部结束 -->
    <!-- 底部结束 -->
</body>
</html>