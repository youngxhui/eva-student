<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>中北大学软件学院在线教育平台-首页</title>
	
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/base.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/index.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/header.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/more.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/swiper.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/spider.css">
    
    <script src="${pageContext.request.contextPath }/stageRescourse/js/jquery-3.2.1.js"></script>
	<script src="${pageContext.request.contextPath }/stageRescourse/js/more.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/swiper.min.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/index.js"></script>


     <style type="text/css">

#banner { position: relative; width: 860px; height: 356px; border: 1px solid #666; overflow: hidden; }

#banner_list img { border: 0px; }

#banner_bg { position: absolute; bottom: 0; background-color: #000; height: 30px; filter: Alpha(Opacity=30); opacity: 0.3; z-index: 1000; cursor: pointer; width: 478px; }

#banner_info { position: absolute; bottom: 0; left: 5px; height: 22px; color: #fff; z-index: 1001; cursor: pointer }

#banner_text { position: absolute; width: 120px; z-index: 1002; right: 3px; bottom: 3px; }

#banner ul { position: absolute; list-style-type: none; filter: Alpha(Opacity=80); opacity: 0.8; z-index: 1002; margin: 0; padding: 0; bottom: 3px; right: 5px; }

#banner ul li { padding: 0px 8px; float: left; display: block; color: #FFF; background: #6f4f67; cursor: pointer; border: 1px solid #333; }

#banner ul li.on { background-color: #000; }

#banner_list a { position: absolute; width:100%;height:100%;}

<!--

让四张图片都可以重叠在一起-->

</style>



<script type="text/javascript">

var t = n =0, count;

$(document).ready(function(){ 

count=$("#banner_list a").length;

$("#banner_list a:not(:first-child)").hide();

$("#banner_info").html($("#banner_list a:first-child").find("img").attr('alt'));

$("#banner_info").click(function(){window.open($("#banner_list a:first-child").attr('href'), "_blank")});

$("#banner li").click(function() {

var i = $(this).text() -1;//获取Li元素内的值，即1，2，3，4

n = i;

if (i >= count) return;

$("#banner_info").html($("#banner_list a").eq(i).find("img").attr('alt'));

$("#banner_info").unbind().click(function(){window.open($("#banner_list a").eq(i).attr('href'), "_blank")})

$("#banner_list a").filter(":visible").fadeOut(500).parent().children().eq(i).fadeIn(1000);

document.getElementById("banner").style.background="";

$(this).toggleClass("on");

$(this).siblings().removeAttr("class");

});

t = setInterval("showAuto()", 4000);

$("#banner").hover(function(){clearInterval(t)}, function(){t = setInterval("showAuto()", 4000);});

})



function showAuto()

{

n = n >=(count -1) ?0 : ++n;

$("#banner li").eq(n).trigger('click');

}

</script>

</head>
<body>
    <header id="header">
      <div class="logo">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/logo3.png" alt="">
          <div class="name">
              <h3><a href="${pageContext.request.contextPath }/login/main">中北在线交流平台</a></h3>
              <h6>软件学院</h6>
          </div>
      </div>

      <ul class="nav">
          <!-- 需要选中状态  加hot-->
          <li class="list hot">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/ssmStage/login/main">网站首页</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/personCenter/info">用户中心</a>
          </li>
      </ul>
      <!--nav-->
	  
	   <div class="notice">
          <div class="nicon">
          </div>
         <!--  <div class="ncontent">
              通知
              <p>
                   到教学楼查询您的课程
              </p>
          </div> -->
          <div class="login">
          	<c:if test="${userinfo == null }">
	          	<a href="${pageContext.request.contextPath }/login/login">&nbsp;&nbsp;&nbsp;&nbsp;登录</a>
          	</c:if>
          	<c:if test="${userinfo != null }">
          		<a href="${pageContext.request.contextPath }/login/loginout">&nbsp;&nbsp;&nbsp;&nbsp;退出</a>
          	</c:if>
          </div>
      </div>
    </header>
    <!-- banner开始 -->
    <div id="index_banner">
        <div class="index_banner_img">
            <div id="banner">

   <!--  <div id="banner_bg"></div> -->
  
    <!--标题背景-->
  
    <!-- <div id="banner_info"></div> -->
  
    <!--标题-->
  
    <ul>
  
      <li class="on">1</li>
  
      <li>2</li>
      <li>3</li>
    </ul>
  
    <div id="banner_list"> 
        <a href="#" target="_blank"><img src="${pageContext.request.contextPath }/stageRescourse/image/banner/lunbo1.jpg" title="" alt=""/></a>
        <a href="#" target="_blank"><img src="${pageContext.request.contextPath }/stageRescourse/image/banner/ynlyxy.jpg" title="" alt=""/></a>
        <a href="#" target="_blank"><img src="${pageContext.request.contextPath }/stageRescourse/image/banner/6.jpg" title="" alt=""/></a>
  
   </div>
  
  </div>
        </div>
        <!-- 个人信息开始 -->
        <div class="index_banner_personal">
            <div class="index_banner_box">
                <a href="${pageContext.request.contextPath }/personCenter/info" class="index_personal_img">
                    <img src="${pageContext.request.contextPath }/stageRescourse/image/home/personal.png" alt="">
                </a>
            </div>
            <h1 class="index_personal_name">${userinfo.username }</h1>
            <h2 class="index_personal_account">${userinfo.sname }</h2>
            <ul class="index_personal_skill">
                <li>
                    <div class="index_banner_skills">
                        <canvas id='canvas' width="67" height="67">
                            <p>You browser not support canvas</p>
                        </canvas>
                    </div>
                    <span>知识点</span>
                </li>
                <li>
                    <div class="index_banner_skills">
                        <canvas id='canvas1' width="67" height="67">
                            <p>You browser not support canvas</p>
                        </canvas>
                    </div>
                    <span>技能</span>
                </li>
                <li>
                    <div class="index_banner_skills">
                        <canvas id='canvas2' width="67" height="67">
                            <p>You browser not support canvas</p>
                        </canvas>
                    </div>
                    <span>能力</span>
                </li>
            </ul>
        </div>
        <!-- 个人信息结束 -->
    </div>  
    <!-- banner结束 -->

    <!-- 推荐课程开始 -->
    <div id="index_recommend">
        <div class="index_module_logo">
            <img src="${pageContext.request.contextPath }/stageRescourse/image/home/m1.png" alt="">
        </div>
        <div class="index_module_more">
            <a href="${pageContext.request.contextPath }/course/index">
                <span>更多</span>
                <i>&#xe6f5;</i>
            </a>
        </div>
        <section id="more_course">
        <!-- 课程介绍开始 -->
        <ul class="more_courseBox">
			<c:forEach items="${requestScope.courses }" var="course" begin="0" end="3">
				
				<li style="display:block" id="hiddenmodel">
					<a href="${pageContext.request.contextPath }/course/video/${course.id}/knowledgeId/0" class="more_imgBox">
					   <img src="${pageContext.request.contextPath }/stageRescourse/image/course/${course.cover_image }" alt=""  width="300px" heigth="300px">
					   <div class="more_switch"></div>
					</a>
					<div class="more_title">
						<h2>${course.name }</h2>
						<h3>${course.name }</h3>
						<h4><a href="${pageContext.request.contextPath }/course/relevantDocument/courseId/${course.id }/pid/0">相关资料</a></h4>
					</div>
					<div class="more_watch">
						<a href="${pageContext.request.contextPath }/course/video/${course.id}/knowledgeId/0">免费观看</a>
						<!-- <h4>已有134人观看</h4> -->
					</div>
				</li>
				
			</c:forEach>
			
		</ul>
        <!-- <ul class="index_module_course">
            <li>
                <div class="index_module_img">
                    <img src="${pageContext.request.contextPath }/stageRescourse/image/home/c1.png" alt="">
                </div>
                <h1 class="index_module_title">面向对象程序设计</h1>
                <p class="index_module_content">面向对象程序设计(Object Oriented Programming)作为一种新方法，其本质是以建立模型体现出来的抽象思维过程和面向对象的方法。</p>
                <div class="index_module_fenge"></div>
                <p class="index_module_time">学习时间：<span>7个月</span></p><div class="index_module_btn">
                    <button class="index_module_details">课程详情</button>
                    <button class="index_module_salary">就业薪资</button>
                </div>   
            </li>
            <li>
                <div class="index_module_img">
                    <img src="${pageContext.request.contextPath }/stageRescourse/image/home/c2.png" alt="">
                </div>
                <h1 class="index_module_title">前端与移动开发</h1>
                <p class="index_module_content">本课程是基于websocket的火拼俄罗斯的升级版本，课程中在前两个课的基础上实现了用两个浏览器对战模式，完整的实现了游戏</p>
                <div class="index_module_fenge"></div>
                <p class="index_module_time">学习时间：<span>7个月</span></p><div class="index_module_btn">
                    <button class="index_module_details">课程详情</button>
                    <button class="index_module_salary">就业薪资</button>
                </div>   
            </li>
            <li>
                <div class="index_module_img">
                    <img src="${pageContext.request.contextPath }/stageRescourse/image/home/c3.png" alt="">
                </div>
                <h1 class="index_module_title">前端与移动开发</h1>
                <p class="index_module_content">本课程是基于websocket的火拼俄罗斯的升级版本，课程中在前两个课的基础上实现了用两个浏览器对战模式，完整的实现了游戏</p>
                <div class="index_module_fenge"></div>
                <p class="index_module_time">学习时间：<span>7个月</span></p><div class="index_module_btn">
                    <button class="index_module_details">课程详情</button>
                    <button class="index_module_salary">就业薪资</button>
                </div>   
            </li>
            <li>
                <div class="index_module_img">
                    <img src="${pageContext.request.contextPath }/stageRescourse/image/home/c4.png" alt="">
                </div>
                <h1 class="index_module_title">前端与移动开发</h1>
                <p class="index_module_content">本课程是基于websocket的火拼俄罗斯的升级版本，课程中在前两个课的基础上实现了用两个浏览器对战模式，完整的实现了游戏</p>
                <div class="index_module_fenge"></div>
                <p class="index_module_time">学习时间：<span>7个月</span></p><div class="index_module_btn">
                    <button class="index_module_details">课程详情</button>
                    <button class="index_module_salary">就业薪资</button>
                </div>   
            </li>
        </ul> -->
        </section>
        <!-- 课程介绍结束 -->
    </div> 
    <!-- 推荐课程结束 -->


    <!-- 在线测评开始 -->
    <div id="index_test">
        <div class="index_module_logo">
            <img src="${pageContext.request.contextPath }/stageRescourse/image/home/m2.png" alt="">
        </div>
     <!--    <ul class="index_test_btn">
            <li class="index_test_btnActive">测评</li>
            <li>排名</li>
        </ul> -->
        <div class="index_test_list">
            <ul class="index_ceping">
                <li>
                    <h1 class="index_test_title">
                    		<a href="http://www.csgmooc.com/00267/" target="_blank">实验平台</a>
                    </h1>
                    <p class="index_test_description">在线实验 成绩可靠并且立即可见</p>
                    <a href="http://193.112.6.35/#/" class="index_test_more">
                        <span class="index_test_moreActive"></span>
                        <span></span>
                        <span></span>
                    </a>
                </li>
                <li>
                    <h1 class="index_test_title">
                      <a href="http://193.112.6.35/#/">评测平台</a>
                    </h1>
                    <p class="index_test_description">在线评测 阶段测评随时进行</p>
                    <a href="" class="index_test_more">
                        <span class="index_test_moreActive"></span>
                        <span></span>
                        <span></span>
                    </a>
                </li>
                <li>
                    <h1 class="index_test_title">
                      <a href="http://39.104.81.8/Students">学习记录平台</a>
                    </h1>
                    <p class="index_test_description">记录学习的点点滴滴，助力明天的美好前程</p>
                    <a href="" class="index_test_more">
                        <span class="index_test_moreActive"></span>
                        <span></span>
                        <span></span>
                    </a>
                </li>
                 <li>
                    <h1 class="index_test_title">
                      <a href="#">正在建设</a>
                    </h1>
                    <p class="index_test_description">正在建设中</p>
                    <a href="" class="index_test_more">
                        <span class="index_test_moreActive"></span>
                        <span></span>
                        <span></span>
                    </a>
                </li>
                <!-- <li>
                    <h1 class="index_test_title">随堂测试</h1>
                    <p class="index_test_description">在线考评 成绩可靠并且立即可见</p>
                    <a href="" class="index_test_more">
                        <span class="index_test_moreActive"></span>
                        <span></span>
                        <span></span>
                    </a>
                </li>
                <li>
                    <h1 class="index_test_title">随机测试</h1>
                    <p class="index_test_description">在线考评 成绩可靠并且立即可见</p>
                    <a href="" class="index_test_more">
                        <span class="index_test_moreActive"></span>
                        <span></span>
                        <span></span>
                    </a>
                </li> -->
            </ul>
            <ul class="index_rank">
                <li>
                    <div class="index_rank_num"><img src="${pageContext.request.contextPath }/stageRescourse/image/home/n1.png" alt=""></div>
                    <div class="index_rank_content">
                        <div class="index_rank_img">
                            <img src="${pageContext.request.contextPath }/stageRescourse/image/home/h2.png" alt="">
                        </div>
                        <div class="index_rank_contents">
                            <a href=""><h1 class="index_interaction_rankListT">建设事业部精益项目终审会&nbsp;</h1></a>
                            <p class="index_interaction_rankListD">委书记孙永强同志带领全体党员在上海中共一大会址足使每一位党员接受了一次深刻。</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="index_rank_num"><img src="${pageContext.request.contextPath }/stageRescourse/image/home/n2.png" alt=""></div>
                    <div class="index_rank_content">
                        <div class="index_rank_img">
                            <img src="${pageContext.request.contextPath }/stageRescourse/image/home/h2.png" alt="">
                        </div>
                        <div class="index_rank_contents">
                            <a href=""><h1 class="index_interaction_rankListT">建设事业部精益项目终审会&nbsp;</h1></a>
                            <p class="index_interaction_rankListD">委书记孙永强同志带领全体党员在上海中共一大会址足使每一位党员接受了一次深刻。</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="index_rank_num"><img src="${pageContext.request.contextPath }/stageRescourse/image/home/n3.png" alt=""></div>
                    <div class="index_rank_content">
                        <div class="index_rank_img">
                            <img src="${pageContext.request.contextPath }/stageRescourse/image/home/h2.png" alt="">
                        </div>
                        <div class="index_rank_contents">
                            <a href=""><h1 class="index_interaction_rankListT">建设事业部精益项目终审会&nbsp;</h1></a>
                            <p class="index_interaction_rankListD">委书记孙永强同志带领全体党员在上海中共一大会址足使每一位党员接受了一次深刻。</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="index_rank_num"><img src="${pageContext.request.contextPath }/stageRescourse/image/home/n4.png" alt=""></div>
                    <div class="index_rank_content">
                        <div class="index_rank_img">
                            <img src="${pageContext.request.contextPath }/stageRescourse/image/home/h2.png" alt="">
                        </div>
                        <div class="index_rank_contents">
                            <a href=""><h1 class="index_interaction_rankListT">建设事业部精益项目终审会&nbsp;</h1></a>
                            <p class="index_interaction_rankListD">委书记孙永强同志带领全体党员在上海中共一大会址足使每一位党员接受了一次深刻。</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="index_test_fenge">
            <div class="index_test_fengeA"></div>
        </div>
    </div>
    <!-- 在线测评结束 -->

    <!-- 在线互动开始 -->
    <div id="index_interaction">
        <div class="index_module_logo">
            <img src="${pageContext.request.contextPath }/stageRescourse/image/home/m3.png" alt="">
        </div>
        <div class="index_interaction_list">
            <div class="index_interaction_rank">
                <div class="index_interaction_top">
                    <div class="index_interaction_logo">
                        <img src="${pageContext.request.contextPath }/stageRescourse/image/home/h1.png" alt="">
                    </div>
                    <h1 class="index_interaction_title">互动排行</h1>
                </div>
                <div class="index_interaction_rankList">
                    <a href="" class="index_interaction_img">
                        <img src="${pageContext.request.contextPath }/stageRescourse/image/main/news1.png" alt="">
                    </a>
                    <div class="index_interaction_content">
                        <a href="${pageContext.request.contextPath }/htmls/main1.jsp"><h1 class="index_interaction_rankListT">
                          新工科背景下软件工程专业课程改革研讨与实践 </h1></a>
                        <p class="index_interaction_rankListD">
                          中北大学软件学院携手优逸客开展新工科背景下软件工程专业课程改革研讨与实践
                        </p>
                    </div>
                </div>
                <div class="index_interaction_rankList">
                    <a href="" class="index_interaction_img">
                        <img src="${pageContext.request.contextPath }/stageRescourse/image/main/news9.png" alt="">
                    </a>
                    <div class="index_interaction_content">
                        <a href="${pageContext.request.contextPath }/htmls/main2.jsp"><h1 class="index_interaction_rankListT">校企合作育栋梁 </h1></a>
                        <p class="index_interaction_rankListD">
                          中北大学软件学院携手优逸客为学生提供为期2周的企业项目实战
                        </p>
                    </div>
                </div>
				 <div class="index_interaction_rankList">
                    <a href="" class="index_interaction_img">
                        <img src="${pageContext.request.contextPath }/stageRescourse/image/main/news28.png" alt="">
                    </a>
                    <div class="index_interaction_content">
                        <a href="${pageContext.request.contextPath }/htmls/main3.jsp"><h1 class="index_interaction_rankListT">
                         基于O2O云平台的实践类课程教学改革 </h1></a>
                        <p class="index_interaction_rankListD">
                          软件学院基于O2O云平台的实践类课程教学改革
                        </p>
                    </div>
                </div>
                <div class="index_interaction_rankList">
                    <a href="" class="index_interaction_img">
                        <img src="${pageContext.request.contextPath }/stageRescourse/image/main/news29.png" alt="">
                    </a>
                    <div class="index_interaction_content">
                        <a href="${pageContext.request.contextPath }/htmls/main4.jsp"><h1 class="index_interaction_rankListT">《面向对象程序设计课程交流会》 </h1></a>
                        <p class="index_interaction_rankListD">
                        中北大学软件学院&优逸客科技开展《面向对象程序设计》课程建设交流会
                        </p>
                    </div>
                </div>
            </div>
            <ul class="index_interaction_personal">
                <li>
                    <a href="" class="index_interaction_personalT">师者风采</a>
                    <p class="index_interaction_personalD">
                      <a href="http://url.cn/5cANsKk" class="index_interaction_personalT"  target="_blank">李华玲：保持热情，保持前进</a>
                    </p>
                </li>

                <li>
                    <a href="" class="index_interaction_personalT">在校学生</a>
                    <p class="index_interaction_personalD">
                      <a href="http://url.cn/5YUa50T" class="index_interaction_personalT" target="_blank">张丹：人若有志，万事可为 </a>
                    </p>
                </li>
            
                <li>
                    <a href="" class="index_interaction_personalT">毕业学生</a>
                    <p class="index_interaction_personalD">
                      <a href="http://url.cn/5UhLbJM" class="index_interaction_personalT" target="_blank">刘兴：奋斗之路，永不停止</a>
                    </p>
                </li>
				
                <!-- <li>
                    <a href="" class="index_interaction_more">
                        <div>
                            <span class="index_test_moreActive"></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                </li> -->
            </ul>
        </div>
    </div>
    <!-- 在线互动结束 -->

    <!-- 师资团队开始 -->
    <div id="index_techer">
        <div class="index_module_logo">
            <img src="${pageContext.request.contextPath }/stageRescourse/image/home/m4.png" alt="">
        </div>
        <ul class="index_techer_list">
            <li>
                <div class="index_techer_img">
				<a href="${pageContext.request.contextPath }/htmls/introduce3.jsp" style="display:block;width:100%;height:100%">	
                    <img src="${pageContext.request.contextPath }/stageRescourse/image/teacherTeam/lhl.jpg" alt="">
				</a>
                </div>
                <h1 class="index_techer_title">李华玲</h1>
                <p class="index_techer_description">
                  研究方向：数据可视化、大数据分析
                </p>
            </li>
            <li>
                <div class="index_techer_img">
					<a href="${pageContext.request.contextPath }/htmls/introduce2.jsp" style="display:block;width:100%;height:100%">
						<img src="${pageContext.request.contextPath }/stageRescourse/image/teacherTeam/ysq.png" alt="">
					</a>
                </div>
                <h1 class="index_techer_title">尹四清</h1>
                <p class="index_techer_description">
                  研究方向：网络信息处理、网络安全、软件开发与软件测试。
                </p>
            </li>
            <li>
                <div class="index_techer_img">
					<a href="${pageContext.request.contextPath }/htmls/introduce1.jsp" style="display:block;width:100%;height:100%">
						<img src="${pageContext.request.contextPath }/stageRescourse/image/teacherTeam/xhl.png" alt="">
					</a>
                </div>
                <h1 class="index_techer_title">薛海丽</h1>
                <p class="index_techer_description">
                  研究方向：智能优化算法及其应用、云计算、软件开发与软件测试。
                </p>
            </li>
        </ul>
        <!--<a href="" class="index_techer_more">更多</a>-->
    </div>
    <!-- 师资团队结束 -->
  <!-- 底部开始 -->
<footer>
  <div class="contain">
    <div class="main">
      <div class="text">
        <p>版权所有 中北大学软件学院&nbsp;&nbsp;&nbsp;&nbsp;综合科：0351-3924578&nbsp;&nbsp;&nbsp;&nbsp;教学科：0351-3925275&nbsp;&nbsp;&nbsp;&nbsp;学生科、团委：0351-3924613&nbsp;&nbsp;&nbsp;&nbsp;科研与对外合作科：0351-3924595</p>
        <p>邮箱：sti@nuc.edu.cn&nbsp;&nbsp;&nbsp;&nbsp;本站由山西优逸客科技有限公司维护&nbsp;&nbsp;&nbsp;&nbsp;
        <!-- <a href="#" target="_blank">后台管理</a> -->
        </p>
      </div>
    </div>
  </div>
</footer>
<!-- 底部结束 -->
</html>