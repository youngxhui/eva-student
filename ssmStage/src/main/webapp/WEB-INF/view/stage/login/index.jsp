<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>中北在线交流平台</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/base.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/animate.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/index.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/header.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/swiper.css">

	 
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/spider.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/jquery.slider.css">
	
    <script src="${pageContext.request.contextPath }/stageRescourse/js/jquery-3.2.1.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/swiper.min.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/index.js"></script>
   
	 <script src="${pageContext.request.contextPath }/stageRescourse/js/spider.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/jquery.slider.min.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/banner.js"></script>

</head>
<body>
    <header id="header">
      <div class="logo">
		  <a href="${pageContext.request.contextPath }/login/main">
			<img src="${pageContext.request.contextPath }/stageRescourse/image/home/logo3.png" alt="">
		  </a>
          <div class="name">
              <h3>中北在线交流平台</h3>
              <h6>中北在线</h6>
          </div>
      </div>
      <!--logo -->

      <div class="search">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/sseachleft.png" alt='' class="sleft">
          <input type="text" placeholder="请输入搜索的课程" class="sinput">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/ssearchright.png" alt='' class="sright">
      </div>
      <!--search-->

      <!--notice -->

      <ul class="nav">
          <!-- 需要选中状态  加hot-->
          <li class="list hot">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/login/main">网站首页</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
				<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">用户中心</a>
				</c:if>
				<c:if test="${userinfo != null }">
					 <a href="${pageContext.request.contextPath }/personCenter/info">用户中心</a>
				</c:if>
				
             
          </li>
		  <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="">后台管理</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
				<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">&nbsp;&nbsp;&nbsp;&nbsp;登录</a>
				</c:if>
				<c:if test="${userinfo != null }">
					<a href="${pageContext.request.contextPath }/login/loginout">&nbsp;&nbsp;&nbsp;&nbsp;退出</a>
				</c:if>
          </li>
          
      </ul>
      <!--nav-->
    </header>
    <!-- banner开始 -->
    <div id="index_banner">

        <div class="index_banner_img">
            <div style="width: 100%;height:100%">
                <div class="slider">
                    <div><a href="#"><img src="${pageContext.request.contextPath }/stageRescourse/image/banner/ynlyxy.jpg" alt=""></a></div>
                    <div><a href="#"><img src="${pageContext.request.contextPath }/stageRescourse/image/banner/lunbo1.jpg" alt=""></a></div>
                    <div><a href="#"><img src="${pageContext.request.contextPath }/stageRescourse/image/banner/banner02.jpg" alt=""></a></div>
                </div>
            </div>


        </div>

        <!-- 个人信息开始 -->
        <div class="index_banner_personal">
            <div class="index_banner_box">
                <a href="${pageContext.request.contextPath }/personCenter/info" class="index_personal_img">
                    <img src="${pageContext.request.contextPath }/stageRescourse/image/home/personal.png" alt="">
                </a>
            </div>
            <h1 class="index_personal_name">${userinfo.username }</h1>
            <h2 class="index_personal_account">${userinfo.sname }</h2>
            <ul class="index_personal_skill">
                <li>
                    <div class="index_banner_skills">
                        <canvas id='canvas' width="67" height="67">
                            <p>You browser not support canvas</p>
                        </canvas>
                    </div>
                    <span>知识点</span>
                </li>
                <li>
                    <div class="index_banner_skills">
                        <canvas id='canvas1' width="67" height="67">
                            <p>You browser not support canvas</p>
                        </canvas>
                    </div>
                    <span>技能</span>
                </li>
                <li>
                    <div class="index_banner_skills">
                        <canvas id='canvas2' width="67" height="67">
                            <p>You browser not support canvas</p>
                        </canvas>
                    </div>
                    <span>能力</span>
                </li>
            </ul>
        </div>
        <!-- 个人信息结束 -->
    </div>  
    <!-- banner结束 -->

    <!-- 推荐课程开始 -->
    <div id="index_recommend">
        <div class="index_module_logo">
            <img src="${pageContext.request.contextPath }/stageRescourse/image/home/m1.png" alt="">
        </div>
        <div class="index_module_more">
            <a href="${pageContext.request.contextPath }/course/index">
                <span>更多</span>
                <i>&#xe6f5;</i>
            </a>
        </div>
        
        <!-- 课程介绍开始 -->
        <ul class="index_module_course">
           
			<c:forEach items="${requestScope.courses }" var="course" begin="0" end="3">
				
				 <li>
					<div class="progress"></div>
					<div class="index_module_img">
						<img src="${pageContext.request.contextPath }/stageRescourse/image/course/${course.cover_image }" alt="">
					</div>
					<h1 class="index_module_title">${course.name }</h1>
					<p class="index_module_content">${course.name }</p>
					<div class="index_module_fenge"></div>
					<p class="index_module_time"><span></span></p><div class="index_module_btn">
						<button class="index_module_details">
							<a href="${pageContext.request.contextPath }/course/video/${course.id}/knowledgeId/0"><font color='white'>免费观看</font></a>
						</button>
						<button class="index_module_salary">
							<a href="${pageContext.request.contextPath }/course/relevantDocument/courseId/${course.id }/pid/0"><font color='white'>相关资料</font></a>
						</button>
					</div>   
				</li>
				
			</c:forEach>
			
           
        </ul>
        <!-- 课程介绍结束 -->
    </div> 
    <!-- 推荐课程结束 -->

    

    <!-- 在线测评开始 -->
    <div id="index_test">
        <div class="index_module_logo">
            <img src="${pageContext.request.contextPath }/stageRescourse/image/home/m2.png" alt="">
        </div>
        <!--<ul class="index_test_btn">
            <li class="index_test_btnActive">测评</li>
            <li>排名</li>
        </ul>-->
        <div class="index_test_list">
            <ul class="index_ceping">
                <li>
                    <h1 class="index_test_title">
						<a href="http://www.csgmooc.com/00267/" target="_blank">
							<font color="white">实验平台</font>
						</a>
					</h1>
                    <p class="index_test_description">在线实验 成绩可靠并且立即可见</p>
                    <a href="" class="index_test_more">
                        <span class="index_test_moreActive"></span>
                        <span></span>
                        <span></span>
                    </a>
                </li>
                <li>
                    <h1 class="index_test_title">
						<a href="http://193.112.6.35/#/" target="_blank">
							<font color="white">评测平台</font>
						</a>
					</h1>
                    <p class="index_test_description">在线评测 阶段测评随时进行</p>
                    <a href="" class="index_test_more">
                        <span class="index_test_moreActive"></span>
                        <span></span>
                        <span></span>
                    </a>
                </li>
                
                <li>
                    <h1 class="index_test_title">
						<a href="http://39.104.81.8/Students" target="_blank">
							<font color="white">学习记录平台</font>
						</a>
					</h1>
                    <p class="index_test_description">记录学习的点点滴滴，助力明天的美好前程</p>
                    <a href="" class="index_test_more">
                        <span class="index_test_moreActive"></span>
                        <span></span>
                        <span></span>
                    </a>
                </li>
				
                <li>
                    <h1 class="index_test_title">正在建设</h1>
                    <p class="index_test_description">正在建设中</p>
                    <a href="" class="index_test_more">
                        <span class="index_test_moreActive"></span>
                        <span></span>
                        <span></span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="index_test_fenge">
            <div class="index_test_fengeA"></div>
        </div>
    </div>
    <!-- 在线测评结束 -->

    <!-- 在线互动开始 -->
    <div id="index_interaction">
        <div class="index_module_logo">
            <img src="${pageContext.request.contextPath }/stageRescourse/image/home/m3.png" alt="">
        </div>
        <div class="index_interaction_list">
            <div class="index_interaction_rank">
                <div class="index_interaction_top">
                    <div class="index_interaction_logo">
                        <img src="${pageContext.request.contextPath }/stageRescourse/image/home/h1.png" alt="">
                    </div>
                    <h1 class="index_interaction_title">互动排行</h1>
                </div>
                <div class="index_interaction_rankList">
                    <a href="" class="index_interaction_img">
                        <img src="${pageContext.request.contextPath }/stageRescourse/image/main/news1.png" alt="">
                    </a>
                    <div class="index_interaction_content">
                        <a href="${pageContext.request.contextPath }/htmls/main1.jsp"><h1 class="index_interaction_rankListT">新工科背景下软件工程专业课程改革实践 </h1></a>
                        <p class="index_interaction_rankListD">中北大学软件学院携手优逸客开展新工科背景下软件工程专业课程改革研讨与实践。</p>
                    </div>
                </div>
                <div class="index_interaction_rankList">
                    <a href="" class="index_interaction_img">
                        <img src="${pageContext.request.contextPath }/stageRescourse/image/main/news9.png" alt="">
                    </a>
                    <div class="index_interaction_content">
                        <a href="${pageContext.request.contextPath }/htmls/main2.jsp"><h1 class="index_interaction_rankListT">基于O2O云平台的实践类课程教学改革 </h1></a>
                        <p class="index_interaction_rankListD">软件学院基于O2O云平台的实践类课程教学改革</p>
                    </div>
                </div>
            </div>

            <ul class="index_interaction_personal">
                <li>
                    <a href="" class="index_interaction_personalT">师者风采</a>
                    <p class="index_interaction_personalD">
						<a href="http://url.cn/5cANsKk" class="index_interaction_personalT"  target="_blank">李华玲：保持热情，保持前进</a>
					</p>
                    <div class="progress"></div>
                </li>
                <li>
                    <a href="" class="index_interaction_personalT">在校学生风采</a>
                    <p class="index_interaction_personalD">
						<a href="http://url.cn/5YUa50T" class="index_interaction_personalT" target="_blank">张丹：人若有志，万事可为 </a>
					</p>
                    <div class="progress"></div>

                </li>
                <li>
                    <a href="" class="index_interaction_personalT">毕业学生风采</a>
                    <p class="index_interaction_personalD">
						<a href="http://url.cn/5UhLbJM" class="index_interaction_personalT" target="_blank">刘兴：奋斗之路，永不停止</a>
					</p>
                    <div class="progress"></div>
                </li>
                <li>
                    <a href="" class="index_interaction_more">
                        <div>
                            <span class="index_test_moreActive"></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                </li>
            </ul>


        </div>
    </div>
    <!-- 在线互动结束 -->

    <!-- 师资团队开始 -->
    <div id="index_techer">
        <div class="index_module_logo">
            <img src="${pageContext.request.contextPath }/stageRescourse/image/home/m4.png" alt="">
        </div>
        <ul class="index_techer_list">
            <li>
                <div class="index_techer_img">
					<a href="${pageContext.request.contextPath }/htmls/introduce3.jsp" style="display:block;width:100%;height:100%">	
						<img src="${pageContext.request.contextPath }/stageRescourse/image/teacherTeam/lhl.jpg" alt="">
					</a>
                   
                </div>
                <h1 class="index_techer_title">李华玲</h1>
                <p class="index_techer_description">研究方向：数据可视化、大数据分析。</p>
                <div class="progress"></div>
            </li>
            <li>
                <div class="index_techer_img">
					<a href="${pageContext.request.contextPath }/htmls/introduce2.jsp" style="display:block;width:100%;height:100%">
						<img src="${pageContext.request.contextPath }/stageRescourse/image/teacherTeam/ysq.png" alt="">
					</a>
                </div>
                <h1 class="index_techer_title">尹四清</h1>
                <p class="index_techer_description">研究方向：网络信息处理、网络安全、软件开发与软件测试。</p>
                <div class="progress"></div>
            </li>
            <li>
                <div class="index_techer_img">
					<a href="${pageContext.request.contextPath }/htmls/introduce1.jsp" style="display:block;width:100%;height:100%">
						<img src="${pageContext.request.contextPath }/stageRescourse/image/teacherTeam/xhl.png" alt="">
					</a>
                </div>
                <h1 class="index_techer_title">薛海丽</h1>
                <p class="index_techer_description">研究方向：智能优化算法及其应用、云计算、软件开发与软件测试。</p>
                <div class="progress"></div>

            </li>
        </ul>
        <a href="" class="index_techer_more">更多</a>
    </div>
    <!-- 师资团队结束 -->
    <!-- 底部开始 -->
    <footer>
      <div class="contain">
        <img src="${pageContext.request.contextPath }/stageRescourse/image/footer/footer_06.png" alt="">
      </div>
    </footer>
    <!-- 底部结束 -->
    <script type="text/javascript" src="${pageContext.request.contextPath }/stageRescourse/Ban3D/box-slider-all.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/banner.js"></script>
</body>
</html>