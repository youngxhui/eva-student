<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<title>视频内容</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/base.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/header.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/content.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/footer.css">
	<style type="text/css">
		.nextborder{
			height:1665px;
			overflow:auto;
			width:584px;
		}
	</style>
</head>
<body>

<header id="header">
      <div class="logo">
		  <a href="${pageContext.request.contextPath }/login/main">
			<img src="${pageContext.request.contextPath }/stageRescourse/image/home/logo3.png" alt="">
		  </a>
          <div class="name">
              <h3>中北在线交流平台</h3>
              <h6>中北在线</h6>
          </div>
      </div>
      <!--logo -->

      <div class="search">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/sseachleft.png" alt='' class="sleft">
          <input type="text" placeholder="请输入搜索的课程" class="sinput">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/ssearchright.png" alt='' class="sright">
      </div>
      <!--search-->

      <!--notice -->

      <ul class="nav">
          <!-- 需要选中状态  加hot-->
          <li class="list hot">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/login/main">网站首页</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              	<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">用户中心</a>
				</c:if>
				<c:if test="${userinfo != null }">
					 <a href="${pageContext.request.contextPath }/personCenter/info">用户中心</a>
				</c:if>
				
          </li>
		  <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="">后台管理</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
				<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">&nbsp;&nbsp;&nbsp;&nbsp;登录</a>
				</c:if>
				<c:if test="${userinfo != null }">
					<a href="${pageContext.request.contextPath }/login/loginout">&nbsp;&nbsp;&nbsp;&nbsp;退出</a>
				</c:if>
          </li>
          
      </ul>
      <!--nav-->
    </header>

<!-- 详细信息开始 -->
	<section id="more_detail">
	    <div class="more_main">
	    	<!-- 面包屑 -->
			<div class="more_bread">
				 <img src="${pageContext.request.contextPath }/stageRescourse/image/more/more01.png" alt="">
				 <a href="${pageContext.request.contextPath }/login/main">首页</a>
				 <img src="${pageContext.request.contextPath }/stageRescourse/image/more/more02.png" alt="">
				 <img src="${pageContext.request.contextPath }/stageRescourse/image/more/more03.png" alt="">
				 <a href="">视频</a>
			</div>
			<div class="more_xinxi">
				<h2>学院:</h2>
				<h3>软件学院</h3>
			</div>
	    </div>
		
	</section>
	<!-- 详细信息结束 -->
	<!-- 主体视频开始 -->
	<section id="content">
		<center><h1>暂时还未上传视频，敬请期待！</h1></center>
		<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
	</section>
	<!-- 主体视频结束 -->
	
  <!-- 底部开始 -->
    <footer>
      <div class="contain">
        <img src="${pageContext.request.contextPath }/stageRescourse/image/footer/footer_06.png" alt="">
      </div>
    </footer>
    <!-- 底部结束 -->
	
<!-- 底部结束 -->

</body>
</html>
