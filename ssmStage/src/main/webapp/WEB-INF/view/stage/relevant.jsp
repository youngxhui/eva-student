'<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
	<title>课程安排更多</title>
	<meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/base.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/header.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/more.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/footer.css">
	<script src="${pageContext.request.contextPath }/stageRescourse/js/jquery-3.2.1.js"></script>
	<script src="${pageContext.request.contextPath }/stageRescourse/js/more.js"></script>
	
</head>
<body>
 
<header id="header">
      <div class="logo">
		  <a href="${pageContext.request.contextPath }/login/main">
			<img src="${pageContext.request.contextPath }/stageRescourse/image/home/logo3.png" alt="">
		  </a>
          <div class="name">
              <h3>中北在线交流平台</h3>
              <h6>中北在线</h6>
          </div>
      </div>
      <!--logo -->

      <div class="search">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/sseachleft.png" alt='' class="sleft">
          <input type="text" placeholder="请输入搜索的课程" class="sinput">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/ssearchright.png" alt='' class="sright">
      </div>
      <!--search-->

      <!--notice -->

      <ul class="nav">
          <!-- 需要选中状态  加hot-->
          <li class="list hot">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/login/main">网站首页</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
			  	<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">用户中心</a>
				</c:if>
				<c:if test="${userinfo != null }">
					 <a href="${pageContext.request.contextPath }/personCenter/info">用户中心</a>
				</c:if>
          </li>
		  <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="">后台管理</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
				<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">&nbsp;&nbsp;&nbsp;&nbsp;登录</a>
				</c:if>
				<c:if test="${userinfo != null }">
					<a href="${pageContext.request.contextPath }/login/loginout">&nbsp;&nbsp;&nbsp;&nbsp;退出</a>
				</c:if>
          </li>
          
      </ul>
      <!--nav-->
    </header>


<!-- 课程开始 -->
<section id="more_course">
	<div class="more_courseTop">
		<div class="more_courseLeft">
			<h1 class="hot">相关资料</h1>
			|
			<h1>&nbsp;&nbsp;&nbsp;&nbsp;<a href="${pageContext.request.contextPath }/course/relevantDocument/courseId/${courseId }/pid/0">所有</a></h1>
			
			<c:if test="${pid !=0 }">
				|
				<h1>&nbsp;&nbsp;&nbsp;&nbsp;${pid }</h1>
			</c:if>
		</div>
		
	</div>
	<!-- 视频课程列表 -->
	<ul class="more_courseBox more_courseBox1">
		<c:forEach items="${requestScope.resources }" var="resource" begin="0" end="8">
			
			<li style="display:block" id="hiddenmodel" title="${resource.name }">
				    <c:if test="${resource.type == 0 }">
						<a href="${pageContext.request.contextPath }/course/relevantDocument/courseId/${courseId}/pid/${resource.id }" class="more_imgBox more_imgBox1">
					   		<img src="${pageContext.request.contextPath }/stageRescourse/image/resourceManager/folder.png" alt="">
						</a>
					</c:if>
					<c:if test="${resource.type == 1 }">
						<c:if test='${resource.name.endsWith("word") }'>
							<img src="${pageContext.request.contextPath }/stageRescourse/image/resourceManager/word.png" alt="">
						</c:if>
						<c:if test='${resource.name.endsWith("docx") }'>
							<img src="${pageContext.request.contextPath }/stageRescourse/image/resourceManager/word.png" alt="">
						</c:if>
						<c:if test='${resource.name.endsWith("doc") }'>
							<img src="${pageContext.request.contextPath }/stageRescourse/image/resourceManager/word.png" alt="">
						</c:if>
						<c:if test='${resource.name.endsWith("xlsx") }'>
							<img src="${pageContext.request.contextPath }/stageRescourse/image/resourceManager/excel.png" alt="">
						</c:if>
						<c:if test='${resource.name.endsWith("zip") }'>
							<img src="${pageContext.request.contextPath }/stageRescourse/image/resourceManager/zip.png" alt="">
						</c:if>
						<c:if test='${resource.name.endsWith("PPT") or resource.name.endsWith("ppt") }'>
							<img src="${pageContext.request.contextPath }/stageRescourse/image/resourceManager/ppt.png" alt="">
						</c:if>
						<c:if test='${resource.name.endsWith("PPTX") or resource.name.endsWith("pptx") }'>
							<img src="${pageContext.request.contextPath }/stageRescourse/image/resourceManager/ppt.png" alt="">
						</c:if>
						<c:if test='${resource.name.endsWith("pdf") }'>
							<img src="${pageContext.request.contextPath }/stageRescourse/image/resourceManager/pdf.png" alt="">
						</c:if>
						
						<c:if test='${resource.name.endsWith("java") }'>
							<img src="${pageContext.request.contextPath }/stageRescourse/image/resourceManager/code.png" alt="">
						</c:if>

						<c:if test='${resource.name.endsWith("rar") }'>
							<img src="${pageContext.request.contextPath }/stageRescourse/image/resourceManager/rar.png" alt="">
						</c:if>

					</c:if>
				<div class="more_title">
					<h2>
						<a href="http://59.110.154.79:8080/ssm/${resource.url}">${resource.name }</a>
					</h2>
					<h3>
						
						<%-- <c:if test="${resource.type == 0 }">
							已有10人查看
						</c:if>
						<c:if test="${resource.type == 1 }">
							已有6人下载
						</c:if> --%>
					</h3>
				</div>
				
			</li>
			
		</c:forEach>
		
	</ul>
	<!-- 分页导航 -->
	<!-- <div class="more_fenye">
		<h2 class="pageoption">首页</h2>
		<h2 class="pageoption prevpage">上一页</h2>
		
		
		<h2 class="pageoption nextpage">下一页</h2>
		<h2 class="pageoption">尾页</h2>
		
		<input type="hidden" class="pagetotal">
	</div> -->
</section>
<!-- 课程结束 -->


<!-- 底部开始 -->
    <footer>
      <div class="contain">
        <img src="${pageContext.request.contextPath }/stageRescourse/image/footer/footer_06.png" alt="">
      </div>
    </footer>
<!-- 底部结束 -->
	

</body>
</html>
