<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>person</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/base.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/animate.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/index.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/header.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/swiper.css">

	
    <script src="${pageContext.request.contextPath }/stageRescourse/js/jquery-3.2.1.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/swiper.min.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/index.js"></script>
   
	
    <link href="${pageContext.request.contextPath }/stageRescourse/Ban3D/screen.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath }/stageRescourse/Ban3D/modernizr.js"></script>
	
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/ydh_person.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/cnew.css">
	
</head>
<body>
	<header id="header">
      <div class="logo">
		  <a href="${pageContext.request.contextPath }/login/main">
			<img src="${pageContext.request.contextPath }/stageRescourse/image/home/logo3.png" alt="">
		  </a>
          <div class="name">
              <h3>中北在线交流平台</h3>
              <h6>中北在线</h6>
          </div>
      </div>
      <!--logo -->

      <div class="search">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/sseachleft.png" alt='' class="sleft">
          <input type="text" placeholder="请输入搜索的课程" class="sinput">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/ssearchright.png" alt='' class="sright">
      </div>
      <!--search-->

      <!--notice -->

      <ul class="nav">
          <!-- 需要选中状态  加hot-->
          <li class="list hot">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/login/main">网站首页</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
               	<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">用户中心</a>
				</c:if>
				<c:if test="${userinfo != null }">
					 <a href="${pageContext.request.contextPath }/personCenter/info">用户中心</a>
				</c:if>
          </li>
		  <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="">后台管理</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
				<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">&nbsp;&nbsp;&nbsp;&nbsp;登录</a>
				</c:if>
				<c:if test="${userinfo != null }">
					<a href="${pageContext.request.contextPath }/login/loginout">&nbsp;&nbsp;&nbsp;&nbsp;退出</a>
				</c:if>
          </li>
          
      </ul>
      <!--nav-->
    </header>
	
	<div class="ydh_main">
		<div class="ydh_imgBox">
			<img src="${pageContext.request.contextPath }/stageRescourse/image/introduce/photo2.png" alt="">
		</div>
		<div class="ydh_con1">
			<div class="ydh_name">尹四清（副教授、副院长、硕导）</div>
			<!--<div class="ydh_date">发布日期：2018.3.21</div>-->
			<ul>
				<li>尹四清,男,硕士,软件工程学院软件开发与测试专业</li>
				<li>联系方式：0351-3925270(办)&nbsp;&nbsp;&nbsp;&nbsp;yinsq@nuc.edu.cn,yinsq@163.com</li>
			</ul>
			<div class="ydh_title">
				研究方向: 
			</div>
			<div class="ydh_content">
				数据可视化、大数据分析
			</div>
		</div>
		<div class="ydh_con2">
			<div class="ydh_title">
				教育经历<span>&nbsp;/&nbsp;Educational experience</span>
			</div>
			<div class="ydh_content">
				1964年生，中共党员，山西省屯留县人。1985年本科毕业于太原机械学院计算机应用专业，1991年研究生毕业于合肥工业大学计算机应用专业，获工学硕士学位。现为中北大学软件学院副教授、副院长，硕士生导师，2010年度校级教学名师。
				
			</div>
			<div class="ydh_title">
				研究方向<span>&nbsp;/&nbsp;Research findings</span>
			</div>
			<div class="ydh_content">
				网络信息处理、网络安全、软件开发与软件测试。
				
			</div>
			<div class="ydh_title">
				研究成果<span>&nbsp;/&nbsp;Research findings</span>
			</div>
			<div class="ydh_content">
				作为骨干或主设计参加多个科学或教学研究项目，主要有：公共食堂电脑磁卡售餐系统、电力变压器CAD软件系统，军用文字信息输入识别与处理系统（国防科工委“九五”预研项目）、基于ATM平台的网络信息实时翻译与智能检索技术研究（国家自然科学基金项目）、多语大型网络信息集成翻译处理系统（本项目获中国科学院科技进步二等奖），山西省教育厅教改项目《软件类"订单培养"教学模式的研究与实践》（排名第四，2010年度山西省教学成果一等奖），《网络与安全管理》精品课程建设（排名第二，2008年度山西省精品课程）。目前，参与山西省发展与改革委员会项目《中北大学软件实训基地暨软件开发支撑平台》，主持山西省教改项目《基于“3+1”模式的软件工程专业复合型人才培养体系研究与实践》。主编或参编教材多部，主要有：《微型计算机硬件基础》（第一作者，山西科学技术出版社），《C语言程序设计实验教程》（主编，国防工业出版社）、《程序设计实训理论教程》（第二作者，国防工业出版社）、《计算机公共基础上机实训》（副主编，高等教育出版社）。在《计算机工程与应用》等杂志或国际会议上发表论文10多篇。
				
			</div>
		</div>
		<div class="ydh_btns">
			<div class="ydh_btn"><a href=""></a></div>
			<div class="ydh_btn"><a href=""></a></div>
		</div>
	</div>
	<footer>
		<img src="${pageContext.request.contextPath }/stageRescourse/ydh_img/ydh_footer_03.png" alt="">
	</footer>
</body>
</html>