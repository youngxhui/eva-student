<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>中北新闻详情页面</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/cnew.css">
</head>
<body>
	<div class="cnewbody">
		<div class="ccontainer">
			
			<!-- 标题部分 -->
			<h2 class="ctime">2018.1.10</h2>
			<h2 class="ctitle">基于O2O云平台的实践类课程教学改革</h2>
			<p class="cdescription">软件学院基于O2O云平台的实践类课程教学改革</p>
			
			<!-- 收藏和观看 -->

			<div class="cnumber">
				<span class="ccollect">32</span>
				<span class="cread">2</span>
			</div>

			<!-- 正文 -->
			<div class="ccontent">
			
				
				<p>为落实国务院相关文件精神，围绕“互联网+教育”的四个核心内容，探索校企合作新型教育服务供给方式， 2017年12月29日 ，软件学院基于O2O云平台的实践类课程教学改革项目正式启动。启动仪式在行政主楼二层会议室举行。副校长熊继军、CSG公司总经理俞鑫出席，教务处、教师教育发展中心及软件学院等相关部门负责人参加。软件学院院长宋文爱主持启动仪式。</p>
                <p>此项目建设内容主要涉及数字化教学资源库、数字化实践教学案例库、实践教学平台、虚拟仿真实验平台等实践教学综合平台软件及配套服务器、存储设备、配套网络设备等基础硬件。旨在共建“实践课堂教学平台”，打造学校—老师—学生的O2O生态圈，通过建设资源平台、管理平台和大数据分析平台，促进学生学习和教师发展，推动教育教学改革。</p>
               
                <p>启动仪式上，副校长熊继军强调了此项目在人才培养领域的重大意义与作用。他表示，本次双方在Java类实践课堂开展的教学改革对于积极探索新的教育教学模式，进一步提升教学质量有着重要意义。并从项目的实施意义、实施规划、实施措施等方面提出意见：积极探索高校教学新模式是“互联网+教育”时代的必然要求，双方要抓住机遇，加强合作，积极探索，推动教育信息不断深入，通过改革教学模式，促进我校教学质量的不断提升；要在关键问题上做好充分论证，确保课程教学改革的系统性和可持续性；要做到总体规划，分步实施，在试点的基础上稳步推进课程教学改革，确保收到实效，形成多赢局面。</p>
               
                <p>公司总经理俞鑫对公司的先期投入与项目整体规划的优越性进行了介绍；项目组负责人介绍了项目的总体计划；校企双方负责人就项目实施过程中的相关问题进行了讨论。</p>
                <div class="pic">
                    <img src="${pageContext.request.contextPath }/stageRescourse/image/main/news28.png" alt="">
                </div>
				
			</div>

			<!-- 分页 -->
			<div class="cpage">
				<a href="" class="cprev"></a>
				<a href="" class="cnext"></a>
				<div class="cclear"></div>
			</div>
		</div>
	</div>	
	<footer>
		<img src="${pageContext.request.contextPath }/stageRescourse/images/ydh_footer_03.png" alt="">
	</footer>
</body>
</html>
