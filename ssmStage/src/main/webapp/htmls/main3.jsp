<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
	 <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/base.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/header.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/main.css">
</head>
<body>
    <header id="header">
      <div class="logo">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/logo.png" alt="">
          <div class="name">
              <h3><a href="${pageContext.request.contextPath }/login/main">中北在线交流平台</a></h3>
              <h6>软件学院</h6>
          </div>
      </div>
      <!--logo -->

      <%-- <div class="search">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/sseachleft.png" alt='' class="sleft">
          <input type="text" placeholder="请输入您想要搜索的课程" class="sinput">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/ssearchright.png" alt='' class="sright">
      </div> --%>
      <!--search-->

     
      <!--notice -->

      <ul class="nav">
          <!-- 需要选中状态  加hot-->
          <li class="list hot">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/login/main">网站首页</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <c:if test="${userinfo == null }">
              <a href="${pageContext.request.contextPath }/login/login">&nbsp;&nbsp;&nbsp;&nbsp;登录</a>
            </c:if>
            <c:if test="${userinfo != null }">
              
              <a href="${pageContext.request.contextPath }/login/loginout">&nbsp;&nbsp;&nbsp;&nbsp;退出</a>
            </c:if>
          </li>
         
      </ul>
      <!--nav-->
</header>

    <div class="main">
        <div class="contain">
            <div class="title">
                <h4>基于O2O云平台的实践类课程教学改革</h4>
                <h4>软件学院基于O2O云平台的实践类课程教学改革</h4>
            </div>
            <div class="article">
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;为落实国务院相关文件精神，围绕“互联网+教育”的四个核心内容，探索校企合作新型教育服务供给方式， 2015年12月29日 ，软件学院基于O2O云平台的实践类课程教学改革项目正式启动。启动仪式在行政主楼二层会议室举行。副校长熊继军、CSG公司总经理俞鑫出席，教务处、教师教育发展中心及软件学院等相关部门负责人参加。软件学院院长宋文爱主持启动仪式。</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;此项目建设内容主要涉及数字化教学资源库、数字化实践教学案例库、实践教学平台、虚拟仿真实验平台等实践教学综合平台软件及配套服务器、存储设备、配套网络设备等基础硬件。旨在共建“实践课堂教学平台”，打造学校—老师—学生的O2O生态圈，通过建设资源平台、管理平台和大数据分析平台，促进学生学习和教师发展，推动教育教学改革。</p>
               
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;启动仪式上，副校长熊继军强调了此项目在人才培养领域的重大意义与作用。他表示，本次双方在Java类实践课堂开展的教学改革对于积极探索新的教育教学模式，进一步提升教学质量有着重要意义。并从项目的实施意义、实施规划、实施措施等方面提出意见：积极探索高校教学新模式是“互联网+教育”时代的必然要求，双方要抓住机遇，加强合作，积极探索，推动教育信息不断深入，通过改革教学模式，促进我校教学质量的不断提升；要在关键问题上做好充分论证，确保课程教学改革的系统性和可持续性；要做到总体规划，分步实施，在试点的基础上稳步推进课程教学改革，确保收到实效，形成多赢局面。</p>
               
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CSG公司总经理俞鑫对公司的先期投入与项目整体规划的优越性进行了介绍；项目组负责人介绍了项目的总体计划；校企双方负责人就项目实施过程中的相关问题进行了讨论。</p>
                <div class="pic">
                    <img src="${pageContext.request.contextPath }/stageRescourse/image/main/news28.png" alt="">
                </div>
            </div>
        </div>
    </div>
	  <!-- 底部开始 -->
	<footer>
	  <div class="contain">
		<div class="main">
		  <div class="text">
			<p>版权所有 中北大学软件学院&nbsp;&nbsp;&nbsp;&nbsp;综合科：0351-3924578&nbsp;&nbsp;&nbsp;&nbsp;教学科：0351-3925275&nbsp;&nbsp;&nbsp;&nbsp;学生科、团委：0351-3924613&nbsp;&nbsp;&nbsp;&nbsp;科研与对外合作科：0351-3924595</p>
			<p>邮箱：sti@nuc.edu.cn&nbsp;&nbsp;&nbsp;&nbsp;本站由山西优逸客科技有限公司维护&nbsp;&nbsp;&nbsp;&nbsp;
			<!-- <a href="#" target="_blank">后台管理</a> -->
			</p>
		  </div>
		</div>
	  </div>
	</footer>
	<!-- 底部结束 -->
</body>
</html>