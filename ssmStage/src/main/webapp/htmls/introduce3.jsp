<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>person</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/base.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/animate.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/index.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/header.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/swiper.css">

	
    <script src="${pageContext.request.contextPath }/stageRescourse/js/jquery-3.2.1.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/swiper.min.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/index.js"></script>
   
	
    <link href="${pageContext.request.contextPath }/stageRescourse/Ban3D/screen.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath }/stageRescourse/Ban3D/modernizr.js"></script>
	
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/ydh_person.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/cnew.css">
	
</head>
<body>
	<header id="header">
      <div class="logo">
		  <a href="${pageContext.request.contextPath }/login/main">
			<img src="${pageContext.request.contextPath }/stageRescourse/image/home/logo3.png" alt="">
		  </a>
          <div class="name">
              <h3>中北在线交流平台</h3>
              <h6>中北在线</h6>
          </div>
      </div>
      <!--logo -->

      <div class="search">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/sseachleft.png" alt='' class="sleft">
          <input type="text" placeholder="请输入搜索的课程" class="sinput">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/ssearchright.png" alt='' class="sright">
      </div>
      <!--search-->

      <!--notice -->

      <ul class="nav">
          <!-- 需要选中状态  加hot-->
          <li class="list hot">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/login/main">网站首页</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
               	<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">用户中心</a>
				</c:if>
				<c:if test="${userinfo != null }">
					 <a href="${pageContext.request.contextPath }/personCenter/info">用户中心</a>
				</c:if>
          </li>
		  <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="">后台管理</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
				<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">&nbsp;&nbsp;&nbsp;&nbsp;登录</a>
				</c:if>
				<c:if test="${userinfo != null }">
					<a href="${pageContext.request.contextPath }/login/loginout">&nbsp;&nbsp;&nbsp;&nbsp;退出</a>
				</c:if>
          </li>
          
      </ul>
      <!--nav-->
    </header>
	
	<div class="ydh_main">
		<div class="ydh_imgBox">
			<img src="${pageContext.request.contextPath }/stageRescourse/image/introduce/photo3.png" alt="">
		</div>
		<div class="ydh_con1">
			<div class="ydh_name">李华玲（副教授，硕导）</div>
			<!--<div class="ydh_date">发布日期：2018.3.21</div>-->
			<ul>
				<li>李华玲，女，硕士，云计算与大数据分析</li>
				<li>联系方式：120857486@qq.com</li>
			</ul>
			<div class="ydh_title">
				研究方向: 
			</div>
			<div class="ydh_content">
				数据可视化、大数据分析
			</div>
		</div>
		<div class="ydh_con2">
			<div class="ydh_title">
				教育经历<span>&nbsp;/&nbsp;Educational experience</span>
			</div>
			<div class="ydh_content">
				1975年生，中共党员，副教授，硕士生导师，山西省霍州市人。1998年毕业于华北工学院(现中北大学) 计算机科学与技术系，获工学学士学位，同年留校任教 ； 2003年毕业于华北工学院(现中北大学)计算机科学与技术系，获工学硕士学位；2006年2月至7月，清华大学软件学院软件工程专业进修。
				
			</div>
			<div class="ydh_title">
				研究成果<span>&nbsp;/&nbsp;Research findings</span>
			</div>
			<div class="ydh_content">
				 近年来主要从事大数据分析与数据可视化等相关方面的科学研究。主持了山西省教育厅科技研发项目一项：面向对象的仿真系统研究及其在虚拟实验中的应用；主持了山西省科技厅攻关项目一项：物联网与云计算环境下的农业信息服务系统的构建；参与了纵、横向项目及教改项目多项，发表论文多篇，被EI收录两篇。
				
			</div>
			<div class="ydh_content">
				多年来，为本科生与研究生主讲了《面向对象的程序设计》、《网页设计与网站建设》、《JavaEE框架与应用开发》、《UML建模与分析》、《计算机组成原理》、《数据结构》、《数据库原理及应用》等课程，参编教材三部。先后获得“中北大学教学名师”、“中北大学首届知行杯学生最喜爱的教师”“中北大学教书育人先进个人”、“中北大学爱岗敬业先进个人”等荣誉。
			</div>
		</div>
		<div class="ydh_btns">
			<div class="ydh_btn"><a href=""></a></div>
			<div class="ydh_btn"><a href=""></a></div>
		</div>
	</div>
	<footer>
		<img src="${pageContext.request.contextPath }/stageRescourse/ydh_img/ydh_footer_03.png" alt="">
	</footer>
</body>
</html>