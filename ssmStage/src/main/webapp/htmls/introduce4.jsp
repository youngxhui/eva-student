<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
   <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/base.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/header.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/introduce.css">
</head>
<body>
    <header id="header">
      <div class="logo">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/logo.png" alt="">
          <div class="name">
              <h3><a href="${pageContext.request.contextPath }/login/main">中北在线交流平台</a></h3>
              <h6>软件学院</h6>
          </div>
      </div>
      <!--logo -->

      <%-- <div class="search">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/sseachleft.png" alt='' class="sleft">
          <input type="text" placeholder="请输入您想要搜索的课程" class="sinput">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/ssearchright.png" alt='' class="sright">
      </div> --%>
      <!--search-->

     
      <!--notice -->

      <ul class="nav">
          <!-- 需要选中状态  加hot-->
          <li class="list hot">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/login/main">网站首页</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <c:if test="${userinfo == null }">
              <a href="${pageContext.request.contextPath }/login/login">&nbsp;&nbsp;&nbsp;&nbsp;登录</a>
            </c:if>
            <c:if test="${userinfo != null }">
              
              <a href="${pageContext.request.contextPath }/login/loginout">&nbsp;&nbsp;&nbsp;&nbsp;退出</a>
            </c:if>
          </li>
         
      </ul>
      <!--nav-->
</header>
    <div class="con">
        <div class="main">
            <div class="title">李华玲  <span>(副教授、硕导)</span></div>
            <p class="time">发布日期：2018.1.2</p>
            <div class="pic">
                <img src="${pageContext.request.contextPath }/stageRescourse/image/introduce/photo3.png" alt="">
            </div>
            <div class="intro">
                <p style="font-size: 16px">李华玲,女,硕士,云计算与大数据分析。</p>
                <p style="font-size: 16px">联系方式：120857486@qq.com</p>
                <p>
                    <b>教育经历:</b>
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    1975年生，中共党员，副教授，硕士生导师，山西省霍州市人。1998年毕业于华北工学院(现中北大学) 计算机科学与技术系，获工学学士学位，同年留校任教 ； 2003年毕业于华北工学院(现中北大学)计算机科学与技术系，获工学硕士学位；2006年2月至7月，清华大学软件学院软件工程专业进修。
                </p>
                <p>
                    <b>研究方向:</b>
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数据可视化、大数据分析
                </p>
                <p>
                    <b>研究成果</b>
                    <br>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;近年来主要从事大数据分析与数据可视化等相关方面的科学研究。主持了山西省教育厅科技研发项目一项：面向对象的仿真系统研究及其在虚拟实验中的应用；主持了山西省科技厅攻关项目一项：物联网与云计算环境下的农业信息服务系统的构建；参与了纵、横向项目及教改项目多项，发表论文多篇，被EI收录两篇。
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;多年来，为本科生与研究生主讲了《面向对象的程序设计》、《网页设计与网站建设》、《JavaEE框架与应用开发》、《UML建模与分析》、《计算机组成原理》、《数据结构》、《数据库原理及应用》等课程，参编教材三部。先后获得“中北大学教学名师”、“中北大学首届知行杯学生最喜爱的教师”“中北大学教书育人先进个人”、“中北大学爱岗敬业先进个人”等荣誉。</p>
                </p>
            </div>
        </div>
    </div>
	
	 <!-- 底部开始 -->
    <footer>
      <div class="contain">
        <img src="${pageContext.request.contextPath }/stageRescourse/image/footer/footer_06.png" alt="">
      </div>
    </footer>
    <!-- 底部结束 -->
	
</body>
</html>