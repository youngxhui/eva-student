<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
	 <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/base.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/header.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/main.css">
</head>
<body>
    <header id="header">
      <div class="logo">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/logo.png" alt="">
          <div class="name">
              <h3><a href="${pageContext.request.contextPath }/login/main">中北在线交流平台</a></h3>
              <h6>软件学院</h6>
          </div>
      </div>
      <!--logo -->

      <%-- <div class="search">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/sseachleft.png" alt='' class="sleft">
          <input type="text" placeholder="请输入您想要搜索的课程" class="sinput">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/ssearchright.png" alt='' class="sright">
      </div> --%>
      <!--search-->

     
      <!--notice -->

      <ul class="nav">
          <!-- 需要选中状态  加hot-->
          <li class="list hot">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/login/main">网站首页</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <c:if test="${userinfo == null }">
              <a href="${pageContext.request.contextPath }/login/login">&nbsp;&nbsp;&nbsp;&nbsp;登录</a>
            </c:if>
            <c:if test="${userinfo != null }">
              
              <a href="${pageContext.request.contextPath }/login/loginout">&nbsp;&nbsp;&nbsp;&nbsp;退出</a>
            </c:if>
          </li>
         
      </ul>
      <!--nav-->
</header>

    <div class="main">
        <div class="contain">
            <div class="title">
                <h4>中北大学软件学院&优逸客科技</h4>
                <h4>开展《面向对象程序设计》课程建设交流会</h4>
            </div>
            <div class="article">
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;为积极响应国家关于积极开展“新工科”背景下课程改革研讨与实践的号召，2018年2月7日，由中北大学软件学院与山西优逸客科技有限公司合作开展的《面向对象程序设计》课程建设交流会在优逸客学府街基地隆重召开。</p>
                 <div class="pic">
                    <img src="${pageContext.request.contextPath }/stageRescourse/image/main/news29.png" alt="">
                </div>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;中北大学软件学院教学副院长李众，《面向对象程序设计》课程组尹四清、薛海丽、李华玲等教师与优逸客副总经理兼实训总监严武军、优逸客图灵研究员BI专家任明明、优逸客软件信息与服务部经理赵士琛、优逸客院校合作部经理梁维岩等老师参加了本次交流会。。</p>
                 <div class="pic">
                    <img src="${pageContext.request.contextPath }/stageRescourse/image/main/news30.png" alt="">
                </div>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;下午15:00，本次《面向对象程序设计》课程建设交流会正式开始。首先优逸客院校合作部经理梁维岩老师就优逸客持续垂直于互联网技术的纵深研究，依托强大的研发团队在服务高校和企业的同时，不断探索高端人才培养方案与互联网产品解决方案等方面进行了综合汇报。并带领中北大学软件学院各位老师对优逸客实训基地进行了参观。</p>
                <div class="pic">
                    <img src="${pageContext.request.contextPath }/stageRescourse/image/main/news31.png" alt="">
                </div>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;参观结束后，中北大学软件学院教学副院长李众就中北大学对于本次课程改革和评估的要求做了全面阐述。他表示，中北大学非常重视此次“新工科”背景下课程改革研讨与实践活动，非常希望能够通过与优逸客的交流探讨，有效融合企业在项目实施中的经验于课程改革中，让改革后的新课程更加接贴合时代与市场需求。
接着中北大学软件学院《面向对象程序设计》课程组尹四清老师向与会人员详细介绍了《面向对象程序设计》课程大纲，并对即将进行的新课程改革要点进行了简单说明。
之后，优逸客图灵研究员BI专家任明明老师以真实项目为案例，详细阐述了在项目实施过程中，企业对Java工程师的多项要求。
最后，优逸客实训总监严武军老师根据优逸客多年来在互联网课程研发、实施与升级过程中的经验，结合“新工科”背景下课程改革与研讨要求，针对此次《面向对象程序设计》课程改革方案提出了一些建设性意见和想法。
分享结束后，校企双方就进一步推进《面向对象程序设计》课程建设与改革进行了深入地交流探讨。老师们根据课程大纲逐条分析，逐条推敲，交流探讨一度进入白热化状态，经过双方老师的共同努力，本次《面向对象程序设计》课程建设确定了初步实施方案。</p>
                <div class="pic">
                    <img src="${pageContext.request.contextPath }/stageRescourse/image/main/news32.png" alt="">
                </div>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;相同的教育情怀，同样的做事态度！中北大学软件学院与优逸客科技的此次课程改革交流会充分融汇了校企双方的优势资源，取得了初步成果。双方的交流合作是新课程评估道路上的大胆尝试，也是关键性一步！而这一切仅仅是开始，未来，相信双方合作的道路会更宽更远。</p>
                <div class="pic">
                    <img src="${pageContext.request.contextPath }/stageRescourse/image/main/news33.png" alt="">
                </div>
            </div>
        </div>
    </div>
	  <!-- 底部开始 -->
	<footer>
	  <div class="contain">
		<div class="main">
		  <div class="text">
			<p>版权所有 中北大学软件学院&nbsp;&nbsp;&nbsp;&nbsp;综合科：0351-3924578&nbsp;&nbsp;&nbsp;&nbsp;教学科：0351-3925275&nbsp;&nbsp;&nbsp;&nbsp;学生科、团委：0351-3924613&nbsp;&nbsp;&nbsp;&nbsp;科研与对外合作科：0351-3924595</p>
			<p>邮箱：sti@nuc.edu.cn&nbsp;&nbsp;&nbsp;&nbsp;本站由山西优逸客科技有限公司维护&nbsp;&nbsp;&nbsp;&nbsp;
			<!-- <a href="#" target="_blank">后台管理</a> -->
			</p>
		  </div>
		</div>
	  </div>
	</footer>
	<!-- 底部结束 -->
</body>
</html>