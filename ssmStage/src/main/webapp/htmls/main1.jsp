<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>中北新闻详情页面</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/base.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/animate.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/index.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/header.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/swiper.css">

	
    <script src="${pageContext.request.contextPath }/stageRescourse/js/jquery-3.2.1.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/swiper.min.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/index.js"></script>
   
	
    <link href="${pageContext.request.contextPath }/stageRescourse/Ban3D/screen.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath }/stageRescourse/Ban3D/modernizr.js"></script>
	
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/cnew.css">
</head>
<body>
	<header id="header">
      <div class="logo">
		  <a href="${pageContext.request.contextPath }/login/main">
			<img src="${pageContext.request.contextPath }/stageRescourse/image/home/logo3.png" alt="">
		  </a>
          <div class="name">
              <h3>中北在线交流平台</h3>
              <h6>中北在线</h6>
          </div>
      </div>
      <!--logo -->

      <div class="search">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/sseachleft.png" alt='' class="sleft">
          <input type="text" placeholder="请输入搜索的课程" class="sinput">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/ssearchright.png" alt='' class="sright">
      </div>
      <!--search-->

      <!--notice -->

      <ul class="nav">
          <!-- 需要选中状态  加hot-->
          <li class="list hot">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/login/main">网站首页</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
               	<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">用户中心</a>
				</c:if>
				<c:if test="${userinfo != null }">
					 <a href="${pageContext.request.contextPath }/personCenter/info">用户中心</a>
				</c:if>
          </li>
		  <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="">后台管理</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
				<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">&nbsp;&nbsp;&nbsp;&nbsp;登录</a>
				</c:if>
				<c:if test="${userinfo != null }">
					<a href="${pageContext.request.contextPath }/login/loginout">&nbsp;&nbsp;&nbsp;&nbsp;退出</a>
				</c:if>
          </li>
          
      </ul>
      <!--nav-->
    </header>
	
	<div class="cnewbody">
		<div class="ccontainer">
			
			<!-- 标题部分 -->
			<!--<h2 class="ctime">2018.1.10</h2>-->
			<h2 class="ctitle">中北大学软件学院携手优逸客</h2>
			<p class="cdescription">开展新工科背景下软件工程专业课程改革研讨和实践</p>
			
			<!-- 收藏和观看 -->

			<div class="cnumber">
				<span class="ccollect">32</span>
				<span class="cread">2</span>
			</div>

			<!-- 正文 -->
			<div class="ccontent">
			
				
				<p class="cxiao">2018年新年伊始，为积极响应习近平总书记《十九大报告》中提出的:“优先发展教育事业，完善职业教育和培训体系，深化产教融合、校企合作。加快一流大学和一流学科建设，实现高等教育内涵式发展”，中北大学软件学院携手山西优逸客科技开展了“新工科”背景下课程改革研讨与实践。此次课程改革充分借鉴了2017年复旦共识、天大行动和北京指南等“新工科”改革经验，以学生为中心，以学生获取能力为导向，持续改进实施方案，优化学生的组织与安排流程，围绕在线智慧教育逐步展开。</p>
                <p>1月10日下午15:30，中北大学软件学院教学沙龙暨新工科背景下软件工程专业课程改革研讨会在中北大学软件学院田园实训基地409会议室隆重召开。中北大学软件学院院长宋文爱，中北大学教务处副处长董小瑞，中北大学继续教育学院副院长刘兴来，中北大学软件学院副院长李众等校方领导出席了本次研讨会。优逸客副总经理兼实训总监严武军、优逸客图灵研究院BI专家任明明、优逸客实训发展部总监岳英俊、优逸客院校合作部总监梁维岩等优逸客领导出席了本次研讨会。中北大学软件学院多名副教授及在校老师也参与了此次研讨会！</p>
                <div class="ccontent-img" style="background-image:url(${pageContext.request.contextPath }/stageRescourse/image/main/news1.png)">
                </div>
                <p>研讨会正式开始，中北大学软件学院宋文爱院长为本次研讨会做了精彩致辞！</p>
                <div class="ccontent-img" style="background-image:url(${pageContext.request.contextPath }/stageRescourse/image/main/news2.png)">
                   
                </div>
                <p>之后，由负责本次新工科背景下课程改革实践活动的中北大学软件学院副教授李华玲老师对活动成果做了总结汇报。她介绍到，本期实践活动是分两批共六个班级分别以“京东爬虫系统实战”、“精品课网站中的UI设计与人机交互”、“H5在线视频播放器技术”为主题驱动，以项目输出为教学逐步展开的。共有来自12个班级的474名同学参与其中，学生在项目实施过程中表现良好，不仅顺利完成了预定项目，也在实践过程中提升了动手能力、表达能力、团队能力等，提高了自身的综合职业素养，增强了自驱力，为未来顺利进入职场积累了丰富的实践经验。经过校企双方的共同努力，此次新课程改革实践活动取得了圆满成功。</p>
                <div class="ccontent-img" style="background-image:url(${pageContext.request.contextPath }/stageRescourse/image/main/news3.png)">
                   
                </div>
                <p>接着，优逸客副总经理兼实训总监严武军老师对本次课程改革实施过程做了汇报。他讲到优逸客非常重视此次与中北大学软件学院共同实施“新工科”背景下课程改革实践活动，先后派出12名具有丰富项目实施经验的技术老师担任项目经理、12名经验丰富助教老师担任辅助指导、3名优秀的督导老师担任班主任，项目实施过程中教授的课程也是经过多位专业技术老师反复研讨才最终确定。实施过程严谨有序，始终坚持以商业项目平台为依托，以实际项目需求为驱动，以技术引导为方向，通过给学生讲解精品课程网站平台中常见的UI设计和人机交互的应用和技术实现。让学生了解了在线教育平台系统，进而在开发相关产品的过程中能有所应用、有所体会。此次教学活动探索和践行了“新工科”改革目标支撑下对毕业生的工程知识、问题分析、设计/开发解决方案、使用现代工具、职业规范、个人和团队、沟通、项目管理等方面综合能力维度的培养等8项基本要求，为下一步产学融合教学改革打开良好的开端。</p>
                <p>此外，通过校企双方联合培养的方式，集中了优势资源，让学生不仅了解了企业的文化和制度、熟悉了企业的工作流程和工作方式以及企业级项目分析、开发流程。也在完成整个企业级项目的过程中积累了丰富的项目经验，掌握了快速学习方法，培养了良好的分析问题和解决问题能力。并在职业素养课程的学习中锤炼良好的表达、沟通能力，为以后顺利走上职业发展道路积累了丰富的实践经验。</p>
                <div class="ccontent-img" style="background-image:url(${pageContext.request.contextPath }/stageRescourse/image/main/news4.png)">
                    
                </div>
                <p>中北大学教务处副处长董小瑞在发言中表示：教务处非常支持学院老师就科研教学方面开展的一系列交流探讨活动。为此，专门申报了 2018年“新工科”项目的专项预算。此次新课程改革和研讨以“新工科”为背景，以学生获取能力为导向，从毕业要求反向设计，并严格把握课程步骤…整个实施过程有纲有领、有张有序，是课程改革研讨中一次很好的尝试。今后，我们应该深刻总结此次活动中关于课程体系的重构、基于课程的培养方案、人才培养目标、毕业要求等方面的经验和不足，分析其中的强指针与弱指针，补足短板，持续改进，从而构建出一套完整的课程改体系，以更好的应用于课堂教学与实践教学当中。同时，他希望，在宋院长主导下，软件学院“新工科”背景下人才培养的研究与实践能够取得更多更高的成就。</p>
                <div class="ccontent-img" style="background-image:url(${pageContext.request.contextPath }/stageRescourse/image/main/news5.png)">
                    
                </div>
				<p>最后，此次课程改革发起人李华玲老师表示：“与会专家们专业，独到的点评使我受益匪浅，我都记在本上，放在心里了。接下来，我一定会用心总结，持续改进！”</p>
			
                <p>下午16:40，本次新工科背景下软件工程专业课程改革研讨会圆满结束！全体参会领导进行了合影留念！</p>
                <div class="ccontent-img" style="background-image:url(${pageContext.request.contextPath }/stageRescourse/image/main/news6.png)">
                    
                </div>
				<!--
                <p>研讨会结束后，参会领导到正在进行学习的学生实训教室进行了参观，了解了学生在此次实践活动中的收获和感受，观摩了学生项目评审过程。</p>
                <div class="ccontent-img" style="background-image:url(${pageContext.request.contextPath }/stageRescourse/image/main/news7.png)">
                    
                </div>
				-->
				</div>

			<!-- 分页 -->
			<div class="cpage">
				<a href="" class="cprev"></a>
				<a href="" class="cnext"></a>
				<div class="cclear"></div>
			</div>
		</div>
	</div>	
	<footer>
		<img src="${pageContext.request.contextPath }/stageRescourse/images/ydh_footer_03.png" alt="">
	</footer>
</body>
</html>
