<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>person</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/base.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/animate.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/index.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/header.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/swiper.css">

	
    <script src="${pageContext.request.contextPath }/stageRescourse/js/jquery-3.2.1.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/swiper.min.js"></script>
    <script src="${pageContext.request.contextPath }/stageRescourse/js/index.js"></script>
   
	
    <link href="${pageContext.request.contextPath }/stageRescourse/Ban3D/screen.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath }/stageRescourse/Ban3D/modernizr.js"></script>
	
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/ydh_person.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/stageRescourse/css/cnew.css">
	
</head>
<body>
	<header id="header">
      <div class="logo">
		  <a href="${pageContext.request.contextPath }/login/main">
			<img src="${pageContext.request.contextPath }/stageRescourse/image/home/logo3.png" alt="">
		  </a>
          <div class="name">
              <h3>中北在线交流平台</h3>
              <h6>中北在线</h6>
          </div>
      </div>
      <!--logo -->

      <div class="search">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/sseachleft.png" alt='' class="sleft">
          <input type="text" placeholder="请输入搜索的课程" class="sinput">
          <img src="${pageContext.request.contextPath }/stageRescourse/image/home/ssearchright.png" alt='' class="sright">
      </div>
      <!--search-->

      <!--notice -->

      <ul class="nav">
          <!-- 需要选中状态  加hot-->
          <li class="list hot">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/login/main">网站首页</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="${pageContext.request.contextPath }/login/login">用户中心</a>
          </li>
		  <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
              <a href="">后台管理</a>
          </li>
          <li class="list">
              <ul class="point">
                  <li></li>
                  <li></li>
              </ul>
				<c:if test="${userinfo == null }">
					<a href="${pageContext.request.contextPath }/login/login">&nbsp;&nbsp;&nbsp;&nbsp;登录</a>
				</c:if>
				<c:if test="${userinfo != null }">
					<a href="${pageContext.request.contextPath }/login/loginout">&nbsp;&nbsp;&nbsp;&nbsp;退出</a>
				</c:if>
          </li>
          
      </ul>
      <!--nav-->
    </header>
	
	<div class="ydh_main">
		<div class="ydh_imgBox">
			<img src="${pageContext.request.contextPath }/stageRescourse/image/introduce/photo1.png" alt="">
		</div>
		<div class="ydh_con1">
			<div class="ydh_name">薛海丽 讲师 </div>
			<div class="ydh_date">发布日期：20018.3.21</div>
			<ul>
				<li>薛海丽,硕士,软件工程学院软件开发与测试专业。</li>
				<li>联系方式 413046513@qq.com</li>
			</ul>
			<div class="ydh_title">
				研究方向: 
			</div>
			<div class="ydh_content">
				智能优化算法及其应用、云计算、软件开发与软件测试
			</div>
		</div>
		<div class="ydh_con2">
			<div class="ydh_title">
				教育经历<span>&nbsp;/&nbsp;Educational experience</span>
			</div>
			<div class="ydh_content">
				 中共党员，山西省运城人。2000年本科毕业于太原重型机械学院（现太原科技大学），2005年研究生毕业于中北大学，获工学硕士学位。现为中北大学软件学院教师。
				
			</div>
			<div class="ydh_title">
				研究方向<span>&nbsp;/&nbsp;Research findings</span>
			</div>
			<div class="ydh_content">
				网络信息处理、网络安全、软件开发与软件测试。
				
			</div>
			<div class="ydh_title">
				研究成果<span>&nbsp;/&nbsp;Research findings</span>
			</div>
			<div class="ydh_content">
				在校工作期间参加了山西省发改委2012年资助的“中北大学软件实训基地暨软件开发技术支撑平台建设“、物联网与云计算环境下的农业信息服务系统构建与推广、三台合一接处警系统等多项科研项目；出版教材1部；发表论文多篇；2011年评为优秀班主任；2012指导学生参加电子创新大赛，获山西赛区二等奖。
				
			</div>
		</div>
		<div class="ydh_btns">
			<div class="ydh_btn"><a href=""></a></div>
			<div class="ydh_btn"><a href=""></a></div>
		</div>
	</div>
	<footer>
		<img src="${pageContext.request.contextPath }/stageRescourse/ydh_img/ydh_footer_03.png" alt="">
	</footer>
</body>
</html>