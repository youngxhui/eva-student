$("input").focus(function(){
    "use strict";
    $(this).prev().addClass("active");
});
$("input").blur(function(){
    "use strict";
    $(this).prev().removeClass("active");
});