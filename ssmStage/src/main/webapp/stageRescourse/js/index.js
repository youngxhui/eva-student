$(function () {
    let parent = $(".index_test_btn>li");

    let son = $(".index_test_list>ul");

    let canvas1 = document.querySelector("#canvas");
    let canvas2 = document.querySelector("#canvas1");
    let canvas3 = document.querySelector("#canvas2");

    function progress(canvas, background, color, num) {
        let cobj = canvas.getContext("2d");
        cobj.lineWidth = 8;
        cobj.strokeStyle = background;
        cobj.font = "12px 宋体";
        cobj.textAlign = "center";
        cobj.textBaseline = "middle";
        cobj.fillStyle = color;
        let n = 0;
        let fn;

        function power() {
            n++;
            if (n >= num) {
                cancelAnimationFrame(fn)
            } else {
                fn = requestAnimationFrame(power);
            }
            cobj.clearRect(0, 0, 67, 67);
            cobj.beginPath();
            let angle = (n / 100 * 360 - 90) * Math.PI / 180;
            cobj.arc(33.5, 33.5, 20, -Math.PI / 2, angle);
            cobj.stroke();
            cobj.fillText(n + "%", 33.5, 33.5);
        }

        power();
    }

    let background1 = '#4682FE';
    let color1 = '#4682FE';
    let background2 = '#29CB6D';
    let color2 = '#29CB6D';
    let background3 = '#FFB033';
    let color3 = '#FFB033';
    progress(canvas1, background1, color1, 80);
    progress(canvas2, background2, color2, 60);
    progress(canvas3, background3, color3, 56);

    let modules = $('.index_module_details');
    let spider = $('.spider');
    spider.css({opacity: 0, zIndex: -1});
    modules.on('click', function () {
        spider.css({opacity: 1, zIndex: 9999});
    });


    // float
    let lis = $('.index_module_course > li , .index_interaction_personal > li,.index_techer_list li')

    lis.hover(function () {
        $(this).toggleClass('float');
        $(this).find('.progress').toggleClass('active')
    })

});