package org.bb.ssm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.bb.ssm.model.College;
import org.bb.ssm.model.Comments;
import org.bb.ssm.model.Course;
import org.bb.ssm.model.Direction;
import org.bb.ssm.model.Knowledge;
import org.bb.ssm.model.ResourceManager;
import org.bb.ssm.model.SecBase;
import org.bb.ssm.model.Subject;
import org.bb.ssm.model.User;
import org.bb.ssm.service.ChapterInfoService;
import org.bb.ssm.service.CollegeInfoService;
import org.bb.ssm.service.CommentsInfoService;
import org.bb.ssm.service.CourseInfoService;
import org.bb.ssm.service.DirectionInfoService;
import org.bb.ssm.service.KnowledgeInfoService;
import org.bb.ssm.service.ResourceManagerInfoService;
import org.bb.ssm.service.SecBaseInfoService;
import org.bb.ssm.service.SubjectInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value = "/course")
public class CourseInfoCotroller {

	@Autowired
	private CollegeInfoService collegeInfoService;

	@Autowired
	private SubjectInfoService subjectInfoService;

	@Autowired
	private DirectionInfoService directionInfoService;

	@Autowired
	private CourseInfoService courseInfoService;

	@Autowired
	private ChapterInfoService chapterInfoService;

	@Autowired
	private KnowledgeInfoService knowledgeInfoService;

	@Autowired
	private SecBaseInfoService secBaseInfoService;

	@Autowired
	private CommentsInfoService commentsInfoService;
	
	@Autowired
	private ResourceManagerInfoService resourceManagerInfoService;

	/**
	 * 课程列表页
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/index")
	public String index(Map<String, Object> map) {
		
		// 1 读取学院信息
		List<College> colleges = collegeInfoService.findAll();
		map.put("colleges", colleges);
		// 2 读取专业信息----学科表中num长度大于等于6的
		List<Subject> subjects = subjectInfoService.findAll();
		map.put("subjects", subjects);

		// 3 读取方向信息
		List<Direction> directions = directionInfoService.findAll();
		map.put("directions", directions);

		// 4 读取课程信息

		List<Course> courses = courseInfoService.findAll();
		map.put("courses", courses);

		return "stage/courseList";
	}
	
	/**
	 * 根据课程id和pid查看相关资料
	 * @param courseId 课程id
	 * @param pid 父id
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/relevantDocument/courseId/{courseId}/pid/{pid}")
	public String relevantDocument(@PathVariable(value = "courseId") Integer courseId , @PathVariable(value = "pid") Integer pid, Map<String, Object> map,HttpSession httpSession) {
		
		User userinfo = (User) httpSession.getAttribute("userinfo");

		if (userinfo == null) {
			httpSession.setAttribute("forwardPage", "/course/relevantDocument/courseId/"+courseId+"/pid/"+pid);
			return "redirect:/login/login";
		}
		
		List<ResourceManager> resources = resourceManagerInfoService.findByCourseIdAndPid(courseId, pid);
		
		map.put("resources", resources);
		map.put("courseId", courseId);
		map.put("pid", pid);
	
		return "stage/relevant";
	}


	
	/**
	 * 异步获取专业信息
	 * 
	 * @param collegeId=0
	 * @return
	 */
	@RequestMapping(value = "/getSubject", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getSubject(int collegeId) {
		System.out.println(collegeId);
		List<Subject> subjects = subjectInfoService.findAllMajor(collegeId);

		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(subjects);

			// System.out.println(jsondata);

			return jsondata;

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 进入到视频查看页面，这个页面是整个系统最重要的页面 要能保证1000人的并发
	 * 
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/video/{id}/knowledgeId/{knowledgeId}", method = RequestMethod.GET)
	public String video(@PathVariable(value = "id") Integer id,
			@PathVariable(value = "knowledgeId") Integer knowledgeId ,
			Map<String, Object> map, HttpSession httpSession) {

		User userinfo = (User) httpSession.getAttribute("userinfo");

		if (userinfo == null) {
			
			httpSession.setAttribute("forwardPage", "/course/video/"+id+"/knowledgeId/"+knowledgeId);
			return "redirect:/login/login";
		}

		// 根据课程id找到所有的知识点 也就是课程-章-节中的节
		// 这里跳过了章，因为节的视频中1.1 2.1等也可以体现节
		List<Knowledge> knowledges = knowledgeInfoService.selectByCourseId(id);
		
		if(knowledges.size() == 0 )
		{
			return "stage/videoNotFound";
		}
		
//		// 根据当前用户查找对应的播放记录进行续播
//		String overtime = secBaseInfoService.findtimeByUserid(userinfo.getId());
//		if (overtime != null) {
//			map.put("overtime", overtime);
//		}

		
		
		List<Knowledge> newKnowledges = null;
		
		//如果是0 证明请求是从courseList页面中点击过来的，否则是从video页面点击过来
		if(knowledgeId == 0)
		{
			knowledgeId = knowledges.get(0).getId();
			
		}
		
		Knowledge fromKnowledge = new Knowledge();
		fromKnowledge.setId(knowledgeId);
		
		
		int fromIndex = knowledges.indexOf(fromKnowledge);
		int nextIndex = fromIndex + 1;
		
		
		fromKnowledge = knowledges.get(fromIndex);
		
		if(nextIndex >= knowledges.size())
			nextIndex = fromIndex;
		
		Knowledge nextKnowledge = knowledges.get(nextIndex);
		
		newKnowledges = knowledges.subList(fromIndex,knowledges.size());
		if(newKnowledges == null)
		{
			map.put("videoinfo", knowledges);
		}else
		{
			map.put("videoinfo", newKnowledges);
		}
		
		map.put("videoinfo", knowledges);
		map.put("fromKnowledge", fromKnowledge);
		map.put("nextKnowledge", nextKnowledge);
		
		// 把课程id也传给页面吧，后续要用，比如评论添加没有用ajax，这样添加之后跳转会当前页面还需要课程id
		map.put("courseId", id);
		map.put("knowledgeId", knowledgeId);
		
		List<Comments> comments = commentsInfoService.findCommentsByKnowledgeId(knowledgeId);
		
		//查看所有的知识点,newKnowledges是根据video页面点击过来，从点击算起后边的知识点
		map.put("knowledges", knowledges);
		map.put("comments", comments);

		return "stage/video";
	}

	@RequestMapping(value = "/videorecord", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public String videorecord(SecBase videorecord) {
		// 插入新的记录值前先删除原来的
		secBaseInfoService.deleteByUserid(videorecord.getUser_id());
		secBaseInfoService.insert(videorecord);
		return null;
	}

	/**
	 * 根据id找到职业路径信息
	 * 
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/coursePath/{id}", method = RequestMethod.GET)
	public String coursePath(@PathVariable(value = "id") Integer id, Map<String, Object> map) {
		map.put("command", courseInfoService.selectByPrimaryKey(id));
		return "stage/coursePath";
	}

	/**
	 * 得到所有课程信息
	 * 
	 * @param college_id,subject_id,grade
	 * @return
	 */
	@RequestMapping(value = "/getAllCourse", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String getAllCourse(int limit, int pageindex, int college_id, int subject_id, int grade) {
		int offset = limit * pageindex;
		List<Course> courseList = courseInfoService.findAllCourse(limit, offset, college_id, subject_id, grade);

		HashMap<String, Object> tcourse = new HashMap<String, Object>();

		tcourse.put("rows", courseList);
		tcourse.put("results", courseInfoService.getCourseCount(college_id, subject_id, grade));

		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(tcourse);

			// System.out.println(jsondata);

			return jsondata;

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 通过handler前往添加课程页面
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/addcourse", method = RequestMethod.GET)
	public String addcourse(Map<String, Object> map) {
		// 因为页面使用spring的form标签，其中属性modelAttribute需要存在bean 要不会报错
		map.put("command", new Course());
		return "addcourse";
	}

	/**
	 * 添加课程操作
	 * 
	 * @param courseinfo
	 * @return
	 */
	@RequestMapping(value = "/addcourse", method = RequestMethod.POST)
	public String save(Course courseinfo) {
		int result = courseInfoService.insert(courseinfo);
		System.out.println("添加课程的操作结果为：" + result);
		return "redirect:/course/getAllCourse";
	}

	/**
	 * 删除课程操作
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "id") int id) {
		int result = courseInfoService.deleteByPrimaryKey(id);
		System.out.println("删除课程的操作结果为：" + result + "传递进来的id为：" + id);
		return "redirect:/course/getAllCourse";
	}

	/**
	 * 更新前先根据id找到课程信息，回显到页面上
	 * 
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public String input(@PathVariable(value = "id") Integer id, Map<String, Object> map) {
		map.put("command", courseInfoService.selectByPrimaryKey(id));
		return "addcourse";
	}

	@ModelAttribute
	public void getcourseInfo(@RequestParam(value = "courseId", required = false) Integer id, Map<String, Object> map) {
		System.out.println("每个controller 方法都会先调用我哦");
		if (id != null) {
			System.out.println("update 操作");
			map.put("courseInfo", courseInfoService.selectByPrimaryKey(id));
		}
		System.out.println("insert  操作");
	}

	@RequestMapping(value = "/addcourse", method = RequestMethod.PUT)
	public String update(Course courseinfo) {
		courseInfoService.updateByPrimaryKey(courseinfo);
		return "redirect:/course/getAllCourse";
	}
}
