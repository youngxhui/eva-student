package org.bb.ssm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bb.ssm.model.Chapter;
import org.bb.ssm.service.ChapterInfoService;
import org.bb.ssm.service.RoleInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value="/chapter")
public class ChapterInfoCotroller {
	
	@Autowired
	private ChapterInfoService chapterInfoService;
	
	@Autowired
	private RoleInfoService roleInfoService;
	
	
	/**
	 * 章列表页
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/index")
	public String index(Map<String, Object> map){
		
		return "bui/acdemic/chapterList";
	}
	
	/**
	 * 得到所有章信息
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/getAllChapter",method={RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public String getAllChapter(@RequestParam(value="limit",required=false) Integer limit,@RequestParam(value="pageIndex",required=false) Integer pageIndex,@RequestParam(value="searchname",required=false) String searchname,@RequestParam(value="status",required=false) Integer status){
		pageIndex=pageIndex*limit;
		List<Chapter> chapterList = chapterInfoService.findAll(limit,pageIndex,searchname,status);
		
		HashMap<String,Object > tChapter = new HashMap<String,Object >();
		
		tChapter.put("rows", chapterList);
		tChapter.put("results", chapterInfoService.totalCount(searchname,status));
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(tChapter);

			//System.out.println(jsondata);
			
			return jsondata;
			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 通过handler前往添加章页面
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/addChapter",method= RequestMethod.GET)
	public String addChapter(Map<String, Object> map){
		//因为页面使用spring的form标签，其中属性modelAttribute需要存在bean 要不会报错
		map.put("command", new Chapter());
		return "addChapter";
	}
	
	
	
	/**
	 * 添加章操作
	 * @param chapterinfo
	 * @return
	 */
	@RequestMapping(value="/addChapter",method=RequestMethod.POST)
	public String save(Chapter chapterinfo){
		int result = chapterInfoService.insert(chapterinfo);
		System.out.println("添加章的操作结果为："+result);
		return "redirect:/chapter/getAllChapter";
	}
	/**
	 * 删除章操作
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public String delete(@PathVariable(value="id") int id){
		int result = chapterInfoService.deleteByPrimaryKey(id);
		System.out.println("删除章的操作结果为："+result+"传递进来的id为："+id);
		return "redirect:/chapter/getAllChapter";
	}
	/**
	 * 更新前先根据id找到章信息，回显到页面上
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/detail/{id}",method=RequestMethod.GET)
	public String input(@PathVariable(value="id") Integer id,Map<String, Object> map){
		map.put("command", chapterInfoService.selectByPrimaryKey(id));
		return "addChapter";
	}
	
	@ModelAttribute
	public void getChapterInfo(@RequestParam(value="chapterId",required=false) Integer id
			,Map<String, Object> map){
		System.out.println("每个controller 方法都会先调用我哦");
		if(id != null){
			System.out.println("update 操作");
			map.put("chapterInfo", chapterInfoService.selectByPrimaryKey(id));
		}
		System.out.println("insert  操作");
	}
	
	@RequestMapping(value="/addChapter",method=RequestMethod.PUT)
	public String update(Chapter chapterinfo){
		chapterInfoService.updateByPrimaryKey(chapterinfo);
		return "redirect:/chapter/getAllChapter";
	}
}
