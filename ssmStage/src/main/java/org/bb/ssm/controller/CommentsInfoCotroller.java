package org.bb.ssm.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bb.ssm.model.Comments;
import org.bb.ssm.service.CommentsInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value="/comments")
public class CommentsInfoCotroller {
	
	@Autowired
	private CommentsInfoService commentsInfoService;

	
	/**
	 * 评论列表页
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/index")
	public String index(Map<String, Object> map){
		
		return "bui/acdemic/commentsList";
	}
	
	/**
	 * 得到所有评论信息
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/getAllComments",method={RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public String getAllComments(@RequestParam(value="knowledgeId",required=false) Integer knowledgeId){
		
		List<Comments> commentsList = commentsInfoService.findAll();
		
		HashMap<String,Object > tComments = new HashMap<String,Object >();
		
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(tComments);

			//System.out.println(jsondata);
			
			return jsondata;
			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 通过handler前往添加评论页面
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/addComments",method= RequestMethod.GET)
	public String addComments(Map<String, Object> map){
		//因为页面使用spring的form标签，其中属性modelAttribute需要存在bean 要不会报错
		map.put("command", new Comments());
		return "addComments";
	}
	
	
	
	/**
	 * 添加评论操作
	 * @param commentsinfo
	 * @return
	 */
	@RequestMapping(value="/addComments",method=RequestMethod.POST)
	public String save(Comments commentsinfo , int courseId,int knowledgeId){
		commentsinfo.setCommentTime(new Timestamp(new Date().getTime()));
		
		int result = commentsInfoService.insert(commentsinfo);
		System.out.println("添加评论的操作结果为："+result);
		
		return "redirect:/course/video/"+courseId+"/knowledgeId/"+knowledgeId;
	}
	/**
	 * 删除评论操作
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/delete/{id}",method=RequestMethod.DELETE)
	public String delete(@PathVariable(value="id") int id){
		int result = commentsInfoService.deleteByPrimaryKey(id);
		System.out.println("删除评论的操作结果为："+result+"传递进来的id为："+id);
		return "redirect:/comments/getAllComments";
	}
	/**
	 * 更新前先根据id找到评论信息，回显到页面上
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/detail/{id}",method=RequestMethod.GET)
	public String input(@PathVariable(value="id") Integer id,Map<String, Object> map){
		map.put("command", commentsInfoService.selectByPrimaryKey(id));
		return "addComments";
	}
	
	@ModelAttribute
	public void getCommentsInfo(@RequestParam(value="commentsId",required=false) Integer id
			,Map<String, Object> map){
		System.out.println("每个controller 方法都会先调用我哦");
		if(id != null){
			System.out.println("update 操作");
			map.put("commentsInfo", commentsInfoService.selectByPrimaryKey(id));
		}
		System.out.println("insert  操作");
	}
	
	@RequestMapping(value="/addComments",method=RequestMethod.PUT)
	public String update(Comments commentsinfo){
		commentsInfoService.updateByPrimaryKey(commentsinfo);
		return "redirect:/comments/getAllComments";
	}
}
