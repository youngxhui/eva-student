package org.bb.ssm.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.bb.ssm.model.Menu;
import org.bb.ssm.service.MenuInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value = "/menu")
public class MenuInfoCotroller {

	@Autowired
	private MenuInfoService menuInfoService;

	/**
	 * 菜单列表页
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/index")
	public String index(Map<String, Object> map){
		//List<User> userList = userInfoService.findAll();
		//map.put("ALLUSER", userList);
		return "bui/system/menuList";
	}
	
	//[{"id":4,"menu":[{"collapsed":"权限管理","text":"权限管理","items":[{"id":"6","text":"菜单管理","href":"/ssm/menu/index"},{"id":"7","text":"角色管理","href":"/ssm/role/index"},{"id":"8","text":"用户管理","href":"/ssm/user/index"}]},{"collapsed":"信息管理","text":"信息管理","items":[{"id":"10","text":"数据备份","href":"/ssm/"},{"id":"11","text":"数据恢复","href":"/ssm/"},{"id":"286","text":"轮播配置","href":"/ssm/carsoul/queryPage"}]},{"collapsed":"个人设置","text":"个人设置","items":[{"id":"14","text":"修改密码","href":"/ssm/Safety/updatePassword"}]}]},{"id":15,"menu":[{"collapsed":"组织机构","text":"组织机构","items":[{"id":"17","text":"部门管理","href":"/ssm/department/index"},{"id":"18","text":"职位管理","href":"/ssm/position/index"},{"id":"19","text":"员工管理","href":"/ssm/employee/index"}]}]},{"id":45,"menu":[{"collapsed":"课程体系","text":"课程体系","items":[{"id":"47","text":"学校管理","href":"/ssm/university/index"},{"id":"48","text":"学院管理","href":"/ssm/college/index"},{"id":"49","text":"学科管理","href":"/ssm/subject/index"},{"id":"50","text":"方向管理","href":"/ssm/direction/index"},{"id":"51","text":"课程管理","href":"/ssm/course/index"},{"id":"277","text":"章管理","href":"/ssm/chapter/index"},{"id":"278","text":"节管理","href":"/ssm/section/index"},{"id":"279","text":"知识点管理","href":"/ssm/knowledge/index"},{"id":"280","text":"能力管理","href":"/ssm/ability/index"},{"id":"281","text":"指标管理","href":"/ssm/indicators/index"},{"id":"295","text":"资源管理","href":"/ssm/resourceManager/index"}]},{"collapsed":"班级","text":"班级","items":[{"id":"63","text":"班级管理","href":"/ssm/class/index"}]},{"collapsed":"学生","text":"学生","items":[{"id":"67","text":"学生管理","href":"/ssm/students/index"}]},{"collapsed":"试卷管理","text":"试卷管理","items":[{"id":"75","text":"题目管理","href":"/ssm/titles/index"},{"id":"76","text":"电脑组卷","href":"/ssm/AutoGeneratePaper/INDEX"},{"id":"79","text":"课程试卷","href":"/ssm/CoursePaper/INDEX"},{"id":"234","text":"人工组卷","href":"/ssm/ManualPaper/INDEX"}]},{"collapsed":"实训员工","text":"实训员工","items":[{"id":"82","text":"随时练习","href":"/ssm/EvaluateAnytimetest/INDEX"},{"id":"84","text":"课程测试","href":"/ssm/EvaluateCourse/INDEX"},{"id":"138","text":"收藏题目库","href":"/ssm/EvaluateCollect/INDEX"},{"id":"139","text":"错题记录库","href":"/ssm/EvaluateWrong/INDEX"},{"id":"159","text":"答题原始记录","href":"/ssm/EvaluatePreAnswer/INDEX"},{"id":"178","text":"查看成绩","href":"/ssm/EvaluateLookScores/index"},{"id":"180","text":"班级成绩统计","href":"/ssm/ShowClassScore/index"},{"id":"244","text":"查看成绩单","href":"/ssm/Evaluatelookpages/stugradepage"}]},{"collapsed":"申请审批","text":"申请审批","items":[]}]}]

	
	/**
	 * 得到所有子级菜单信息
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/getMenus" )
	@ResponseBody
	public String getMenus(HttpServletRequest request) {
		List<Menu> menuList = menuInfoService.findAll();
		

		/*HashMap<String , String> tMenu1 = new HashMap<String , String>();
		tMenu1.put("id" , "main-menu");
		tMenu1.put("text" , "顶部导航");
		tMenu1.put("href" , "href");
		
		List<HashMap<String , String>> list = new ArrayList<HashMap<String , String>>();
		
		list.add(tMenu1);
		
		HashMap<String , Object> tMenu2 = new HashMap<String , Object>();
		tMenu2.put("text", "首页内容");
		tMenu2.put("items", list);
		
		List<Map> list2 = new ArrayList<Map>();
		list2.add(tMenu2);
		
		HashMap<String , Object> tMenu3 = new HashMap<String , Object>();
		tMenu3.put("id", "menu");
		tMenu3.put("menu", list2);*/
		
		List<Map> list3 = new ArrayList<Map>();
		for (Menu menus : menuList) {
			HashMap<String , Object> tMenu3 = new HashMap<String , Object>();
			if(menus.getParent_id()==null){
				tMenu3.put("id", menus.getId());
				
				List<Map> list2 = new ArrayList<Map>();
				for (Menu menus1 : menuList) {
					HashMap<String , Object> tMenu2 = new HashMap<String , Object>();
					if (menus1.getParent_id()==menus.getId()) {
						tMenu2.put("text", menus1.getName());
						tMenu2.put("collapsed", menus1.getName());
						List<HashMap<String , String>> list = new ArrayList<HashMap<String , String>>();
						for (Menu menus2 : menuList) {
							HashMap<String , String> tMenu1 = new HashMap<String , String>();
							if(menus2.getParent_id()==menus1.getId()){
								tMenu1.put("id" , ""+menus2.getId());
								tMenu1.put("text" ,menus2.getName());
								tMenu1.put("href" , request.getContextPath()+"/"+menus2.getTarget_href());
								
								list.add(tMenu1);
							}
							
						}
						tMenu2.put("items", list);
						
						list2.add(tMenu2);
					}
					
				}
				tMenu3.put("menu", list2);
				
				list3.add(tMenu3);
			}
			
		}
		
//		return tMenu3;
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(list3);

			System.out.println(jsondata);
			
			return jsondata;
			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;

//		System.out.println("-------------------------");
		
	}

	/**
	 * 得到树形菜单
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/getAllMenu")
	@ResponseBody
	public String getAllMenu(Map<String, Object> map) {
		List<Menu> menuList = menuInfoService.findAll();
		
		HashMap<String,Object > tUser = new HashMap<String,Object >();
		
		tUser.put("rows", menuList);
		tUser.put("results", menuList.size());
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(tUser);

			//System.out.println(jsondata);
			
			return jsondata;
			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 得到分页菜单信息
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/getPageMenu")
	@ResponseBody
	public String getPageMenu(Map<String, Object> map) {
		List<Menu> menuList = menuInfoService.findPage();
		
		HashMap<String,Object > tUser = new HashMap<String,Object >();
		
		tUser.put("rows", menuList);
		tUser.put("results", menuList.size());
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsondata = mapper.writeValueAsString(tUser);

			//System.out.println(jsondata);
			
			return jsondata;
			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 通过handler前往添加菜单页面
	 * 
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/addMenu", method = RequestMethod.GET)
	public String addMenu(Map<String, Object> map) {
		// 因为页面使用spring的form标签，其中属性modelAttribute需要存在bean 要不会报错
		map.put("command", new Menu());
		return "addMenu";
	}

	/**
	 * 添加菜单操作
	 * 
	 * @param menuinfo
	 * @return
	 */
	@RequestMapping(value = "/addMenu", method = RequestMethod.POST)
	public String save(Menu menuinfo) {
		int result = menuInfoService.insert(menuinfo);
		System.out.println("添加菜单的操作结果为：" + result);
		return "redirect:/menu/getAllMenu";
	}

	/**
	 * 删除菜单操作
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "id") int id) {
		int result = menuInfoService.deleteByPrimaryKey(id);
		System.out.println("删除菜单的操作结果为：" + result + "传递进来的id为：" + id);
		System.out.println("***************************************");
		return "redirect:/menu/getAllMenu";
	}

	/**
	 * 更新前先根据id找到菜单信息，回显到页面上
	 * 
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public String input(@PathVariable(value = "id") Integer id,
			Map<String, Object> map) {
		map.put("command", menuInfoService.selectByPrimaryKey(id));
		return "addMenu";
	}

	@ModelAttribute
	public void getMenuInfo(
			@RequestParam(value = "menuId", required = false) Integer id,
			Map<String, Object> map) {
		System.out.println("每个controller 方法都会先调用我哦");
		if (id != null) {
			System.out.println("update 操作");
			map.put("menuInfo", menuInfoService.selectByPrimaryKey(id));
		}
		System.out.println("insert  操作");
	}

	@RequestMapping(value = "/addMenu", method = RequestMethod.PUT)
	public String update(Menu menuinfo) {
		menuInfoService.updateByPrimaryKey(menuinfo);
		return "redirect:/menu/getAllMenu";
	}
}
