package org.bb.ssm.controller;

import javax.servlet.http.HttpSession;

import org.bb.ssm.model.User;
import org.bb.ssm.service.UserInfoService;
import org.bb.ssm.utils.AppMD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value="/personCenter")
public class PersonCenterCotroller {
	
	@Autowired
	private UserInfoService userInfoService;
	
	/**
	 * 进入到个人中心-默认是维护个人信息页面
	 * @param 
	 * @return
	 */
	@RequestMapping(value="/info")
	public String index( HttpSession httpSession){
		
		User userinfo = (User) httpSession.getAttribute("userinfo");

		if (userinfo == null) {
			return "redirect:/login/login";
		}
		
		return "stage/user/info";
	}

	/**
	 * 进入到个人中心-修改密码界面
	 * @param 
	 * @return
	 */
	@RequestMapping(value="/password")
	public String password(){
		
		return "stage/user/password";
	}
	
	/**
	 * 进入到个人中心-我的收藏
	 * @param 
	 * @return
	 */
	@RequestMapping(value="/collect")
	public String collect(){
		
		return "stage/user/collect";
	}
	
	/**
	 * 进入到个人中心-我正在学的课程
	 * @param 
	 * @return
	 */
	@RequestMapping(value="/course")
	public String course(){
		
		return "stage/user/course";
	}
	
	/**
	 * 修改密码 
	 */
	@RequestMapping(value="/updatePassword",method=RequestMethod.POST)
	public String update(User userinfo){
		userinfo.setStatus(1);
		
		userinfo.setPassword(AppMD5Util.getMD5(userinfo.getPassword()));
		
		userInfoService.updateByPrimaryKey(userinfo);
		
		return "redirect:/login/login";
	}
}
