package org.bb.ssm.mapper;

import java.util.List;

import org.bb.ssm.model.Comments;

public interface CommentsInfoMapper {
	
	List<Comments> findAll();
	
	List<Comments> findCommentsByKnowledgeId(Integer knowledgeId);
	
    int deleteByPrimaryKey(Integer commentsId);

    int insert(Comments record);

    int insertSelective(Comments record);

    Comments selectByPrimaryKey(Integer commentsId);

    int updateByPrimaryKeySelective(Comments record);

    int updateByPrimaryKey(Comments record);

	Comments selectByPwd(Comments record);
	
}