package org.bb.ssm.service;

import java.util.List;

import org.bb.ssm.model.Comments;

public interface CommentsInfoService {

	List<Comments> findAll();
	List<Comments> findCommentsByKnowledgeId(int knowledgeId);
	
	int deleteByPrimaryKey(Integer id);

	int insert(Comments record);

	int insertSelective(Comments record);

	Comments selectByPrimaryKey(Integer id);
	
	Comments selectByPwd(Comments record);

	int updateByPrimaryKeySelective(Comments record);

	int updateByPrimaryKey(Comments record);
	
	
}
