package org.bb.ssm.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.bb.ssm.mapper.CommentsInfoMapper;
import org.bb.ssm.model.Comments;
import org.bb.ssm.service.CommentsInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//可以消除xml中对bean的配置
@Service
// 此处使用spring的声明式事务，不在使用sqlsession和提交事务了
@Transactional
public class CommentsInfoServiceImpl implements CommentsInfoService {

	@Resource
	private CommentsInfoMapper mapper;

	/**
	 * 查询CommentsInfo表所有数据
	 */
	@Override
	public List<Comments> findAll() {
		List<Comments> list = mapper.findAll();
		return list;
	}
	
	/**
	 * 根据知识点id（也就是对应视频）查找视频评论
	 */
	@Override
	public List<Comments> findCommentsByKnowledgeId(int knowledgeId) {
		List<Comments> list = mapper.findCommentsByKnowledgeId(knowledgeId);
		return list;
	}
	
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(Comments record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(Comments record) {
		return mapper.insertSelective(record);
	}

	@Override
	public Comments selectByPrimaryKey(Integer id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(Comments record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Comments record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public Comments selectByPwd(Comments record) {
	
		return mapper.selectByPwd(record);
	}

}
