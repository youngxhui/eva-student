package org.bb.ssm.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.bb.ssm.mapper.ResourceManagerInfoMapper;
import org.bb.ssm.model.ResourceManager;
import org.bb.ssm.service.ResourceManagerInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//可以消除xml中对bean的配置
@Service
// 此处使用spring的声明式事务，不在使用sqlsession和提交事务了
@Transactional
public class ResourceManagerInfoServiceImpl implements ResourceManagerInfoService {

	@Resource
	
	private ResourceManagerInfoMapper mapper;

	
	
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(ResourceManager record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(ResourceManager record) {
		return mapper.insertSelective(record);
	}

	@Override
	public ResourceManager selectByPrimaryKey(Integer id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(ResourceManager record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(ResourceManager record) {
		return mapper.updateByPrimaryKey(record);
	}

	
	@Override
	public List<ResourceManager> findAll(Integer limit, Integer pageIndex,
			String searchname,Integer pid) {
		List<ResourceManager> list = mapper.findAll(limit,pageIndex,searchname,pid);
		return list;
	}
	
	@Override
	public List<ResourceManager> findByCourseIdAndPid(Integer courseId , Integer pid) {
		List<ResourceManager> list = mapper.findByCourseIdAndPid(courseId, pid);
		
		return list;
	}

	@Override
	public int totalCount(String searchname,Integer pid) {
		return (int) mapper.getResourceManagerCount(searchname,pid);
	}

	@Override
	public List<ResourceManager> finddir() {
		List<ResourceManager> list = mapper.finddir();
		return list;
	}

}
